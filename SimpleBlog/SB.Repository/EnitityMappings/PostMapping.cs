﻿using Microsoft.EntityFrameworkCore;
using SB.Repository.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SB.Repository.EnitityMappings
{
    public class PostMapping : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.ToTable("Post");
            builder.HasOne(x => x.Category)
                             .WithMany(x => x.Posts)
                             .HasForeignKey(x => x.CategoryId);
        }
    }
}
