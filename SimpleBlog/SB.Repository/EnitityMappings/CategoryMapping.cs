﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SB.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SB.Repository.EnitityMappings
{
    public class CategoryMapping : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("Category");
            entityTypeBuilder.HasMany(x => x.Posts);
        }
    }
}
