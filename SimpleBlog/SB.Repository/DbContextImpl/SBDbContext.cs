﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SB.Core.Identity;
using SB.Repository.Extensions;
using System.Reflection;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System;
using SB.Repository.Entities;

namespace SB.Repository.DbContextImpl
{
    public class SBDbContext : IdentityDbContext<User, Role, int,UserClaim,UserRole,UserLogin, RoleClaim, UserToken>
    {
        public bool IsSavedChangeAutomatically { get; set; }

        private readonly IHttpContextAccessor httpContext;
        public SBDbContext(DbContextOptions<SBDbContext> options, IHttpContextAccessor httpContextAccessor) : base(options)
        {
            httpContext = httpContextAccessor;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.AddEntityConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }

        public override int SaveChanges()
        {
            AddAuditData();
            return base.SaveChanges();
        }

        protected void AddAuditData()
        {
            var entities = this.ChangeTracker.Entries().Where(x => x.Entity is BaseAdvanceEntity && (x.State == EntityState.Added || x.State == EntityState.Modified)).ToList();

            var userId = "";
            try
            {
                userId = httpContext.HttpContext.User.Identity.Name;
            }
            catch (Exception)
            {
                // ignored

            }

            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((BaseAdvanceEntity)entity.Entity).CreatedDate = DateTime.Now;
                    ((BaseAdvanceEntity)entity.Entity).CreatedBy = userId;
                    ((BaseAdvanceEntity)entity.Entity).UpdatedDate = DateTime.Now;
                    ((BaseAdvanceEntity)entity.Entity).UpdatedBy = userId;
                }
                else if (entity.State == EntityState.Modified)
                {
                    if (((BaseAdvanceEntity)entity.Entity) != null)
                    {
                        entity.Property("CreatedDate").IsModified = false;
                        entity.Property("CreatedBy").IsModified = false;
                        ((BaseAdvanceEntity)entity.Entity).UpdatedDate = DateTime.Now;
                        ((BaseAdvanceEntity)entity.Entity).UpdatedBy = userId;
                    }

                }

            }
        }
    }
}