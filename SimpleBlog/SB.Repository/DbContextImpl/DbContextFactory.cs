﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;

namespace SB.Repository.DbContextImpl
{
    public class DbContextFactory : IDesignTimeDbContextFactory<SBDbContext>
    {
        public SBDbContext CreateDbContext(string[] args)
        {
            string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new String[] { @"bin\" }, StringSplitOptions.None)[0];
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(projectPath)
                .AddJsonFile("connectionString.json")
                .Build();

            string connectionString = configuration.GetConnectionString("DefaultConnection");

            var optionsBuilder = new DbContextOptionsBuilder<SBDbContext>();
            optionsBuilder.UseSqlServer(connectionString);
            var serviceCollection = new Microsoft.Extensions.DependencyInjection.ServiceCollection();
            return new SBDbContext(optionsBuilder.Options, new HttpContextAccessor());
        }
    }
}
