﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using SB.Repository.DbContextImpl;
using SB.Repository.Entities;
using SB.Repository.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace SB.Repository.Repositories
{
    public class Repository<TContext, TEntity, TId> : IRepository<TEntity, TId>
        where TEntity : BaseEntity<TId>
        where TContext : SBDbContext
    {
        private readonly TContext context;

        public Repository(TContext _context)
        {
            this.context = _context;
        }

        private DbSet<TEntity> GetEntities()
        {
            return this.context.Set<TEntity>();
        }

        public IQueryable<TEntity> Query()
        {
            return this.GetEntities().AsQueryable();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return this.GetEntities().ToList();
        }

        public TEntity GetById(TId id)
        {
            return this.Query().SingleOrDefault(x => x.Id.Equals(id));
        }

        public EntityEntry Insert(TEntity entity)
        {
            return this.GetEntities().Add(entity);
        }

        public void Update(TEntity entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
            {
                this.context.Attach(entity);
            }
            this.context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
            {
                this.context.Attach(entity);
            }
            this.GetEntities().Remove(entity);
        }

        public int SaveChanges()
        {
            return this.context.SaveChanges();
        }

    }

    //default repository with FreelanceDbContext and int Id
    public class Repository<TEntity> : Repository<SBDbContext, TEntity, int>, IRepository<TEntity>
        where TEntity : BaseEntity<int>
    {
        public Repository(SBDbContext context) : base(context)
        {
        }
    }
}
