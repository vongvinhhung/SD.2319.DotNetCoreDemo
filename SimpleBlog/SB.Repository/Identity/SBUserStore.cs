﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using SB.Core.Identity;
using SB.Repository.DbContextImpl;

namespace SB.Repository.Identity
{
    public class SBUserStore : UserStore<User, Role, SBDbContext, int, UserClaim, UserRole, UserLogin, UserToken, RoleClaim>
    {
        public SBUserStore(SBDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
    }
}
