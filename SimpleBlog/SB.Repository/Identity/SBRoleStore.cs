﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using SB.Core.Identity;
using SB.Repository.DbContextImpl;
using System;
using System.Collections.Generic;
using System.Text;

namespace SB.Repository.Identity
{
    public class SBRoleStore : RoleStore<Role, SBDbContext, int, UserRole, RoleClaim>
    {
        public SBRoleStore(SBDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
    }
}
