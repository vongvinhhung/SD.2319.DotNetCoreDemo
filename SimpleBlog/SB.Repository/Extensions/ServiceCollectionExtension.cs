﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SB.Repository.DbContextImpl;
using SB.Repository.Repositories;
using SB.Repository.Repositories.Interfaces;

namespace SB.Repository.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddDataModule(this IServiceCollection services, IConfiguration configuration)
        {
            //Configure DbContext
            services.AddDbContext<SBDbContext>(options =>
            {
                options.UseSqlite(configuration.GetConnectionString("DefaultConnection"));
            });

            //Repository
            //Scoped = instance per request
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,,>));

            return services;
        }
    }
}
