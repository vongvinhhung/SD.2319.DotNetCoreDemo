﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SB.Repository.Entities
{
    public class Category:BaseAdvanceEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual IEnumerable<Post> Posts { get; set; }
    }
}
