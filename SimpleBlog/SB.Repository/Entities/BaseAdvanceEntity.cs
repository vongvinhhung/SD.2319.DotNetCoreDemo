﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SB.Repository.Entities
{
    public class BaseAdvanceEntity : BaseEntity<int>
    {
        public BaseAdvanceEntity()
        {
            CreatedDate = new DateTime();
        }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

    }
}
