﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SB.Repository.Entities
{
    public class Post : BaseAdvanceEntity
    {
        public string Title { get; set; }
        public string Detail { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}
