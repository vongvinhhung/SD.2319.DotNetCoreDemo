﻿
namespace SB.Core.DbInitializer
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
