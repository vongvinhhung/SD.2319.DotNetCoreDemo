﻿using SB.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace SB.Core.Services
{
    public interface ICategory:IService
    {
        List<CategoryDto> GetAll();

        bool Add(CategoryDto dto);
        bool Edit(CategoryDto dto);
    }
}
