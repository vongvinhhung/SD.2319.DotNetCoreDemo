﻿using SB.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace SB.Core.Services
{
    public interface IPost : IService
    {
        List<PostDto> GetAll();

        List<PostDto> GetAllByCategoryId(int categoryId);

        bool Add(PostDto dto);

        PostDto Get(int id);
    }
}
