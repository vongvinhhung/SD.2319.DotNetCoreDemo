﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SB.Core.DTO
{
    public class BaseAdvanceDto : BaseDto<int>
    {
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
