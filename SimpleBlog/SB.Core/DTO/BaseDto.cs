﻿
namespace SB.Core.DTO
{
    public class BaseDto<TId>
    {
        public TId Id { get; set; }
    }
}
