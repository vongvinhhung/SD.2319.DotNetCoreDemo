﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SB.Core.DTO
{
    public class CategoryDto: BaseAdvanceDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
