﻿
namespace SB.Core.DTO
{
    public class PostDto : BaseAdvanceDto
    {
        public string Detail { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
    }
}
