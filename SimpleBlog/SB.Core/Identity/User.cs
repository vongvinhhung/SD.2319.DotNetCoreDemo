﻿using Microsoft.AspNetCore.Identity;

namespace SB.Core.Identity
{
    public class User : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Province { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
    }
}
