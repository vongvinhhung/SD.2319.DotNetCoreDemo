﻿using AutoMapper;
using SB.Core.DTO;
using SimpleBlog.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.AutoMapper
{
    public class MappingConfiguration : Profile
    {
        public MappingConfiguration()
        {
            CreateMap<CategoryViewModel, CategoryDto>().ReverseMap();
            CreateMap<PostViewModel, PostDto>().ReverseMap();
        }
    }
}
