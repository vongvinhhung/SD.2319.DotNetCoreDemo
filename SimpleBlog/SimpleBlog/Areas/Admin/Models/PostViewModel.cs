﻿
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SimpleBlog.Areas.Admin.Models
{
    public class PostViewModel
    {
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public string Title { get; set; }
        public SelectList CategoryList { get; set; }
    }
}
