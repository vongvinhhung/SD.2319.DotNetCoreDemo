﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SB.Core.DTO;
using SB.Core.Services;
using SimpleBlog.Areas.Admin.Models;
using SimpleBlog.Datatable;
using System.Security.Claims;
using SB.Repository.Entities;
using SB.Service.Identity;

namespace SimpleBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class CategoryController : Controller
    {
        private readonly ICategory _category;
        private readonly IMapper _mapper;
        private readonly SBUserManager _userManager;
        public CategoryController(ICategory category, IMapper mapper, SBUserManager userManager)
        {
            _category = category;
            _mapper = mapper;
            _userManager = userManager;
    }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult GetAll(DataTableParameterModel param)
        {
            var data = _category.GetAll();
            
            return Json(
                new
                {
                    draw = param.Draw,
                    recordsTotal = data.Count,
                    recordsFiltered = data.Count,
                    data = data
                });
        }

        public JsonResult Add(CategoryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(false);
            }

            var dto = _mapper.Map<CategoryDto>(model);
            return Json(_category.Add(dto));
        }

        public JsonResult Edit(CategoryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(false);
            }

            var dto = _mapper.Map<CategoryDto>(model);
            return Json(_category.Edit(dto));
        }
    }
}