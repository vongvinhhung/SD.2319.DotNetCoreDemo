﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SB.Core.DTO;
using SB.Core.Services;
using SimpleBlog.Areas.Admin.Models;
using SimpleBlog.Datatable;

namespace SimpleBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class PostController : Controller
    {
        ICategory category;
        IPost post;
        IMapper mapper;
        public PostController(ICategory _category, IMapper _mapper, IPost _post)
        {
            category = _category;
            mapper = _mapper;
            post = _post;
        }

        public IActionResult Index()
        {
            var categories = category.GetAll();

            var model = new PostViewModel()
            {
                CategoryList = new SelectList(categories, "Id", "Name")
            };

            return View(model);
        }

        public JsonResult Add(PostViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(false);
            }

            var dto = mapper.Map<PostDto>(model);

            return Json(post.Add(dto));
        }


        public JsonResult GetAll(DataTableParameterModel param)
        {
            var data = post.GetAll();

            return Json(
                new
                {
                    draw = param.Draw,
                    recordsTotal = data.Count,
                    recordsFiltered = data.Count,
                    data = data
                });
        }
    }
}