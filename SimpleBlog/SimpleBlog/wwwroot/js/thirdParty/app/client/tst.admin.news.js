﻿(function ($, TST) {
    TST.Client = TST.Admin || {}

    TST.Client.News = {
        TableId: "#news-table",
        RegisterEvent: function () {

        },
        AddPopup: {
            registerEvent: {
            },
            RegisterTagsEvent: function () {
                
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            table.DataTable({
                dom: "lrtip",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("News", "GetData")
                },
                columns: [
                    {
                        title: "",
                        data: "Newses[0]",
                        render: function (data, type, full, meta) {
                            return data.Description;
                        }
                    },
                    {
                        title: "",
                        data: "Newses[1]",
                        render: function (data, type, full, meta) {
                            return data.Description;
                        }
                    },
                    {
                        title: "",
                        data: "Newses[2]",
                        render: function (data, type, full, meta) {
                            return data.Description;
                        }
                    },
                    {
                        title: "",
                        data: "Newses[3]",
                        render: function (data, type, full, meta) {
                            return data.Description;
                        }
                    }
                ],
                serverSide: true,
                "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    //event for show/hide
                    //TST.Admin.News.RegisterCheckBoxActive();
                    //TST.Admin.News.RegisterFilterDatatable(table);
                }
            });
            
        },
        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});