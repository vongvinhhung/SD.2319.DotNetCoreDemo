﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.Video = {
        RegisterEvent: function () {

            $("#new-video-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "Video",
                    action: "AddVideoPopup",
                    width: 500,
                    callback: function () {
                        TST.Admin.Video.AddVideoPopup.registerEvent();
                        TST.Admin.Video.AddVideoPopup.RegisterTagsEvent("#Video_Tags");
                    }
                });
            });

            $("#video-categories-ddl").on("change", function () {
                var dataTable = $("#video-table").DataTable();
                var filterValue = $(this).val();
                if (filterValue) {
                    dataTable.search(filterValue).draw();
                }
            });
        },

        AddVideoPopup: {
            registerEvent: function () {
                var saveBtn = $("#save-video-btn");
                if (saveBtn) {
                    saveBtn.on("click", function () {
                        var form = $("#add-video-form");
                        if (form.isValid()) {
                            var formData = TST.Helpers.formToObject(form, "Video");
                            formData.Tags = $("#Video_Tags").tagsinput("items").toString();

                            TST.Helpers.ajax({
                                controller: "Video",
                                action: "AddVideo",
                                data: formData,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.Video.ReloadVideoTable();
                                    TST.Helpers.showAlert("Thêm thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

                var editBtn = $("#edit-video-btn");
                if (editBtn) {
                    editBtn.on("click", function () {
                        var form = $("#edit-video-form");
                        if (form.isValid()) {
                            var formData = TST.Helpers.formToObject(form, "Video");
                            formData.Tags = $("#Video_Tags").tagsinput('items').toString();

                            TST.Helpers.ajax({
                                controller: "Video",
                                action: "EditVideo",
                                data: formData,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.Video.ReloadVideoTable();
                                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }
            },

            RegisterTagsEvent: function (control) {
                var url = TST.Helpers.resolveUrl("Tags", "GetAllTags");
                var tags = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Name'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch: {
                        url: url,
                        filter: function (tags) {
                            return $.map(tags, function (tag) {
                                return {
                                    Name: tag.Name
                                };
                            });
                        },
                        cache: false
                    }
                });
                tags.initialize();

                $(control).tagsinput({
                    typeaheadjs: {
                        name: "tags",
                        source: tags.ttAdapter(),
                        displayKey: 'Name',
                        valueKey: 'Name'
                    }
                });
            }
        },


        InitVideoTable: function () {
            var videoTable = $("#video-table");
            var deleteButton = $("#delete-video-btn");

            videoTable.DataTable({
                order: [[7, "desc"]],
                ajax: {
                    url: TST.Helpers.resolveUrl("Video", "GetVideosData")
                },
                columns: [
                    {
                        title: "Link video",
                        data: "Link",
                        render: function (data, type, full, meta) {
                            var url = "";

                            var statusCode = TST.Admin.Video.CheckUrl(data);

                            if (statusCode !== 404 && data) {
                                url = data.replace("watch?v=", "embed/");
                                return `<iframe width="250" height="150" id="iframe-${full.Id}" src="${url}" style="display: block; margin: 0 auto;"></iframe>`;
                            }
                            return `<img src="/TST/quan-ly/General/LoadImage/?image=404-image.png" width="235" height="125" style="display: block; margin: 0 auto;">`;
                        }
                    },
                    {
                        title: "Tên",
                        data: "Name"

                    },
                    {
                        title: "Mô tả",
                        data: "Description",
                        render: function (data, type, full, meta) {
                            if (data) {
                                return `<p style="word-wrap: break-word; max-width: 200px;">${data}</p>`;
                            }
                            return "";
                        }
                    },
                    {
                        title: "Danh mục",
                        data: "Lookup.Description",
                    },
                    {
                        title: "Tags",
                        data: "Tags",
                        render: function (data, type, full, meta) {
                            if (data) {
                                return `<input class ="form-control tag-column" data-role="tagsinput" value="${data}">`;
                            }
                            return "";
                        }
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    },
                    {
                        title: "Id",
                        data: "Id",
                        visible: false
                    }
                ],
                //serverSide: true,
                responsive: true,
                "autoWidth": true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    videoTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    videoTable.on("dblclick", "tr", function () {
                        var selectedItem = videoTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "Video",
                                action: "EditVideoPopup",
                                data: { videoId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.Video.AddVideoPopup.RegisterTagsEvent("#Video_Tags");
                                    TST.Admin.Video.AddVideoPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.Video.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa video",
                            message: "Bạn có muốn xóa video này?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "Video",
                                    action: "DeleteVideo",
                                    data: { videoId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.Video.ReloadVideoTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    videoTable.on("draw.dt", function () {
                        TST.Admin.Video.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa video",
                                message: "Bạn có muốn xóa video này?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        controller: "Video",
                                        action: "DeleteVideo",
                                        data: { videoId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.Video.ReloadVideoTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                },

                "drawCallback": function (settings) {
                    //show tag in datatable 
                    $('.tag-column', "#video-table").each(function () {
                        $(this).tagsinput();
                        $(this).prev().removeClass('bootstrap-tagsinput');
                        $(this).prev().find("input").remove();
                    });
                }
            });
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox("#video-table");

            $("input", "#video-table").on("ifChecked", function (event) {
                TST.Admin.Video.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input", "#video-table").on("ifUnchecked", function (event) {
                TST.Admin.Video.UpdateActiveStatus($(this).data("id"), false);
            });
        },

        UpdateActiveStatus: function (videoId, isActive) {
            TST.Helpers.ajax({
                controller: "Video",
                action: "UpdateVideoActiveStatus",
                data: {
                    videoId: videoId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadVideoTable: function () {
            $("#video-table").DataTable().ajax.reload();
            $("#delete-video-btn").addClass("disabled");
        },

        CheckUrl: function (url) {
            var video_id = url.split('v=')[1];
            if (!video_id) return 404;
            var ampersandPosition = video_id.indexOf('&');
            if (ampersandPosition !== -1) {
                video_id = video_id.substring(0, ampersandPosition);
            }
            var result = 404;

            $.ajaxSetup({
                async: false
            });
            $.getJSON("https://www.googleapis.com/youtube/v3/videos?id=" + video_id + "&key=AIzaSyA1lHsUXK4d2uxxqfkPMV7OBA0pmuBlyRE&part=id",
              function (data, status, xhr) {
                  if (data.items.length > 0) result = 1;
              })
            .fail(function (xhr, errorType, exception) {
                var errorMessage = exception || xhr.statusText || xhr.responseText;
                console.log(errorMessage);
            });
            $.ajaxSetup({
                async: true
            });

            return result;
        }
    }

})(jQuery, window.TST = window.TST || {});