﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.ContactEmail = {
        TableId: "#contact-email-table",
        AddPopup: {
            registerEvent: function () {
                $("#save-contact-email-btn").on("click", function () {
                    var form = $("#add-contact-email-form");
                    if (form.isValid()) {
                        var formData = new FormData(form[0]);
                        TST.Helpers.closePopup();
                        TST.Helpers.ajax({
                            controller: "ContactEmail",
                            action: "Save",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                                TST.Admin.ContactEmail.ReloadTable();
                            }
                        });
                    }
                });
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            var deleteButton = $("#delete-contact-email-btn");
            table.DataTable({
                order: [[5, "asc"]],
                dom: "lrtip",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("ContactEmail", "GetData")
                },
                columns: [
                    {
                        title: "Tiêu đề",
                        data: "Title"
                    },
                    {
                        title: "Tên",
                        data: "Name"
                    },
                    {
                        title: "Điện thoại",
                        data: "Phone"
                    },
                    {
                        title: "Email",
                        data: "Email"
                    },
                    {
                        title: "Ngày tạo",
                        data: "CreatedDate",
                        render: function (data, type, full, meta) {
                            if (data === null) return "";
                            var pattern = /Date\(([^)]+)\)/;
                            var results = pattern.exec(data);
                            var dt = new Date(parseFloat(results[1]));
                            return dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear();
                        }
                    },
                    {
                        title: "Trạng thái",
                        data: "Status"
                    }
                ],
                serverSide: true,
                "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "ContactEmail",
                                action: "Detail",
                                data: { contactEmailId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.ContactEmail.AddPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.ContactEmail.RegisterFilterDatatable(table);
                }
            });
        },
        RegisterFilterDatatable: function (table) {
            $('#contact-email-by-title').on('keyup', function () {
                table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});