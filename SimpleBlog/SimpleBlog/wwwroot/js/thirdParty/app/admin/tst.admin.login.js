﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.Login = {
		registerEvent: function () {

	    	$("input").iCheck({
	    		checkboxClass: "icheckbox_square-blue",
	    		radioClass: "iradio_square-blue",
	    		increaseArea: "20%"
	    	});

	    	$("#login-submit").click(function () {
	    		var form = $("#login-form");
	    		var data = TST.Helpers.formToObject(form, "LoginViewModel");
	    		var returnUrl = TST.Helpers.getUrlParameter("ReturnUrl");
	    		if (form.valid()) {
	    		    TST.Helpers.ajax({
	    				controller: "Account",
	    				action: "Login",
	    				data: {
	    					model: data,
	    					returnUrl: returnUrl,
	    					__RequestVerificationToken: data.__RequestVerificationToken
	    				},
	    				method: "POST",
	    				callback: function (e) {
	    				}
	    			});
	    		}
	    	});
	    }
	}

})(jQuery, window.TST = window.TST || {});
