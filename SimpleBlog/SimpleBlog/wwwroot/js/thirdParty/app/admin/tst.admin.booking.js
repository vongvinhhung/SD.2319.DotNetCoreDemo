﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.Booking = {
        TableId: "#booking-table",
        RegisterEvent: function () {
            $('#booking-by-Date').datetimepicker({
                format: 'd/m/Y',
                timepicker: false,
                //onChangeDateTime: function (dp, $input) {
                //    alert($input.val());
                //}
            });
        },
        AddPopup: {
            registerEvent: function () {
                //$("#save-booking-btn").on("click", function () {
                //    var form = $("#add-booking-form");
                //    if (form.isValid()) {
                //        var formData = new FormData(form[0]);
                //        TST.Helpers.closePopup();
                //        TST.Helpers.ajax({
                //            controller: "Booking",
                //            action: "Save",
                //            processData: false,
                //            contentType: false,
                //            data: formData,
                //            method: "POST",
                //            callback: function (data) {
                //                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                //                TST.Admin.Booking.ReloadTable();
                //            }
                //        });
                //    }
                //});
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            var deleteButton = $("#delete-booking-btn");
            table.DataTable({
                order: [[7, "asc"]],
                dom: "lrtip",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("Booking", "GetData")
                },
                columns: [
                    {
                        title: "Mã tour",
                        data: "TourGroupCode"
                    },
                    {
                        title: "Tên khách hàng",
                        data: "Fullname"
                    },
                    {
                        title: "Điện thoại",
                        data: "Phone",
                        width: "100px",
                    },
                    {
                        title: "Email",
                        data: "Email"
                    },
                    {
                        title: "Ngày đặt",
                        data: "CreatedDate",
                        render: function (data, type, full, meta) {
                            if (data === null) return "";
                            var pattern = /Date\(([^)]+)\)/;
                            var results = pattern.exec(data);
                            var dt = new Date(parseFloat(results[1]));
                            var day = dt.getDate();
                            var month = dt.getMonth() + 1;
                            var year = dt.getFullYear();
                            if (day < 10) {
                                day = "0" + day;
                            }
                            if (month < 10) {
                                month = "0" + month;
                            }
                            return day + "/" + month + "/" + year;
                        },
                        width: "100px"
                    },
                    {
                        title: "Tổng tiền",
                        data: "Price",
                        render: function (data, type, full, meta) {
                            return data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + " đ";
                        }
                    },
                    {
                        title: "Trạng thái",
                        data: "Status",
                        width: "100px"
                    },
                    {
                        title: "Status",
                        data: "StatusId",
                        visible: false
                    }
                ],
                serverSide: true,
                "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });
                    TST.Admin.Booking.RegisterFilterDatatable(table);
                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        //if (selectedItem) {
                        //    TST.Helpers.ajaxOpenPopup({
                        //        controller: "Booking",
                        //        action: "Detail",
                        //        data: { bookingId: selectedItem.Id },
                        //        width: 500,
                        //        callback: function () {
                        //            TST.Admin.Booking.AddPopup.registerEvent();
                        //        }
                        //    });
                        //}

                        if (selectedItem) {
                            TST.Helpers.ajaxLoadNewContent({
                                controller: "Booking",
                                action: "BookingDetail",
                                data: { bookingId: selectedItem.Id },
                                callback: function () {
                                    TST.Admin.Booking.AddPopup.registerEvent();
                                }
                            });
                        }
                    });
                    table.on("draw.dt", function () {

                    });
                }
            });
        },
        RegisterFilterDatatable: function (table) {
            $('#booking-by-Code').on('keyup', function () {
                table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
            });
            $('#booking-by-Email').on('keyup', function () {
                table.DataTable().columns(3)
                    .search(this.value)
                    .draw();
            });
            $('#booking-by-Name').on('keyup', function () {
                table.DataTable().columns(1)
                    .search(this.value)
                    .draw();
            });
            $('#booking-by-Phone').on('keyup', function () {
                table.DataTable().columns(2)
                    .search(this.value)
                    .draw();
            });

            $('#booking-by-Date').on('change', function () {
                var date = $("#booking-by-Date").datetimepicker('getValue');
                var searchText = "";
                if (date != null) {
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    searchText = `${month}/${day}/${year}`;
                }
                table.DataTable().columns(4)
                    .search(searchText)
                    .draw();
            });

            $('#booking-by-Status').on('change', function () {
                table.DataTable().columns(6)
                    .search($("#booking-by-Status option:selected").val())
                    .draw();
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});