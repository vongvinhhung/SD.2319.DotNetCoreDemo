﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.Image = {
        RegisterEvent: function () {

            $("#new-image-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "Image",
                    action: "AddImagePopup",
                    width: 500,
                    callback: function () {
                        TST.Admin.Image.AddImagePopup.registerEvent();
                    }
                });
            });

            $("#back-btn").on("click", function () {
                TST.Helpers.ajaxLoadNewContent({
                    controller: "ImageGallery",
                    action: "Index",
                    method: "GET"
                });
            });
        },

        AddImagePopup: {
            registerEvent: function () {
                $("#add-image").on("change", function () {
                    var previewBox = $("#add-image-preview .box.box-primary").empty();
                    var preview = $("#add-image-preview");

                    preview.show();

                    if (this.files && this.files.length > 0) {
                        $.each(this.files, readAndPreview);
                    } else {
                        preview.hide();
                    }

                    function readAndPreview(i, file) {
                        if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                            preview.hide();
                        }
                        var reader = new FileReader();
                        $(reader).on("load", function () {
                            previewBox.append($("<img/>", { src: this.result, height: 100 }));
                        });
                        reader.readAsDataURL(file);
                    }
                });

                var saveBtn = $("#save-image-btn");
                if (saveBtn) {
                    saveBtn.on("click", function () {
                        var form = $("#add-image-form");
                        if (form.isValid()) {
                            var formData = new FormData(form[0]);
                            formData.append("Image.GalleryId", $("#gallery-id").val());
                            TST.Helpers.ajax({
                                controller: "Image",
                                action: "AddImage",
                                processData: false,
                                contentType: false,
                                data: formData,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.Image.ReloadImageTable();
                                    TST.Helpers.showAlert("Thêm thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

                var editBtn = $("#edit-image-btn");
                if (editBtn) {
                    editBtn.on("click", function () {
                        var form = $("#edit-image-form");
                        if (form.isValid()) {
                            var formData = new FormData(form[0]);
                            formData.append("Image.GalleryId", $("#gallery-id").val());
                            TST.Helpers.ajax({
                                controller: "Image",
                                action: "EditImage",
                                processData: false,
                                contentType: false,
                                data: formData,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.Image.ReloadImageTable();
                                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }
            }
        },


        InitImageTable: function () {
            var imageTable = $("#image-table");
            var deleteButton = $("#delete-image-btn");
            var fileUrl = $("#file-url").val();
            var galleryId = $("#gallery-id").val();

            imageTable.DataTable({
                order: [[4, "desc"]],
                dom: "lrtip",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("Image", "GetAllImagesByGalleryId"),
                    data: { galleryId: galleryId }
                },
                columns: [
                    {
                        title: "Hình ảnh",
                        data: "PathName",
                        render: function (data, type, full, meta) {
                            return `<img src="${fileUrl}/?image=${data}" height="75" style="display: block; margin: 0 auto;">`;
                        }
                    },
                    {
                        title: "Tên",
                        data: "Title"
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    },
                    {
                        title: "Id",
                        data: "Id",
                        visible: false
                    }
                ],
                serverSide: true,
                "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {

                    //select event
                    imageTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });


                    imageTable.on("dblclick", "tr", function () {
                        var selectedItem = imageTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "Image",
                                action: "EditImagePopup",
                                data: { imageId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.Image.AddImagePopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.Image.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa hình",
                            message: "Bạn có muốn xóa hình?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "Image",
                                    action: "DeleteImage",
                                    data: { imageId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.Image.ReloadImageTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    imageTable.on("draw.dt", function () {
                        TST.Admin.Image.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa hình",
                                message: "Bạn có muốn xóa hình?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        controller: "Image",
                                        action: "DeleteImage",
                                        data: { imageId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.Image.ReloadImageTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                },

                "drawCallback": function (settings) {
                    //show tag in datatable 
                    $(".tag-column", "#image-table").each(function () {
                        $(this).tagsinput();
                        $(this).prev().removeClass('bootstrap-tagsinput');
                        $(this).prev().find("input").remove();
                    });
                }
            });
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox("#image-table");

            $("input", "#image-table").on("ifChecked", function (event) {
                TST.Admin.Image.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input", "#image-table").on("ifUnchecked", function (event) {
                TST.Admin.Image.UpdateActiveStatus($(this).data("id"), false);
            });
        },

        UpdateActiveStatus: function (imageId, isActive) {
            TST.Helpers.ajax({
                controller: "Image",
                action: "UpdateImageActiveStatus",
                data: {
                    imageId: imageId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadImageTable: function () {
            $("#image-table").DataTable().ajax.reload();
            $("#delete-image-btn").addClass("disabled");
        }
    }

})(jQuery, window.TST = window.TST || {});