﻿(function ($, TST) {
	TST.Admin = TST.Admin || {}

	TST.Admin.Area = {
		TableId: "#area-table",
		RegisterEvent: function () {
			$("#new-area-btn").on("click", function () {
				TST.Helpers.ajaxOpenPopup({
					url: TST.Helpers.resolveUrl("Area", "Detail"),
					width: 900,
					callback: function () {
						TST.Admin.Area.AddPopup.registerEvent();
					}
				});
			});
		},
		AddPopup: {
			registerEvent: function () {
				$("#save-area-btn").on("click", function () {
					var form = $("#add-area-form");
					if (form.isValid()) {
					    CKEDITOR.instances["areaEmail"].updateElement();
					    CKEDITOR.instances["areaDescription"].updateElement();
						var formData = new FormData(form[0]);
						TST.Helpers.closePopup();
						TST.Helpers.ajax({
							controller: "Area",
							action: "Save",
							processData: false,
							contentType: false,
							data: formData,
							method: "POST",
							callback: function (data) {
								TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
								TST.Admin.Area.ReloadTable();
							}
						});
					}
				});
			}
		},
		InitTable: function () {
			var table = $(this.TableId);
			var deleteButton = $("#delete-area-btn");
			table.DataTable({
				//dom: "lrtip",
				//select: {
				//	style: "single",
				//	info: false
				//},
				ajax: {
					url: TST.Helpers.resolveUrl("Area", "GetData")
				},
				columns: [
					{
						title: "Tên",
						data: "Title"
					},
                    {
                    	title: "Mô tả",
                    	data: "Description"
                    },
					{
						title: "Nước ngoài",
						data: "IsAbroad",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" area-id=${full.Id} value=${data} ${checked} data-type="isAbroad">`;
						}
					},
                    {
                    	title: "Hiển thị",
                    	data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" area-id=${full.Id} value=${data} ${checked} data-type="isShow">`;
                    	}
                    },
                    //{
                    //	title: "Xóa",
                    //	data: "Id",
                    //	render: function (data, type, full, meta) {
                    //	    return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                    //	}
                    //}
                    {
                        title: "",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return TST.Templates.DeleteButton(data);
                        },
                        "orderable": "true"
                    }
				],
				//serverSide: true,
				//"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
				"initComplete": function () {
					table.DataTable()
                        .on("select", function () {
                        	deleteButton.enable();
                        })
                        .on("deselect", function () {
                        	deleteButton.disable();
                        });

					table.on("dblclick", "tr", function () {
						var selectedItem = table.DataTable().row(this).select().data();
						if (selectedItem) {
							TST.Helpers.ajaxOpenPopup({
								controller: "Area",
								action: "Detail",
								data: { AreaId: selectedItem.Id },
								width: 900,
								callback: function () {
									TST.Admin.Area.AddPopup.registerEvent();
								}
							});
						}
					});

					//event for show/hide and isAbroad
					TST.Admin.Area.RegisterCheckBoxActive();
					$(".delete-btn").on("click", function () {
					    var id = $(this).data("index");
					    TST.Helpers.confirmDialog({
					        title: "Xóa Thị trường",
					        message: "Xác nhận xóa?",
					        callback: function () {
					            TST.Helpers.ajax({
					                url: TST.Helpers.resolveUrl("Area", "Delete"),
					                data: { areaId: id },
					                method: "POST",
					                callback: function () {
					                    TST.Admin.Area.ReloadTable();
					                    TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                    TST.Helpers.closePopup();
					                }
					            });
					        }
					    });
					});
					TST.Admin.Area.RegisterFilterDatatable(table);
					table.on("draw.dt", function () {
					    TST.Admin.Area.RegisterCheckBoxActive();
					    $(".delete-btn").on("click", function () {
					        var id = $(this).data("index");
					        TST.Helpers.confirmDialog({
					            title: "Xóa Thị trường",
					            message: "Xác nhận xóa?",
					            callback: function () {
					                TST.Helpers.ajax({
					                    url: TST.Helpers.resolveUrl("Area", "Delete"),
					                    data: { areaId: id },
					                    method: "POST",
					                    callback: function () {
					                        TST.Admin.Area.ReloadTable();
					                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                        TST.Helpers.closePopup();
					                    }
					                });
					            }
					        });
					    });
					});
				}
			});
		},

		RegisterCheckBoxActive: function () {
		    TST.Helpers.initCheckbox(this.TableId);

			$("input[data-type='isShow']", this.TableId).on("ifChecked", function (event) {
				TST.Admin.Area.UpdateActiveStatus(event.currentTarget.getAttribute("area-id"), true);
			});

			$("input[data-type='isShow']", this.TableId).on("ifUnchecked", function (event) {
				TST.Admin.Area.UpdateActiveStatus(event.currentTarget.getAttribute("area-id"), false);
			});

			$("input[data-type='isAbroad']", this.TableId).on("ifChecked", function (event) {
				TST.Admin.Area.UpdateAbroadStatus(event.currentTarget.getAttribute("area-id"), true);
			});

			$("input[data-type='isAbroad']", this.TableId).on("ifUnchecked", function (event) {
				TST.Admin.Area.UpdateAbroadStatus(event.currentTarget.getAttribute("area-id"), false);
			});
		},
		RegisterFilterDatatable: function (table) {
		    $('#area-by-Title').on('keyup', function () {
		        table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
		    });
		    $('#area-by-Description').on('keyup', function () {
		        table.DataTable().columns(1)
                    .search(this.value)
                    .draw();
		    });
		},
		UpdateActiveStatus: function (areaId, isActive) {
			TST.Helpers.ajax({
				url: TST.Helpers.resolveUrl("Area", "UpdateActiveStatus"),
				data: {
					AreaId: areaId,
					isActive: isActive
				},
				method: "POST",
				callback: function (data) {
					TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
				}
			});
		},

		UpdateAbroadStatus: function (areaId, isAbroad) {
			TST.Helpers.ajax({
				url: TST.Helpers.resolveUrl("Area", "UpdateAbroadStatus"),
				data: {
					AreaId: areaId,
					isAbroad: isAbroad
				},
				method: "POST",
				callback: function (data) {
					TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
				}
			});
		},

		ReloadTable: function () {
		    $(this.TableId).DataTable().ajax.reload();
            $("delete-area-btn").disable();
		}
	}

})(jQuery, window.TST = window.TST || {});