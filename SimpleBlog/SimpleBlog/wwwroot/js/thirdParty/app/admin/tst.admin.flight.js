﻿(function ($, TST) {
	TST.Admin = TST.Admin || {}

	TST.Admin.Flight = {
		TableId: "#flight-table",
		RegisterEvent: function () {
			$("#new-flight-btn").on("click", function () {
				TST.Helpers.ajaxOpenPopup({
				    url: TST.Helpers.resolveUrl("Flight", "Detail"),
                    data: { flightId: 0, tourGroupId: $("#TourGroup_Id").val() },
					width: 500,
					callback: function () {
						TST.Admin.Flight.AddPopup.registerEvent();
					}

				});
			});
		},
		AddPopup: {
		    registerEvent: function () {
		        $('.datepicker').removeAttr("data-val-date");
		        $('.datepicker').datetimepicker({
		            format: 'd/m/Y H:i'
		        });
				$("#save-flight-btn").on("click", function () {
					var form = $("#add-flight-form");
					if (form.isValid()) {
					    var formData = new FormData(form[0]);

					    var part1 = formData.get('StartTime').split('/');
					    //var startTime = new Date(part1[2], part1[1] - 1, part1[0]).toUTCString();
					    formData.set('StartTime', part1[1] + "/" + part1[0] + "/" + part1[2]);

					    var part2 = formData.get('ArrivalTime').split('/');
					    //var latestDate = new Date(part2[2], part2[1] - 1, part2[0]).toUTCString();
					    formData.set('ArrivalTime', part2[1] + "/" + part2[0] + "/" + part2[2]);

						TST.Helpers.closePopup();
						TST.Helpers.ajax({
							controller: "Flight",
							action: "Save",
							processData: false,
							contentType: false,
							data: formData,
							method: "POST",
							callback: function (data) {
								TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
								TST.Admin.Flight.ReloadTable();
							}
						});
					}
				});
			}
		},
		InitTable: function () {
			var table = $(this.TableId);
			var deleteButton = $("#delete-flight-btn");
			table.DataTable({
				ajax: {
				    url: TST.Helpers.resolveUrl("Flight", "GetData"),
				    data: { tourGroupId: $("#TourGroup_Id").val() }
				},
				columns: [
					{
						title: "Mã",
						data: "FlightCode"
					},
                    {
                    	title: "Đi/Về",
                    	data: "IsGoAway",
                        render: function (data, type, full, meta) {
							return data === true ? "Đi" : "Về";
						}
                    },
                    {
                        title: "Giờ đi",
                        data: "StartTime",
                        render: function (data, type, row) {
                            var d = /\/Date\((\d*)\)\//.exec(data);
                            return (d) ? TST.Admin.Flight.formatDate(new Date(+d[1])) : "";
                        }
                    },
                    {
                        title: "Giờ đến",
                        data: "ArrivalTime",
                        render: function (data, type, row) {
                            var d = /\/Date\((\d*)\)\//.exec(data);
                            return (d) ? TST.Admin.Flight.formatDate(new Date(+d[1])) : "";
                        }
                    },
                    {
                    	title: "Hiển thị",
                    	data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                    		return `<input type="checkbox" flight-id=${full.Id} value=${data} data-type="isActive" ${checked}>`;
                    	}
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
				],
				serverSide: true,
				"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
				"initComplete": function () {
					table.DataTable()
                        .on("select", function () {
                        	deleteButton.enable();
                        })
                        .on("deselect", function () {
                        	deleteButton.disable();
                        });

					table.on("dblclick", "tr", function () {
						var selectedItem = table.DataTable().row(this).select().data();
						if (selectedItem) {
							TST.Helpers.ajaxOpenPopup({
								controller: "Flight",
								action: "Detail",
								data: { flightId: selectedItem.Id, tourGroupId: $("#TourGroup_Id").val() },
								width: 500,
								callback: function () {
									TST.Admin.Flight.AddPopup.registerEvent();
								}
							});
						}
					});

					//event for show/hide and isAbroad
					TST.Admin.Flight.RegisterCheckBoxActive();
					$(".delete-btn").on("click", function () {
					    var id = $(this).data("index");
					    TST.Helpers.confirmDialog({
					        title: "Xóa chuyến bay",
					        message: "Xác nhận xóa?",
					        callback: function () {
					            TST.Helpers.ajax({
					                url: TST.Helpers.resolveUrl("Flight", "Delete"),
					                data: { flightId: id },
					                method: "POST",
					                callback: function () {
					                    TST.Admin.Flight.ReloadTable();
					                    TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                    TST.Helpers.closePopup();
					                }
					            });
					        }
					    });
					});
					TST.Admin.Flight.RegisterFilterDatatable(table);
					table.on("draw.dt", function () {
					    TST.Admin.Flight.RegisterCheckBoxActive();
					    $(".delete-btn").on("click", function () {
					        var id = $(this).data("index");
					        TST.Helpers.confirmDialog({
					            title: "Xóa chuyến bay",
					            message: "Xác nhận xóa?",
					            callback: function () {
					                TST.Helpers.ajax({
					                    url: TST.Helpers.resolveUrl("Flight", "Delete"),
					                    data: { flightId: id },
					                    method: "POST",
					                    callback: function () {
					                        TST.Admin.Flight.ReloadTable();
					                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                        TST.Helpers.closePopup();
					                    }
					                });
					            }
					        });
					    });
					});
				}
			});
		},
		formatDate: function (date) {
		    var hours = date.getHours();
		    var minutes = date.getMinutes();
		    var ampm = hours >= 12 ? 'PM' : 'AM';
		    hours = hours % 12;
		    hours = hours ? hours : 12; // the hour '0' should be '12'
		    minutes = minutes < 10 ? '0' + minutes : minutes;
		    var strTime = hours + ':' + minutes + ' ' + ampm;
		    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + "  " + strTime;
		},
		RegisterCheckBoxActive: function () {
		    TST.Helpers.initCheckbox(this.TableId);

			$("input[data-type='isActive']", this.TableId).on("ifChecked", function (event) {
				TST.Admin.Flight.UpdateActiveStatus(event.currentTarget.getAttribute("flight-id"), true);
			});

			$("input[data-type='isActive']", this.TableId).on("ifUnchecked", function (event) {
				TST.Admin.Flight.UpdateActiveStatus(event.currentTarget.getAttribute("flight-id"), false);
			});

			//$("input[data-type='isGoAway']", this.TableId).on("ifChecked", function (event) {
			//	TST.Admin.Flight.UpdateFlightType(event.currentTarget.getAttribute("flight-id"), true);
			//});

			//$("input[data-type='isGoAway']", this.TableId).on("ifUnchecked", function (event) {
			//    TST.Admin.Flight.UpdateFlightType(event.currentTarget.getAttribute("flight-id"), false);
			//});
		},
		RegisterFilterDatatable: function (table) {
		    $('#flight-by-FlightCode').on('keyup', function () {
		        table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
		    });
		},
		UpdateActiveStatus: function (flightId, isActive) {
			TST.Helpers.ajax({
				url: TST.Helpers.resolveUrl("Flight", "UpdateActiveStatus"),
				data: {
					flightId: flightId,
					isActive: isActive
				},
				method: "POST",
				callback: function (data) {
					TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
				}
			});
		},

		//UpdateFlightType: function (flightId, isGoAway) {
		//	TST.Helpers.ajax({
		//	    url: TST.Helpers.resolveUrl("Flight", "UpdateFlightType"),
		//		data: {
		//			flightId: flightId,
		//			isGoAway: isGoAway
		//		},
		//		method: "POST",
		//		callback: function (data) {
		//			TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
		//		}
		//	});
		//},

		ReloadTable: function () {
		    $(this.TableId).DataTable().ajax.reload();
            $("delete-flight-btn").disable();
		}
	}

})(jQuery, window.TST = window.TST || {});