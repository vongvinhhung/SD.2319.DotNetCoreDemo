﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.CategoryPrice = {
        TableId: "#category-price-table",
        RegisterEvent: function () {
            $("#new-category-price-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    url: TST.Helpers.resolveUrl("CategoryPriceType", "Detail"),
                    width: 500,
                    callback: function () {
                        TST.Admin.CategoryPrice.AddPopup.registerEvent();
                    }
                });
            });
        },
        AddPopup: {
            registerEvent: function () {
                $("#save-category-price-btn").on("click", function () {
                    var form = $("#add-category-price-form");
                    if (form.isValid()) {
                        var formData = new FormData(form[0]);
                        TST.Helpers.closePopup();
                        TST.Helpers.ajax({
                            controller: "CategoryPriceType",
                            action: "Save",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                                TST.Admin.CategoryPrice.ReloadTable();
                            }
                        });
                    }
                });
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            var deleteButton = $("#delete-category-price-btn");
            table.DataTable({
                //dom: "lrtip",
                //select: {
                //    style: "single",
                //    info: false
                //},
                ajax: {
                    url: TST.Helpers.resolveUrl("CategoryPriceType", "GetData")
                },
                columns: [
                    {
                        title: "Mô tả",
                        data: "Description"
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked} data-type="isShow">`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
                ],
                //serverSide: true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "CategoryPriceType",
                                action: "Detail",
                                data: { categoryPriceTypeId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.CategoryPrice.AddPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.CategoryPrice.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa",
                            message: "Xác nhận xóa?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    url: TST.Helpers.resolveUrl("CategoryPriceType", "Delete"),
                                    data: { categoryPriceTypeId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.CategoryPrice.ReloadTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    TST.Admin.CategoryPrice.RegisterFilterDatatable(table);
                    table.on("draw.dt", function () {
                        TST.Admin.CategoryPrice.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa",
                                message: "Xác nhận xóa?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        url: TST.Helpers.resolveUrl("CategoryPriceType", "Delete"),
                                        data: { categoryPriceTypeId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.CategoryPrice.ReloadTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox(this.TableId);

            $("input[data-type='isShow']", this.TableId).on("ifChecked", function (event) {
                TST.Admin.CategoryPrice.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input[data-type='isShow']", this.TableId).on("ifUnchecked", function (event) {
                TST.Admin.CategoryPrice.UpdateActiveStatus($(this).data("id"), false);
            });

            $("input[data-type='isMain']", this.TableId).on("ifChecked", function (event) {
                TST.Admin.CategoryPrice.UpdateMainStatus($(this).data("id"), true);
            });

            $("input[data-type='isMain']", this.TableId).on("ifUnchecked", function (event) {
                TST.Admin.CategoryPrice.UpdateMainStatus($(this).data("id"), false);
            });
        },
        RegisterFilterDatatable: function (table) {
            $('#category-price-by-Description').on('keyup', function () {
                table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
            });
        },
        UpdateActiveStatus: function (categoryPriceId, isActive) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("CategoryPriceType", "UpdateActiveStatus"),
                data: {
                    categoryPriceTypeId: categoryPriceId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});