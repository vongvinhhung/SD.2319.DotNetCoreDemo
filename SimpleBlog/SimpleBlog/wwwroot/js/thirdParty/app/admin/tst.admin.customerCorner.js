﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.CustomerCorner = {
        TableId: "#customer-corner-table",
        RegisterEvent: function () {
            $("#new-customer-corner-btn").on("click", function () {
                TST.Helpers.ajaxOpenLargePopup({
                    url: TST.Helpers.resolveUrl("CustomerCorner", "Detail"),
                    width: 500,
                    callback: function () {
                        CKEDITOR.replace('CustomerCornerContent2');
                        TST.Admin.CustomerCorner.AddPopup.registerEvent();
                        TST.Admin.CustomerCorner.AddPopup.RegisterTagsEvent();
                    }
                });
            });
        },
        AddPopup: {
            registerEvent: function () {
                $("#add-customer-corner-image").on("change", function () {
                    var previewBox = $("#add-customer-corner-image-preview .box.box-primary").empty();
                    var preview = $("#add-customer-corner-image-preview");

                    preview.show();
                    function readAndPreview(i, file) {
                        if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                            preview.hide();
                        }
                        var reader = new FileReader();
                        $(reader).on("load", function () {
                            previewBox.append($("<img/>", { src: this.result, height: 100 }));
                        });
                        reader.readAsDataURL(file);
                    }
                    if (this.files && this.files.length > 0) {
                        $.each(this.files, readAndPreview);
                    } else {
                        preview.hide();
                    }
                });
                $("#save-customer-corner-btn").on("click", function () {
                    var form = $("#add-customer-corner-form");
                    if (form.isValid()) {
                        for (instance in CKEDITOR.instances) {
                            CKEDITOR.instances[instance].updateElement();
                        }
                        var formData = new FormData(form[0]);
                        TST.Helpers.ajax({
                            controller: "CustomerCorner",
                            action: "Save",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                                TST.Admin.CustomerCorner.ReloadTable();
                                TST.Helpers.closePopup();
                            }
                        });
                    }
                });
            },
            RegisterTagsEvent: function () {
                var url = TST.Helpers.resolveUrl("Tags", "GetAllTags");
                var tags = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Title'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch: {
                        url: url,
                        filter: function (tags) {
                            return $.map(tags, function (tag) {
                                return {
                                    Title: tag.Title
                                };
                            });
                        },
                        cache: false
                    }
                });
                tags.initialize();

                $("#Tags").tagsinput({
                    typeaheadjs: {
                        name: "tags",
                        source: tags.ttAdapter(),
                        displayKey: 'Title',
                        valueKey: 'Title'
                    }
                });
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            var deleteButton = $("#delete-customer-corner-btn");
            var fileUrl = $("#file-url").val();
            table.DataTable({
                //dom: "lrtip",
                //select: {
                //    style: "single",
                //    info: false
                //},
                ajax: {
                    url: TST.Helpers.resolveUrl("CustomerCorner", "GetData")
                },
                columns: [
                    {
                        title: "Tên",
                        data: "Name"
                    },
                    {
                        title: "Chú thích",
                        data: "Job"
                    },
                    {
                        title: "Hiển thị",
                        data: "IsShowed",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
                ],
                //serverSide: true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenLargePopup({
                                controller: "CustomerCorner",
                                action: "Detail",
                                data: { customerId: selectedItem.Id },
                                callback: function () {
                                    CKEDITOR.replace('CustomerCornerContent2');
                                    TST.Admin.CustomerCorner.AddPopup.registerEvent();
                                    TST.Admin.CustomerCorner.AddPopup.RegisterTagsEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.CustomerCorner.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa",
                            message: "Xác nhận xóa?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    url: TST.Helpers.resolveUrl("CustomerCorner", "Delete"),
                                    data: { customerId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.CustomerCorner.ReloadTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    TST.Admin.CustomerCorner.RegisterFilterDatatable(table);
                    table.on("draw.dt", function () {
                        TST.Admin.CustomerCorner.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa",
                                message: "Xác nhận xóa?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        url: TST.Helpers.resolveUrl("CustomerCorner", "Delete"),
                                        data: { customerId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.CustomerCorner.ReloadTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
            
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox(this.TableId);

            $("input", this.TableId).on("ifChecked", function (event) {
                TST.Admin.CustomerCorner.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input", this.TableId).on("ifUnchecked", function (event) {
                TST.Admin.CustomerCorner.UpdateActiveStatus($(this).data("id"), false);
            });
        },
        RegisterFilterDatatable: function (table) {
            $('#titleCustomerCornerFilter').on('keyup', function () {
                table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
            });
        },
        UpdateActiveStatus: function (customerId, isActive) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("CustomerCorner", "UpdateActiveStatus"),
                data: {
                    customerId: customerId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});