﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.NewsCategory = {
        TableId: "#news-category-table",
        RegisterEvent: function () {
            $("#new-news-category-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    url: TST.Helpers.resolveUrl("NewsCategory", "Detail"),
                    width: 500,
                    callback: function () {
                        TST.Admin.NewsCategory.AddPopup.registerEvent();
                    }
                });
            });
        },
        AddPopup: {
            registerEvent: function () {
                $("#save-news-category-btn").on("click", function () {
                    var form = $("#add-news-category-form");
                    if (form.isValid()) {
                        var formData = new FormData(form[0]);
                        TST.Helpers.closePopup();
                        TST.Helpers.ajax({
                            controller: "NewsCategory",
                            action: "Save",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                                TST.Admin.NewsCategory.ReloadTable();
                            }
                        });
                    }
                });
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            var deleteButton = $("#delete-news-category-btn");
            table.DataTable({
                dom: "lrtip",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("NewsCategory", "GetData")
                },
                columns: [
                    {
                        title: "Tiêu đề",
                        data: "Title"
                    },
                    {
                        title: "Hiển thị",
                        data: "IsShowed",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked} >`;
                            //return `<input type="checkbox" category-id=${full.Id} value=${data}>`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
                ],
                serverSide: true,
                "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "NewsCategory",
                                action: "Detail",
                                data: { categoryId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.NewsCategory.AddPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.NewsCategory.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa tin tức",
                            message: "Xác nhận xóa?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    url: TST.Helpers.resolveUrl("NewsCategory", "Delete"),
                                    data: { categoryId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.NewsCategory.ReloadTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    table.on("draw.dt", function () {
                        TST.Admin.NewsCategory.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa tin tức",
                                message: "Xác nhận xóa?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        url: TST.Helpers.resolveUrl("NewsCategory", "Delete"),
                                        data: { categoryId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.NewsCategory.ReloadTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox(TST.Admin.NewsCategory.TableId);

            $("input", this.TableId).on("ifChecked", function (event) {
                TST.Admin.NewsCategory.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input", this.TableId).on("ifUnchecked", function (event) {
                TST.Admin.NewsCategory.UpdateActiveStatus($(this).data("id"), false);
            });
        },

        UpdateActiveStatus: function (newsCategoryId, isActive) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("NewsCategory", "UpdateActiveStatus"),
                data: {
                    categoryId: newsCategoryId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});