﻿(function ($, TST) {
	TST.Admin = TST.Admin || {}

	TST.Admin.Tag = {
		TableId: "#tag-table",
		RegisterEvent: function () {
			$("#new-tag-btn").on("click", function () {
				TST.Helpers.ajaxOpenPopup({
					url: TST.Helpers.resolveUrl("Tag", "Detail"),
					width: 500,
					callback: function () {
						TST.Admin.Tag.AddPopup.registerEvent();
					}
				});
			});
		},
		AddPopup: {
			registerEvent: function () {
				$("#save-tag-btn").on("click", function () {
					var form = $("#add-tag-form");
					if (form.isValid()) {
						var formData = new FormData(form[0]);
						TST.Helpers.closePopup();
						TST.Helpers.ajax({
							controller: "Tag",
							action: "Save",
							processData: false,
							contentType: false,
							data: formData,
							method: "POST",
							callback: function (data) {
								TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
								TST.Admin.Tag.ReloadTable();
							}
						});
					}
				});
			}
		},
		InitTable: function () {
			var table = $(this.TableId);
			var deleteButton = $("#delete-tag-btn");
			table.DataTable({
			    order: [[1, "asc"]],
				ajax: {
					url: TST.Helpers.resolveUrl("Tag", "GetData")
				},
				columns: [
					{
						title: "Tiêu đề",
						data: "Title"
					},
                    {
                        title: "Tên",
                        data: "Name"
                    },
                    //{
                    //    title: "Tên ngắn",
                    //    data: "ShortName"
                    //},
                    {
                    	title: "Mô tả",
                    	data: "Description"
                    },
                    {
                    	title: "Hiển thị",
                    	data: "IsActive",
                    	render: function (data, type, full, meta) {
                    	    var checked = data ? "checked" : "";
                    	    return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked} data-type="isShow">`;
                    	}
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
				],
				//serverSide: true,
				//"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
				"initComplete": function () {
					table.DataTable()
                        .on("select", function () {
                        	deleteButton.enable();
                        })
                        .on("deselect", function () {
                        	deleteButton.disable();
                        });

					table.on("dblclick", "tr", function () {
						var selectedItem = table.DataTable().row(this).select().data();
						if (selectedItem) {
							TST.Helpers.ajaxOpenPopup({
								controller: "Tag",
								action: "Detail",
								data: { TagId: selectedItem.Id },
								width: 500,
								callback: function () {
									TST.Admin.Tag.AddPopup.registerEvent();
								}
							});
						}
					});

					//event for show/hide and isAbroad
					TST.Admin.Tag.RegisterCheckBoxActive();
					$(".delete-btn").on("click", function () {
					    var id = $(this).data("index");
					    TST.Helpers.confirmDialog({
					        title: "Xóa tag",
					        message: "Xác nhận xóa?",
					        callback: function () {
					            TST.Helpers.ajax({
					                url: TST.Helpers.resolveUrl("Tag", "Delete"),
					                data: { tagId: id },
					                method: "POST",
					                callback: function () {
					                    TST.Admin.Tag.ReloadTable();
					                    TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                    TST.Helpers.closePopup();
					                }
					            });
					        }
					    });
					});
					TST.Admin.Tag.RegisterFilterDatatable(table);
					table.on("draw.dt", function () {
					    TST.Admin.Tag.RegisterCheckBoxActive();
					    $(".delete-btn").on("click", function () {
					        var id = $(this).data("index");
					        TST.Helpers.confirmDialog({
					            title: "Xóa tag",
					            message: "Xác nhận xóa?",
					            callback: function () {
					                TST.Helpers.ajax({
					                    url: TST.Helpers.resolveUrl("Tag", "Delete"),
					                    data: { tagId: id },
					                    method: "POST",
					                    callback: function () {
					                        TST.Admin.Tag.ReloadTable();
					                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                        TST.Helpers.closePopup();
					                    }
					                });
					            }
					        });
					    });
					});
				}
			});
		},

		RegisterCheckBoxActive: function () {
		    TST.Helpers.initCheckbox(this.TableId);

			$("input[data-type='isShow']", this.TableId).on("ifChecked", function (event) {
			    TST.Admin.Tag.UpdateActiveStatus($(this).data("id"), true);
			});

			$("input[data-type='isShow']", this.TableId).on("ifUnchecked", function (event) {
				TST.Admin.Tag.UpdateActiveStatus($(this).data("id"), false);
			});
		},
		RegisterFilterDatatable: function (table) {
		    $('#tag-by-Title').on('keyup', function () {
		        table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
		    });
		    $('#tag-by-Name').on('keyup', function () {
		        table.DataTable().columns(1)
                    .search(this.value)
                    .draw();
		    });
		    //$('#tag-by-ShortName').on('keyup', function () {
		    //    table.DataTable().columns(2)
            //        .search(this.value)
            //        .draw();
		    //});
		},
		UpdateActiveStatus: function (TagId, isActive) {
			TST.Helpers.ajax({
				url: TST.Helpers.resolveUrl("Tag", "UpdateActiveStatus"),
				data: {
					TagId: TagId,
					isActive: isActive
				},
				method: "POST",
				callback: function (data) {
					TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
				}
			});
		},

		ReloadTable: function () {
		    $(this.TableId).DataTable().ajax.reload();
		    $("#delete-tag-btn").disable();
		}
	}

})(jQuery, window.TST = window.TST || {});