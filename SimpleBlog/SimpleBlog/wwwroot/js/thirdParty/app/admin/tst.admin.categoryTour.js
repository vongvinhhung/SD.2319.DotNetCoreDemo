﻿(function ($, TST) {
	TST.Admin = TST.Admin || {}

	TST.Admin.CategoryTour = {
		TableId: "#categorytour-table",
		RegisterEvent: function () {
			$("#new-categorytour-btn").on("click", function () {
				TST.Helpers.ajaxOpenPopup({
					url: TST.Helpers.resolveUrl("CategoryTour", "Detail"),
					width: 500,
					callback: function () {
						TST.Admin.CategoryTour.AddPopup.registerEvent();
					}

				});
			});
		},
		AddPopup: {
		    registerEvent: function () {
		        $('.datepicker').datetimepicker({
		            format: 'd/m/Y H:i'
		        });
				$("#save-categorytour-btn").on("click", function () {
					var form = $("#add-categorytour-form");
					if (form.isValid()) {
						var formData = new FormData(form[0]);
						TST.Helpers.closePopup();
						TST.Helpers.ajax({
							controller: "CategoryTour",
							action: "Save",
							processData: false,
							contentType: false,
							data: formData,
							method: "POST",
							callback: function (data) {
								TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
								TST.Admin.CategoryTour.ReloadTable();
							}
						});
					}
				});
			}
		},
		InitTable: function () {
			var table = $(this.TableId);
			var deleteButton = $("#delete-categorytour-btn");
			table.DataTable({
				ajax: {
					url: TST.Helpers.resolveUrl("CategoryTour", "GetData")
				},
                columns: [
                    {
                        title: "Mã loại hình tour",
                        data: "SignChar"
                    },
					{
						title: "Mô tả",
						data: "Description"
					},
                    {
                    	title: "Hiển thị",
                    	data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" categorytour-id=${full.Id} value=${data} ${checked} data-type="isActive">`;
                    	}
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return TST.Templates.DeleteButton(data);
                        }
                    }
				],
				serverSide: true,
				"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
				"initComplete": function () {
					table.DataTable()
                        .on("select", function () {
                        	deleteButton.enable();
                        })
                        .on("deselect", function () {
                        	deleteButton.disable();
                        });

					table.on("dblclick", "tr", function () {
						var selectedItem = table.DataTable().row(this).select().data();
						if (selectedItem) {
							TST.Helpers.ajaxOpenPopup({
								controller: "CategoryTour",
								action: "Detail",
								data: { categoryTourId: selectedItem.Id },
								width: 500,
								callback: function () {
									TST.Admin.CategoryTour.AddPopup.registerEvent();
								}
							});
						}
					});

					//event for show/hide and isAbroad
					TST.Admin.CategoryTour.RegisterCheckBoxActive();
					$(".delete-btn").on("click", function () {
					    var id = $(this).data("index");
					    TST.Helpers.confirmDialog({
					        title: "Xóa chuyến bay",
					        message: "Xác nhận xóa?",
					        callback: function () {
					            TST.Helpers.ajax({
					                url: TST.Helpers.resolveUrl("CategoryTour", "Delete"),
					                data: { categoryTourId: id },
					                method: "POST",
					                callback: function () {
					                    TST.Admin.CategoryTour.ReloadTable();
					                    TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                    TST.Helpers.closePopup();
					                }
					            });
					        }
					    });
					});
					TST.Admin.CategoryTour.RegisterFilterDatatable(table);
					table.on("draw.dt", function () {
					    TST.Admin.CategoryTour.RegisterCheckBoxActive();
					    $(".delete-btn").on("click", function () {
					        var id = $(this).data("index");
					        TST.Helpers.confirmDialog({
					            title: "Xóa loại hình tour",
					            message: "Xác nhận xóa?",
					            callback: function () {
					                TST.Helpers.ajax({
					                    url: TST.Helpers.resolveUrl("CategoryTour", "Delete"),
					                    data: { categoryTourId: id },
					                    method: "POST",
					                    callback: function () {
					                        TST.Admin.CategoryTour.ReloadTable();
					                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                        TST.Helpers.closePopup();
					                    }
					                });
					            }
					        });
					    });
					});
				}
			});
		},
		RegisterCheckBoxActive: function () {
		    TST.Helpers.initCheckbox(this.TableId);

			$("input[data-type='isActive']", this.TableId).on("ifChecked", function (event) {
				TST.Admin.CategoryTour.UpdateActiveStatus(event.currentTarget.getAttribute("categorytour-id"), true);
			});

			$("input[data-type='isActive']", this.TableId).on("ifUnchecked", function (event) {
				TST.Admin.CategoryTour.UpdateActiveStatus(event.currentTarget.getAttribute("categorytour-id"), false);
			});
		},
		RegisterFilterDatatable: function (table) {
		    $('#categorytour-by-Description').on('keyup', function () {
		        table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
		    });
		},
		UpdateActiveStatus: function (categoryTourId, isActive) {
			TST.Helpers.ajax({
				url: TST.Helpers.resolveUrl("CategoryTour", "UpdateActiveStatus"),
				data: {
					categoryTourId: categoryTourId,
					isActive: isActive
				},
				method: "POST",
				callback: function (data) {
					TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
				}
			});
		},

		ReloadTable: function () {
		    $(this.TableId).DataTable().ajax.reload();
		}
	}

})(jQuery, window.TST = window.TST || {});