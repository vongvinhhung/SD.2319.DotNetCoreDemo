﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.Slider = {
        RegisterEvent: function () {

            $("#new-slider-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "Slider",
                    action: "AddSliderPopup",
                    width: 500,
                    callback: function () {
                        TST.Admin.Slider.AddSliderPopup.registerEvent();
                    }
                });
            });
        },

        AddSliderPopup: {
            registerEvent: function () {
                $("#add-slider").on("change", function () {
                    var previewBox = $("#add-slider-preview .box.box-primary").empty();
                    var preview = $("#add-slider-preview");

                    preview.show();

                    if (this.files && this.files.length > 0) {
                        $.each(this.files, readAndPreview);
                    } else {
                        preview.hide();
                    }

                    function readAndPreview(i, file) {
                        if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                            preview.hide();
                        }
                        var reader = new FileReader();
                        $(reader).on("load", function () {
                            previewBox.append($("<img/>", { src: this.result, height: 100 }));
                        });
                        reader.readAsDataURL(file);
                    }
                });

                var saveBtn = $("#save-slider-btn");
                if (saveBtn) {
                    saveBtn.on("click", function () {
                        var form = $("#add-slider-form");
                        if (form.isValid()) {
                            var formData = new FormData(form[0]);

                            TST.Helpers.ajax({
                                controller: "Slider",
                                action: "AddSlider",
                                processData: false,
                                contentType: false,
                                data: formData,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.Slider.ReloadSliderTable();
                                    TST.Helpers.showAlert("Thêm thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

                var editBtn = $("#edit-slider-btn");
                if (editBtn) {
                    editBtn.on("click", function () {
                        var form = $("#edit-slider-form");
                        if (form.isValid()) {
                            var formData = new FormData(form[0]);

                            TST.Helpers.ajax({
                                controller: "Slider",
                                action: "EditSlider",
                                processData: false,
                                contentType: false,
                                data: formData,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.Slider.ReloadSliderTable();
                                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }
            }
        },

        InitSliderTable: function () {
            var sliderTable = $("#slider-table");
            var deleteButton = $("#delete-slider-btn");
            var fileUrl = $("#file-url").val();

            sliderTable.DataTable({
                ajax: {
                    url: TST.Helpers.resolveUrl("Slider", "GetSlidersData")
                },
                columns: [
                    {
                        title: "Hình ảnh",
                        data: "PathName",
                        render: function (data, type, full, meta) {
                            return `<img src="${fileUrl}/?image=${data}" height="75" style="display: block; margin: 0 auto;">`;
                        }
                    }, 
                    {
                        title: "Tựa đề",
                        data: "Title"
                    },
                    {
                        title: "Nội dung",
                        data: "Description"
                    },
                    {
                        title: "Link hình",
                        data: "Link"
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" slider-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return TST.Templates.DeleteButton(data);
                        }
                    }
                ],
                
                "initComplete": function () {

                    //select event
                    sliderTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });


                    sliderTable.on("dblclick", "tr", function () {
                        var selectedItem = sliderTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "Slider",
                                action: "EditSliderPopup",
                                data: { sliderId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.Slider.AddSliderPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //filter event
                    TST.Admin.Slider.RegisterFilterEvent();

                    //event for show/hide
                    TST.Admin.Slider.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa hình",
                            message: "Bạn có muốn xóa hình này?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "Slider",
                                    action: "DeleteSlider",
                                    data: { sliderId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.Slider.ReloadSliderTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    sliderTable.on("draw.dt", function () {
                        TST.Admin.Slider.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa hình",
                                message: "Bạn có muốn xóa hình này?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        controller: "Slider",
                                        action: "DeleteSlider",
                                        data: { sliderId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.Slider.ReloadSliderTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox("#slider-table");

            $("input", "#slider-table").on("ifChecked", function (event) {
                TST.Admin.Slider.UpdateActiveStatus(event.currentTarget.getAttribute("slider-id"), true);
            });

            $("input", "#slider-table").on("ifUnchecked", function (event) {
                TST.Admin.Slider.UpdateActiveStatus(event.currentTarget.getAttribute("slider-id"), false);
            });
        },

        RegisterFilterEvent: function () {
            var sliderTable = $("#slider-table");
            var timer; // for delay keyup
            function delayKeyUp(callBack) {
                clearTimeout(timer);
                timer = setTimeout(function () { callBack() }, 500);
            }

            $("#filter-by-description", "#slider-filter-group").on("keyup", function () {
                delayKeyUp(function() {
                    var descriptionColumn = sliderTable.DataTable().columns(2);
                    var searchValue = $("#filter-by-description", "#slider-filter-group").val();
                    if (descriptionColumn.search() !== searchValue) {
                        descriptionColumn.search(searchValue).draw();
                    }
                });
            });

            $("#filter-by-link", "#slider-filter-group").on("keyup", function () {
                delayKeyUp(function() {
                    var linkColumn = sliderTable.DataTable().columns(3);
                    var searchValue = $("#filter-by-link", "#slider-filter-group").val();
                    if (linkColumn.search() !== searchValue) {
                        linkColumn.search(searchValue).draw();
                    }
                });
            });
        },

        UpdateActiveStatus: function (sliderId, isActive) {
            TST.Helpers.ajax({
                controller: "Slider",
                action: "UpdateSliderActiveStatus",
                data: {
                    sliderId: sliderId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadSliderTable: function () {
            $("#slider-table").DataTable().ajax.reload();
            $("#delete-slider-btn").addClass("disabled");
        }
    }

})(jQuery, window.TST = window.TST || {});