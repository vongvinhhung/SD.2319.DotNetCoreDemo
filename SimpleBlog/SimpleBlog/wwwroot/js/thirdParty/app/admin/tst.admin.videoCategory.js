﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.VideoCategory = {
        RegisterEvent: function () {

            $("#new-video-category-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "VideoCategory",
                    action: "AddVideoCategoryPopup",
                    width: 500,
                    callback: function () {
                        TST.Admin.VideoCategory.AddVideoCategoryPopup.registerEvent();
                    }
                });
            });
        },

        AddVideoCategoryPopup: {
            registerEvent: function () {
                var saveBtn = $("#save-video-category-btn");
                if (saveBtn) {
                    saveBtn.on("click", function () {
                        var form = $("#add-video-category-form");
                        if (form.isValid()) {
                            var formData = TST.Helpers.formToObject(form);

                            TST.Helpers.ajax({
                                controller: "VideoCategory",
                                action: "AddVideoCategory",
                                data: formData,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.VideoCategory.ReloadVideoCategoryTable();
                                    TST.Helpers.showAlert("Thêm thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

                var editBtn = $("#edit-video-category-btn");
                if (editBtn) {
                    editBtn.on("click", function () {
                        var form = $("#edit-video-category-form");
                        if (form.isValid()) {
                            var formData = TST.Helpers.formToObject(form);

                            TST.Helpers.ajax({
                                controller: "VideoCategory",
                                action: "EditVideoCategory",
                                data: formData,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.VideoCategory.ReloadVideoCategoryTable();
                                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }
            }
        },


        InitVideoCategoryTable: function () {
            var videoCategoryTable = $("#video-category-table");
            var deleteButton = $("#delete-video-category-btn");

            videoCategoryTable.DataTable({
                //dom: "lrtip",
                //select: {
                //    style: "single",
                //    info: false
                //},
                ajax: {
                    url: TST.Helpers.resolveUrl("VideoCategory", "GetVideoCategorysData")
                },
                columns: [
                    {
                        title: "Mô tả",
                        data: "Description"

                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
                ],
                //serverSide: true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    videoCategoryTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    videoCategoryTable.on("dblclick", "tr", function () {
                        var selectedItem = videoCategoryTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "VideoCategory",
                                action: "EditVideoCategoryPopup",
                                data: { lookupId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.VideoCategory.AddVideoCategoryPopup.registerEvent();
                                }
                            });
                        }
                    });

                    TST.Admin.VideoCategory.RegisterFilterEvent();

                    //event for show/hide
                    TST.Admin.VideoCategory.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa",
                            message: "Bạn có muốn xóa danh mục này?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "VideoCategory",
                                    action: "DeleteVideoCategory",
                                    data: { lookupId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.VideoCategory.ReloadVideoCategoryTable();
                                        TST.Helpers.showAlert("Xóa thành công!", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    videoCategoryTable.on("draw.dt", function () {
                        TST.Admin.VideoCategory.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa",
                                message: "Bạn có muốn xóa danh mục này?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        controller: "VideoCategory",
                                        action: "DeleteVideoCategory",
                                        data: { lookupId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.VideoCategory.ReloadVideoCategoryTable();
                                            TST.Helpers.showAlert("Xóa thành công!", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                },

                "drawCallback": function (settings) {
                    
                }
            });
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox("#video-category-table");

            $("input", "#video-category-table").on("ifChecked", function (event) {
                TST.Admin.VideoCategory.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input", "#video-category-table").on("ifUnchecked", function (event) {
                TST.Admin.VideoCategory.UpdateActiveStatus($(this).data("id"), false);
            });
        },

        RegisterFilterEvent: function () {
            var videoCategoryTable = $("#video-category-table");
            var timer; // for delay keyup
            function delayKeyUp(callBack) {
                clearTimeout(timer);
                timer = setTimeout(function () { callBack() }, 500);
            }

            $("#filter-by-description", "#video-category-filter-group").on("keyup", function () {
                delayKeyUp(function () {
                    var descriptionColumn = videoCategoryTable.DataTable().columns(0);
                    var searchValue = $("#filter-by-description", "#video-category-filter-group").val();
                    if (descriptionColumn.search() !== searchValue) {
                        descriptionColumn.search(searchValue).draw();
                    }
                });
            });
        },

        UpdateActiveStatus: function (lookupId, isActive) {
            TST.Helpers.ajax({
                controller: "VideoCategory",
                action: "UpdateVideoCategoryActiveStatus",
                data: {
                    lookupId: lookupId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadVideoCategoryTable: function () {
            $("#video-category-table").DataTable().ajax.reload();
            $("#delete-video-category-btn").addClass("disabled");
        }
    }

})(jQuery, window.TST = window.TST || {});