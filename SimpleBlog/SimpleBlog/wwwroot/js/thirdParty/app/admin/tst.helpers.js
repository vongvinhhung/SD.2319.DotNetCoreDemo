﻿(function ($, TST, window) {
    //handle event when history state change
    window.onpopstate = function (event) {
        var href = document.location.href.replace("/#", "");
        if (document.location.origin + document.location.pathname === document.location.href) {
            href = document.location.origin + TST.Helpers.resolveUrl("Tour", "Index");
        }

        TST.Helpers.ajaxLoadNewContent({
            url: href,
            data: event.state,
            method: "GET",
            callback: function () {
                
            },
            error: function () {

            },
            addToHistoryState: false
        });
    };

    //setup default for jquery unobtrusive validate
    var defaultOptions = {
        highlight: function (element, errorClass, validClass) {
            $(element).closest(".form-group").addClass(errorClass);
            $(element).closest(".form-group")
                      .find("span.field-validation-valid, span.field-validation-error").addClass("help-block");

            if ($(element).closest(".form-group")[0] == undefined) {
                $(element).addClass(errorClass);
            }
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest(".form-group").removeClass(errorClass);

            if ($(element).closest(".form-group")[0] == undefined) {
                $(element).removeClass(errorClass);
            }

            //for booking
            $("#li1").css("border-top-color", "");
            $("#li2").css("border-top-color", "");
        },
        focusInvalid: false,
        ignore: []
    };

    $.validator.setDefaults(defaultOptions);

    $.validator.unobtrusive.options = {
        errorClass: "has-error",
        invalidHandler: function (e, validator) {

        }
    }

    //use for parse add row booking
    $.validator.unobtrusive.parseDynamicContent = function (selector) {
        //use the normal unobstrusive.parse method
        $.validator.unobtrusive.parse(selector);

        //get the relevant form
        var form = $(selector).first().closest('form');

        //get the collections of unobstrusive validators, and jquery validators
        //and compare the two
        var unobtrusiveValidation = form.data('unobtrusiveValidation');
        var validator = form.validate();

        $.each(unobtrusiveValidation.options.rules, function (elname, elrules) {
            if (validator.settings.rules[elname] == undefined) {
                var args = {};
                $.extend(args, elrules);
                args.messages = unobtrusiveValidation.options.messages[elname];
                //edit:use quoted strings for the name selector
                $("[name='" + elname + "']").rules("add", args);
            } else {
                $.each(elrules, function (rulename, data) {
                    if (validator.settings.rules[elname][rulename] == undefined) {
                        var args = {};
                        args[rulename] = data;
                        args.messages = unobtrusiveValidation.options.messages[elname][rulename];
                        //edit:use quoted strings for the name selector
                        $("[name='" + elname + "']").rules("add", args);
                    }
                });
            }
        });
    }

    $.fn.extend({
        isValid: function () {
            $.validator.unobtrusive.parse($(this));
            return $(this).valid();
        },

        disable: function () {
            $(this).attr("disabled", "disabled");
            $(this).addClass("disabled");
        },

        enable: function () {
            $(this).removeAttr("disabled");
            $(this).removeClass("disabled");
        }
    });

    TST.Helpers = {
        initCheckbox: function (container) {
            if (!container) {
                container = "body";
            }
            var checkBoxs = $("input[type='checkbox']", container);
            for (let i = 0; i < checkBoxs.length; i++) {
                var cb = $(checkBoxs[i]);
                if (cb) {
                    cb.iCheck({
                        checkboxClass: "icheckbox_square-blue",
                        increaseArea: "20%"
                    });
                    //if (cb.is(":checked") || cb.val() === 'true') cb.iCheck("check");
                }
            }
        },
        showAlert: function (message, type, icon, closeDelay) {

            if ($("#alerts-container").length === 0) {
                $("body").append($('<div id="alerts-container" style="position: fixed; width: 25%; left: 73%; top: 8%;">'));
            }

            type = type || "info";
            icon = icon || "info";

            var alert =
                '<div id="tst-alert" class="alert alert-' + type + ' alert-dismissible fade in">\n' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\n' +
                    '<i class="icon fa fa-' + icon + '"></i>\n' + message +
                "</div>";

            $("#alerts-container").empty();
            $("#alerts-container").append(alert);

            // if closeDelay was passed - set a timeout to close the alert
            window.setTimeout(function () { $("#tst-alert").alert("close") }, closeDelay || 3000);
        },

        addOverlay: function () {
            $.LoadingOverlay("show",
            {
                size: false,
                fade: [200, 100],
                color: "rgba(0, 0, 0, 0.4)",
                image: "data:image/false;base64,R0lGODlhNgA3APMAAP///5L/c8X+tJ3+gpf+eu3+6bX+oO/+6+X+3rD+msz+vgAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAANgA3AAAEzBDISau9OOvNu/9gKI5kaZ4lkhBEgqCnws6EApMITb93uOqsRC8EpA1Bxdnx8wMKl51ckXcsGFiGAkamsy0LA9pAe1EFqRbBYCAYXXUGk4DWJhZN4dlAlMSLRW80cSVzM3UgB3ksAwcnamwkB28GjVCWl5iZmpucnZ4cj4eWoRqFLKJHpgSoFIoEe5ausBeyl7UYqqw9uaVrukOkn8LDxMXGx8ibwY6+JLxydCO3JdMg1dJ/Is+E0SPLcs3Jnt/F28XXw+jC5uXh4u89EQAh+QQJCgAAACwAAAAANgA3AAAEzhDISau9OOvNu/9gKI5kaZ5oqhYGQRiFWhaD6w6xLLa2a+iiXg8YEtqIIF7vh/QcarbB4YJIuBKIpuTAM0wtCqNiJBgMBCaE0ZUFCXpoknWdCEFvpfURdCcM8noEIW82cSNzRnWDZoYjamttWhphQmOSHFVXkZecnZ6foKFujJdlZxqELo1AqQSrFH1/TbEZtLM9shetrzK7qKSSpryixMXGx8jJyifCKc1kcMzRIrYl1Xy4J9cfvibdIs/MwMue4cffxtvE6qLoxubk8ScRACH5BAkKAAAALAAAAAA2ADcAAATOEMhJq7046827/2AojmRpnmiqrqwwDAJbCkRNxLI42MSQ6zzfD0Sz4YYfFwyZKxhqhgJJeSQVdraBNFSsVUVPHsEAzJrEtnJNSELXRN2bKcwjw19f0QG7PjA7B2EGfn+FhoeIiYoSCAk1CQiLFQpoChlUQwhuBJEWcXkpjm4JF3w9P5tvFqZsLKkEF58/omiksXiZm52SlGKWkhONj7vAxcbHyMkTmCjMcDygRNAjrCfVaqcm11zTJrIjzt64yojhxd/G28XqwOjG5uTxJhEAIfkECQoAAAAsAAAAADYANwAABM0QyEmrvTjrzbv/YCiOZGmeaKqurDAMAlsKRE3EsjjYxJDrPN8PRLPhhh8XDMk0KY/OF5TIm4qKNWtnZxOWuDUvCNw7kcXJ6gl7Iz1T76Z8Tq/b7/i8qmCoGQoacT8FZ4AXbFopfTwEBhhnQ4w2j0GRkgQYiEOLPI6ZUkgHZwd6EweLBqSlq6ytricICTUJCKwKkgojgiMIlwS1VEYlspcJIZAkvjXHlcnKIZokxJLG0KAlvZfAebeMuUi7FbGz2z/Rq8jozavn7Nev8CsRACH5BAkKAAAALAAAAAA2ADcAAATLEMhJq7046827/2AojmRpnmiqrqwwDAJbCkRNxLI42MSQ6zzfD0Sz4YYfFwzJNCmPzheUyJuKijVrZ2cTlrg1LwjcO5HFyeoJeyM9U++mfE6v2+/4PD6O5F/YWiqAGWdIhRiHP4kWg0ONGH4/kXqUlZaXmJlMBQY1BgVuUicFZ6AhjyOdPAQGQF0mqzauYbCxBFdqJao8rVeiGQgJNQkIFwdnB0MKsQrGqgbJPwi2BMV5wrYJetQ129x62LHaedO21nnLq82VwcPnIhEAIfkECQoAAAAsAAAAADYANwAABMwQyEmrvTjrzbv/YCiOZGmeaKqurDAMAlsKRE3EsjjYxJDrPN8PRLPhhh8XDMk0KY/OF5TIm4qKNWtnZxOWuDUvCNw7kcXJ6gl7Iz1T76Z8Tq/b7/g8Po7kX9haKoAZZ0iFGIc/iRaDQ40Yfj+RepSVlpeYAAgJNQkIlgo8NQqUCKI2nzNSIpynBAkzaiCuNl9BIbQ1tl0hraewbrIfpq6pbqsioaKkFwUGNQYFSJudxhUFZ9KUz6IGlbTfrpXcPN6UB2cHlgfcBuqZKBEAIfkECQoAAAAsAAAAADYANwAABMwQyEmrvTjrzbv/YCiOZGmeaKqurDAMAlsKRE3EsjjYxJDrPN8PRLPhhh8XDMk0KY/OF5TIm4qKNWtnZxOWuDUvCNw7kcXJ6gl7Iz1T76Z8Tq/b7yJEopZA4CsKPDUKfxIIgjZ+P3EWe4gECYtqFo82P2cXlTWXQReOiJE5bFqHj4qiUhmBgoSFho59rrKztLVMBQY1BgWzBWe8UUsiuYIGTpMglSaYIcpfnSHEPMYzyB8HZwdrqSMHxAbath2MsqO0zLLorua05OLvJxEAIfkECQoAAAAsAAAAADYANwAABMwQyEmrvTjrzbv/YCiOZGmeaKqurDAMAlsKRE3EsjjYxJDrPN8PRLPhfohELYHQuGBDgIJXU0Q5CKqtOXsdP0otITHjfTtiW2lnE37StXUwFNaSScXaGZvm4r0jU1RWV1hhTIWJiouMjVcFBjUGBY4WBWw1A5RDT3sTkVQGnGYYaUOYPaVip3MXoDyiP3k3GAeoAwdRnRoHoAa5lcHCw8TFxscduyjKIrOeRKRAbSe3I9Um1yHOJ9sjzCbfyInhwt3E2cPo5dHF5OLvJREAOwAAAAAAAAAAADxiciAvPgo8Yj5XYXJuaW5nPC9iPjogIG15c3FsX3F1ZXJ5KCkgWzxhIGhyZWY9J2Z1bmN0aW9uLm15c3FsLXF1ZXJ5Jz5mdW5jdGlvbi5teXNxbC1xdWVyeTwvYT5dOiBDYW4ndCBjb25uZWN0IHRvIGxvY2FsIE15U1FMIHNlcnZlciB0aHJvdWdoIHNvY2tldCAnL3Zhci9ydW4vbXlzcWxkL215c3FsZC5zb2NrJyAoMikgaW4gPGI+L2hvbWUvYWpheGxvYWQvd3d3L2xpYnJhaXJpZXMvY2xhc3MubXlzcWwucGhwPC9iPiBvbiBsaW5lIDxiPjY4PC9iPjxiciAvPgo8YnIgLz4KPGI+V2FybmluZzwvYj46ICBteXNxbF9xdWVyeSgpIFs8YSBocmVmPSdmdW5jdGlvbi5teXNxbC1xdWVyeSc+ZnVuY3Rpb24ubXlzcWwtcXVlcnk8L2E+XTogQSBsaW5rIHRvIHRoZSBzZXJ2ZXIgY291bGQgbm90IGJlIGVzdGFibGlzaGVkIGluIDxiPi9ob21lL2FqYXhsb2FkL3d3dy9saWJyYWlyaWVzL2NsYXNzLm15c3FsLnBocDwvYj4gb24gbGluZSA8Yj42ODwvYj48YnIgLz4KPGJyIC8+CjxiPldhcm5pbmc8L2I+OiAgbXlzcWxfcXVlcnkoKSBbPGEgaHJlZj0nZnVuY3Rpb24ubXlzcWwtcXVlcnknPmZ1bmN0aW9uLm15c3FsLXF1ZXJ5PC9hPl06IENhbid0IGNvbm5lY3QgdG8gbG9jYWwgTXlTUUwgc2VydmVyIHRocm91Z2ggc29ja2V0ICcvdmFyL3J1bi9teXNxbGQvbXlzcWxkLnNvY2snICgyKSBpbiA8Yj4vaG9tZS9hamF4bG9hZC93d3cvbGlicmFpcmllcy9jbGFzcy5teXNxbC5waHA8L2I+IG9uIGxpbmUgPGI+Njg8L2I+PGJyIC8+CjxiciAvPgo8Yj5XYXJuaW5nPC9iPjogIG15c3FsX3F1ZXJ5KCkgWzxhIGhyZWY9J2Z1bmN0aW9uLm15c3FsLXF1ZXJ5Jz5mdW5jdGlvbi5teXNxbC1xdWVyeTwvYT5dOiBBIGxpbmsgdG8gdGhlIHNlcnZlciBjb3VsZCBub3QgYmUgZXN0YWJsaXNoZWQgaW4gPGI+L2hvbWUvYWpheGxvYWQvd3d3L2xpYnJhaXJpZXMvY2xhc3MubXlzcWwucGhwPC9iPiBvbiBsaW5lIDxiPjY4PC9iPjxiciAvPgo8YnIgLz4KPGI+V2FybmluZzwvYj46ICBteXNxbF9xdWVyeSgpIFs8YSBocmVmPSdmdW5jdGlvbi5teXNxbC1xdWVyeSc+ZnVuY3Rpb24ubXlzcWwtcXVlcnk8L2E+XTogQ2FuJ3QgY29ubmVjdCB0byBsb2NhbCBNeVNRTCBzZXJ2ZXIgdGhyb3VnaCBzb2NrZXQgJy92YXIvcnVuL215c3FsZC9teXNxbGQuc29jaycgKDIpIGluIDxiPi9ob21lL2FqYXhsb2FkL3d3dy9saWJyYWlyaWVzL2NsYXNzLm15c3FsLnBocDwvYj4gb24gbGluZSA8Yj42ODwvYj48YnIgLz4KPGJyIC8+CjxiPldhcm5pbmc8L2I+OiAgbXlzcWxfcXVlcnkoKSBbPGEgaHJlZj0nZnVuY3Rpb24ubXlzcWwtcXVlcnknPmZ1bmN0aW9uLm15c3FsLXF1ZXJ5PC9hPl06IEEgbGluayB0byB0aGUgc2VydmVyIGNvdWxkIG5vdCBiZSBlc3RhYmxpc2hlZCBpbiA8Yj4vaG9tZS9hamF4bG9hZC93d3cvbGlicmFpcmllcy9jbGFzcy5teXNxbC5waHA8L2I+IG9uIGxpbmUgPGI+Njg8L2I+PGJyIC8+Cg=="
            });
        },

        removeOverlay: function () {
            $.LoadingOverlay("hide");
        },

        ajax: function (option, callback) {
            var defaultOpt = {
                url: "",
                controller: "",
                action: "",
                data: {},
                method: "GET",
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                processData: true,
                beforeSend: function () { },
                callback: function () {
                    if (callback) {
                        callback();
                    }
                },
                error: function () { },
                handleErrorByDefault: true,
                addToHistoryState: true
            }

            var opt = $.extend({}, defaultOpt, option);

            var url = opt.url;
            var baseUrl = $("#base-url").val();
            if (opt.controller !== "" && opt.action !== "") url = `${baseUrl}/${opt.controller}/${opt.action}`;
            var history = `${opt.controller}/${opt.action}`;
            $.ajax({
                url: url,
                method: opt.method,
                data: opt.data,
                contentType: opt.contentType,
                processData: opt.processData,
                beforeSend: function (e) {
                    //add overlay to prevent multiple click
                    TST.Helpers.addOverlay();
                    opt.beforeSend(e);
                },
                success: function (e) {
                    setTimeout(function () {
                        if (opt.openPopup && typeof (opt.openPopup) == "function") {
                            opt.openPopup(e, opt);
                            TST.Helpers.initCheckbox(".modal-dialog");
                        } else if (opt.loadContent && typeof (opt.loadContent) == "function") {
                            opt.loadContent(e);

                            if (opt.addToHistoryState) {
                                //push current url to history 
                                window.history.pushState(opt.data, "", `#${url.replace(baseUrl,"")}`);
                            }
                        }

                        opt.callback(e);

                        if (e.result === "Redirect") {
                            window.location = e.url;
                        } else {
                            TST.Helpers.removeOverlay();
                        }
                    }, 200);
                },
                error: function (e) {
                    if (opt.handleErrorByDefault) {
                        if (e.responseJSON && e.responseJSON.CustomValidator && e.status === 500) {
                            var tmp = e.responseJSON.CustomValidator[0];
                            var validator = $("span[data-valmsg-for='" + tmp.ErrorProperty + "']").closest("form").validate();
                            var validateFields = {}
                            if (validator) {
                                for (var i = 0; i < e.responseJSON.CustomValidator.length; i++) {
                                    validateFields[e.responseJSON.CustomValidator[i].ErrorProperty] = e.responseJSON.CustomValidator[i].ErrorMessage;
                                }
                                validator.showErrors(validateFields);
                            }
                        } else {
                            TST.Helpers.confirmDialog({
                                title: "Error",
                                message: "Looks like something went wrong!",
                                dialogType: "danger",
                                buttonType: "ok"
                            });
                        }
                    }

                    opt.error(e);

                    TST.Helpers.removeOverlay();
                }
            });
        },

        ajaxLoadNewContent: function (option) {
            option.loadContent = TST.Helpers.loadContent;
            TST.Helpers.ajax(option);
        },

        ajaxOpenPopup: function (option, callback) {
            var defaultOpt = {
                width: 500 // default width for popup
            };
            var opt = $.extend({}, defaultOpt, option);

            opt.openPopup = TST.Helpers.openPopup;

            TST.Helpers.ajax(opt, callback);
        },
        ajaxOpenLargePopup: function (option, callback) {
            var defaultOpt = {

            };
            var opt = $.extend({}, defaultOpt, option);

            opt.openPopup = TST.Helpers.openLargePopup;

            TST.Helpers.ajax(opt, callback);
        },

        loadContent: function (content) {
            var mainContent = $("#tst-admin-main-content");
            mainContent.empty();
            mainContent.html(content);
        },

        openPopup: function (popupContent, opt) {
            var popup = $("#tst-default-popup");
            if (popup.length > 0 && $(".bs-example-modal-lg").length > 0) {
                popup.remove();
            }
            popup = $("#tst-default-popup");
            var popupBody = $("#tst-default-popup .modal-content");
            if (popup.length <= 0) {
                $("body").append(
                    '<div class="modal fade" id="tst-default-popup" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">\n' +
                    '<div class="modal-dialog" role="document">\n' +
                    '<div class="modal-content">\n' +
                    "</div>\n" +
                    "</div>\n" +
                    "</div>");

                popup = $("#tst-default-popup");
                popupBody = $("#tst-default-popup .modal-content");
            }

            popup.find(".modal-dialog").width(opt.width || 500);
            popupBody.empty();
            popupBody.append(popupContent);

            popup.modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        openLargePopup: function (popupContent, opt) {
            var popup = $("#tst-default-popup");
            if (popup.length > 0 && $(".bs-example-modal-lg").length <= 0) {
                popup.remove();
            }
            popup = $("#tst-default-popup");
            var popupBody = $("#tst-default-popup .modal-content");

            if (popup.length <= 0 || $(".bs-example-modal-lg").length <= 0) {
                $("body").append(
                '<div class="modal fade bs-example-modal-lg" id="tst-default-popup" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">\n' +
                    '<div class="modal-dialog modal-lg" role="document">\n' +
                        '<div class="modal-content">\n' +

                        "</div>\n" +
                    "</div>\n" +
                "</div>");

                popup = $("#tst-default-popup");
                popupBody = $("#tst-default-popup .modal-content");
            }

            //popup.find(".modal-dialog").width(opt.width || 500);
            popup.find(".modal-dialog").css("width", "");
            popupBody.empty();
            popupBody.append(popupContent);

            popup.modal({
                backdrop: 'static',
                keyboard: false
            });
        },

        closePopup: function () {
            $("#tst-default-popup").modal("hide");
        },

        confirmDialog: function (option) {
            var defaultOpt = {
                title: "",
                message: "",
                dialogType: "danger",
                callback: function () { },
                buttonType: "yes/no"
            };
            var opt = $.extend({}, defaultOpt, option);

            var popupContent =
                `<div class="box box-${opt.dialogType} box-solid">\n` +
                    '<div class="box-header with-border">\n' +
                        `<h3 class="box-title">${opt.title}</h3>\n` +
                        '<div class="box-tools pull-right">\n' +
                            '<button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>\n' +
                        "</div>\n" +
                    "</div>\n" +
                    '<div class="box-body">\n' +
                        `<div><h4>${opt.message}</h4></div> <br>\n` +
                        '<div class="pull-right">\n' +
                            '<button type="button" class="btn btn-default" data-dismiss="modal" id="tst-default-dialog-no-btn">Không</button>\n' +
                            '<button type="button" class="btn btn-danger" id="tst-default-dialog-yes-btn">Có</button>\n' +
                        "</div>" +
                    "</div>" +
                "</div>";

            TST.Helpers.openPopup(popupContent, {});

            if (opt.buttonType === "yes/no") {

            } else if (opt.buttonType === "ok") {
                $("#tst-default-dialog-no-btn").html("Ok");
                $("#tst-default-dialog-no-btn").removeClass("btn-default");
                $("#tst-default-dialog-no-btn").addClass("btn-primary");

                $("#tst-default-dialog-yes-btn").hide();
            }

            $("#tst-default-dialog-yes-btn").on("click", function () {
                opt.callback();
            });
        },

        loadDDL: function (controlId, action, data) {
            $(document).ready(function () {
                $.ajax({
                    type: "POST",
                    url: action,
                    data: data,
                    success: function (Result) {
                        //$(controlId).append($("<option></option>").val
                        //    (value.CountryId).html(value.CountryName));
                        $(controlId).html('');
                        $.each(Result, function (key, value) {
                            $(controlId).append($("<option></option>").val
                                (value.Value).html(value.Text));
                        });
                        // Another way of writing
                        //  for (var i = 0; i < Result.length; i++) {
                        // $("#ddlcountry").append("<option value=" + Result[i].ID + ">" + 
                        //     Result[i].Name + "</option>");
                        //  }

                        // One more way of writing
                        // for (var i in Result) {
                        //  $("#ddlcountry").append($("<option></option>").val(Result[i].ID).
                        //   text(Result[i].Name));
                        //  }

                    },
                    error: function (Result) {
                        alert("Error");
                    }
                });
            });
        },

        defaultOptionDirtyChecking: {
            denoteDirtyForm: true,
            denoteDirtyOptions: true,
            dirtyOptionClass: "dirtyChoice",
            trimText: true,

            preFieldChangeCallback: function (originalValue) {

            },

            fieldChangeCallback: function (originalValue, result) {

            },

            formChangeCallback: function (result, dirtyFieldsArray) {

            }
        },

        formToObject: function (form, modelName) {
            var prefix = "";
            if (modelName && typeof (modelName) == "string") {
                prefix = modelName + ".";
            }

            var returnArray = {};
            var formArray = form.serializeArray();

            for (let i = 0; i < formArray.length; i++) {
                returnArray[formArray[i]["name"].replace(prefix, "")] = formArray[i]["value"];
            }

            return returnArray;
        },

        getUrlParameter: function (parameterName) {
            var result = null,
                tmp = [];
            var items = location.search.substr(1).split("&");
            for (let index = 0; index < items.length; index++) {
                tmp = items[index].split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            }
            return result;
        },

        resolveUrl: function (controller, action) {
            var baseUrl = $("#base-url").val();
            return `${baseUrl}/${controller}/${action}`;
        },
        setTitleAdmin(title) {
            $("#adminTitle").text(title);
        }
    }
})(jQuery, window.TST = window.TST || {}, window);