﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.PriceType = {
        TableId: "#price-type-table",
        RegisterEvent: function () {
            $("#new-price-type-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    url: TST.Helpers.resolveUrl("PriceType", "Detail"),
                    width: 500,
                    callback: function () {
                        TST.Admin.PriceType.AddPopup.registerEvent();
                    }
                });
            });
        },
        AddPopup: {
            registerEvent: function () {
                $("#save-price-type-btn").on("click", function () {
                    var form = $("#add-price-type-form");
                    if (form.isValid()) {
                        var formData = new FormData(form[0]);
                        TST.Helpers.closePopup();
                        TST.Helpers.ajax({
                            controller: "PriceType",
                            action: "Save",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                                TST.Admin.PriceType.ReloadTable();
                            }
                        });
                    }
                });
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            var deleteButton = $("#delete-price-type-btn");
            table.DataTable({
                //dom: "lrtip",
                //select: {
                //    style: "single",
                //    info: false
                //},
                ajax: {
                    url: TST.Helpers.resolveUrl("PriceType", "GetData")
                },
                columns: [
                    {
                        title: "Mô tả",
                        data: "Description"
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
                ],
                //serverSide: true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "PriceType",
                                action: "Detail",
                                data: { priceTypeId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.PriceType.AddPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.PriceType.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa hình",
                            message: "Xác nhận xóa?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    url: TST.Helpers.resolveUrl("PriceType", "Delete"),
                                    data: { priceTypeId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.PriceType.ReloadTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    TST.Admin.PriceType.RegisterFilterDatatable(table);
                    table.on("draw.dt", function () {
                        TST.Admin.PriceType.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa hình",
                                message: "Xác nhận xóa?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        url: TST.Helpers.resolveUrl("PriceType", "Delete"),
                                        data: { priceTypeId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.PriceType.ReloadTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox(this.TableId);

            $("input", this.TableId).on("ifChecked", function (event) {
                TST.Admin.PriceType.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input", this.TableId).on("ifUnchecked", function (event) {
                TST.Admin.PriceType.UpdateActiveStatus($(this).data("id"), false);
            });
        },
        RegisterFilterDatatable: function (table) {
            $('#price-type-by-Description').on('keyup', function () {
                table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
            });
        },
        UpdateActiveStatus: function (priceTypeId, isActive) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("PriceType", "UpdateActiveStatus"),
                data: {
                    priceTypeId: priceTypeId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});