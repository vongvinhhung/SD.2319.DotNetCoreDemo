﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.Navigation = {

        registerEvent: function () {
            var mainNavigation = $("#tst-admin-main-navigation");
            var items = mainNavigation.find("a");

            items.each(function () {
                $(this).on("click", function () {
                    var li = $(this).closest("li");
                    var clickedItem = $(this);

                    if (clickedItem.attr("url")) {
                        TST.Helpers.ajaxLoadNewContent({
                            url: $(this).attr("url"),
                            method: "GET",
                            callback: function() {
                                mainNavigation.find("li").removeClass("active");

                                if (!clickedItem.closest("ul").hasClass("treeview-menu menu-open")) {
                                    mainNavigation.find(".treeview-menu.menu-open").prev().click();
                                } else {
                                    mainNavigation.find(".treeview-menu.menu-open").closest("li.treeview")
                                        .addClass("active");
                                }

                                li.addClass("active");

                            },
                            error: function() {

                            }
                        });
                    }
                });
            });
        }
    }
})(jQuery, window.TST = window.TST || {});