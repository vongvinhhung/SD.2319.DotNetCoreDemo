﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.PriceDefault = {
        TableId: "#price-default-table",
        RegisterEvent: function () {


            TST.Helpers.initCheckbox("#priceDefaultTable");

            $(".pricetype-textBox", "#priceDefaultTable").on("change textInput input", function() {
                $(this).data("change", "True");
            });
            $(".pricetype-textBox", "#priceDefaultTable").on("focusout", function () {
                if ($(this).data("change") == "True") {
                    $(this).data("change", "False");
                    TST.Helpers.ajax({
                        controller: "PriceDefault",
                        action: "UpdatePrice",
                        method: "POST",
                        data: {
                            categoryId: $(this).data("category"),
                            pricetypeId: $(this).data("pricetype"),
                            value: $(this).val()
                        },
                        callback: function () {
                            $(this).data("change", "False");
                            TST.Helpers.showAlert("Sửa giá thành công!", "success", "check");
                        }
                    });
                }
            });

            $("#new-price-default-btn").on("click", function () {
                var newTypeName = $("#new-price-type-name").val();
                if (newTypeName == undefined || newTypeName == "") {
                    TST.Helpers.showAlert("Làm ơn nhập tên loại giá !", "error", "check");
                }
                else {
                    TST.Helpers.ajax({
                        controller: "PriceDefault",
                        action: "AddPriceType",
                        data: { nameType: newTypeName},
                        method: "POST",
                        callback: function (data) {
                            TST.Helpers.ajaxLoadNewContent({
                                controller: "PriceDefault",
                                action: "index",
                                callback: function () {

                                }
                            });
                            //TST.Admin.Tour.ReloadTourDetailTable();
                            TST.Helpers.showAlert("Thêm loại giá thành công !", "success", "check");
                            //TST.Helpers.closePopup();
                            
                        }
                    });
                }
                
                //TST.Helpers.ajaxOpenPopup({
                //    url: TST.Helpers.resolveUrl("PriceDefault", "Detail"),
                //    width: 500,
                //    callback: function () {
                //        TST.Admin.PriceDefault.AddPopup.registerEvent();
                //    }
                //});
            });

            $(".delete-price-default-btn").on("click", function () {
                //var table = $(TST.Admin.PriceDefault.TableId);
                //var selectedItem = table.DataTable().rows({ selected: true }).data()[0];

                var priceTypeId = $(this).data("index");

                TST.Helpers.confirmDialog({
                    title: "Xóa loại giá",
                    message: "Xác nhận xóa?",
                    callback: function () {
                        TST.Helpers.ajax({
                            url: TST.Helpers.resolveUrl("PriceDefault", "DeletePriceType"),
                            data: { priceTypeId: priceTypeId },
                            method: "POST",
                            callback: function () {
                                //TST.Admin.PriceDefault.ReloadTable();
                                TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                TST.Helpers.closePopup();
                                TST.Helpers.ajaxLoadNewContent({
                                    controller: "PriceDefault",
                                    action: "index",
                                    callback: function () {

                                    }
                                });
                            }
                        });
                    }
                });
            });

            $("#priceDefaultTable .main-price-chk").on("click", function () {
                //var table = $(TST.Admin.PriceDefault.TableId);
                //var selectedItem = table.DataTable().rows({ selected: true }).data()[0];
                var checked = $(this).parent('[class*="icheckbox"]').hasClass("checked");
                debugger;
                TST.Admin.PriceDefault.UpdateMainStatus($(this).data("id"), !checked);
               
            });
            $("input", "#priceDefaultTable").on("ifChanged", function (event) {
                
                TST.Admin.PriceDefault.UpdateMainStatus($(this).data("id"), event.target.checked);
            });
            $("#save-price-default-btn").on("click", function () {
                var form = $("#add-price-default-form");
                if (form.isValid()) {
                    var formData = new FormData(form[0]);
                    TST.Helpers.ajax({
                        controller: "PriceDefault",
                        action: "SubmitPricePopup",
                        processData: false,
                        data: form.serialize(),
                        method: "POST",
                        callback: function (data) {
                            //TST.Admin.Tour.ReloadTourDetailTable();
                            TST.Helpers.showAlert("Sửa giá thành công !", "success", "check");
                            //TST.Helpers.closePopup();
                        }
                    });
                }
            });
        },
        /*
        AddPopup: {
            registerEvent: function () {
                $("#save-price-default-btn").on("click", function () {
                    var form = $("#add-price-default-form");
                    if (form.isValid()) {
                        var formData = new FormData(form[0]);
                        TST.Helpers.closePopup();
                        TST.Helpers.ajax({
                            controller: "PriceDefault",
                            action: "Save",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                                TST.Admin.PriceDefault.ReloadTable();
                            }
                        });
                    }
                });
            }
        },
        
        InitTable: function () {
            var table = $(this.TableId);
            var deleteButton = $("#delete-price-default-btn");
            table.DataTable({
                dom: "lrtip",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("PriceDefault", "GetData")
                },
                columns: [
                    {
                        title: "Tiêu đề",
                        data: "Title"
                    },
                    {
                        title: "Mô tả",
                        data: "Description"
                    },
                    {
                        title: "Nhóm giá",
                        data: "CategoryType"
                    },
                    {
                        title: "Loại",
                        data: "PriceType"
                    },
                    {
                        title: "Giá",
                        data: "Value",
                        render: function (data, type, full, meta) {
                            return data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + " VND";
                        }
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            return `<input type="checkbox" price-default-id=${full.Id} value=${data} data-type="isShow">`;
                        }
                    },
                    {
                        title: "Giá chính?",
                        data: "IsMainPrice",
                        render: function (data, type, full, meta) {
                            return `<input type="checkbox" price-default-id=${full.Id} value=${data} data-type="isMain">`;
                        }
                    }
                ],
                serverSide: true,
                "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "PriceDefault",
                                action: "Detail",
                                data: { priceDefaultId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.PriceDefault.AddPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.PriceDefault.RegisterCheckBoxActive();
                    TST.Admin.PriceDefault.RegisterFilterDatatable(table);
                    table.on("draw.dt", function () {
                        TST.Admin.PriceDefault.RegisterCheckBoxActive();
                    });
                }
            });
        },
        */
        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox(this.TableId);

            $("input[data-type='isShow']", this.TableId).on("ifChecked", function (event) {
                TST.Admin.PriceDefault.UpdateActiveStatus(event.currentTarget.getAttribute("price-default-id"), true);
            });

            $("input[data-type='isShow']", this.TableId).on("ifUnchecked", function (event) {
                TST.Admin.PriceDefault.UpdateActiveStatus(event.currentTarget.getAttribute("price-default-id"), false);
            });

            $("input[data-type='isMain']", this.TableId).on("ifChecked", function (event) {
                TST.Admin.PriceDefault.UpdateMainStatus(event.currentTarget.getAttribute("price-default-id"), true);
            });

            $("input[data-type='isMain']", this.TableId).on("ifUnchecked", function (event) {
                TST.Admin.PriceDefault.UpdateMainStatus(event.currentTarget.getAttribute("price-default-id"), false);
            });
        },
        RegisterFilterDatatable: function (table) {
            $('#price-default-by-Title').on('keyup', function () {
                table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
            });
            $('#price-default-by-Description').on('keyup', function () {
                table.DataTable().columns(1)
                    .search(this.value)
                    .draw();
            });
        },
        UpdateActiveStatus: function (priceDefaultId, isActive) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("PriceDefault", "UpdateActiveStatus"),
                data: {
                    priceDefaultId: priceDefaultId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },
        UpdateMainStatus: function (priceDefaultId, isMain) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("PriceDefault", "UpdateMainStatus"),
                data: {
                    priceDefaultId: priceDefaultId,
                    isMain: isMain
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});