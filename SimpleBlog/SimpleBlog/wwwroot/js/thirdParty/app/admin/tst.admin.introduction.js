﻿(function ($, TST) {
	TST.Admin = TST.Admin || {}

	TST.Admin.Introduction = {
		TableId: "#introduction-table",
		RegisterEvent: function () {
			$("#new-introduction-btn").on("click", function () {
				TST.Helpers.ajaxOpenLargePopup({
					url: TST.Helpers.resolveUrl("Introduction", "Detail"),
					width: 500,
					callback: function () {
					    CKEDITOR.replace('IntroductionDescription');
						TST.Admin.Introduction.AddPopup.registerEvent();
					}
				});
			});
		},
		AddPopup: {
		    registerEvent: function () {
		        $("#add-introduction-image").on("change", function () {
		            var previewBox = $("#add-introduction-image-preview .box.box-primary").empty();
		            var preview = $("#add-introduction-image-preview");

		            preview.show();
		            function readAndPreview(i, file) {
		                if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
		                    preview.hide();
		                }
		                var reader = new FileReader();
		                $(reader).on("load", function () {
		                    previewBox.append($("<img/>", { src: this.result, height: 100 }));
		                });
		                reader.readAsDataURL(file);
		            }
		            if (this.files && this.files.length > 0) {
		                $.each(this.files, readAndPreview);
		            } else {
		                preview.hide();
		            }
		        });

				$("#save-introduction-btn").on("click", function () {
					var form = $("#add-introduction-form");
					if (form.isValid()) {
					    for (instance in CKEDITOR.instances) {
					        CKEDITOR.instances[instance].updateElement();
					    }
						var formData = new FormData(form[0]);
						TST.Helpers.ajax({
							controller: "Introduction",
							action: "Save",
							processData: false,
							contentType: false,
							data: formData,
							method: "POST",
							callback: function (data) {
								TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
								TST.Admin.Introduction.ReloadTable();
								TST.Helpers.closePopup();
							}
						});
					}
				});
			}
		},
		InitTable: function () {
		    var table = $(this.TableId);
		    var fileUrl = $("#file-url").val();
			var deleteButton = $("#delete-introduction-btn");
			table.DataTable({
				//dom: "lrtip",
				//select: {
				//	style: "single",
				//	info: false
				//},
				ajax: {
					url: TST.Helpers.resolveUrl("Introduction", "GetData")
				},
				columns: [
					{
					    title: "Tiêu đề",
						data: "Title"
					},
                    {
                        title: "Hình ảnh",
                        data: "Image",
                        render: function (data, type, full, meta) {
                            if (data && data != null && data != "") {
                                return `<img src="${fileUrl}/?image=${data}" height="75" style="display: block; margin: 0 auto;">`;
                            }
                            else return ``;
                        }
                    },
                    {
                    	title: "Hiển thị",
                    	data: "IsActive",
                    	render: function (data, type, full, meta) {
                    	    var checked = data ? "checked" : "";
                    		return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked} data-type="isShow">`;
                    	}
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
				],
				//serverSide: true,
				//"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
				"initComplete": function () {
					table.DataTable()
                        .on("select", function () {
                        	deleteButton.enable();
                        })
                        .on("deselect", function () {
                        	deleteButton.disable();
                        });

					table.on("dblclick", "tr", function () {
						var selectedItem = table.DataTable().row(this).select().data();
						if (selectedItem) {
							TST.Helpers.ajaxOpenLargePopup({
								controller: "Introduction",
								action: "Detail",
								data: { introductionId: selectedItem.Id },
								width: 500,
								callback: function () {
								    CKEDITOR.replace('IntroductionDescription');
									TST.Admin.Introduction.AddPopup.registerEvent();
								}
							});
						}
					});

					//event for show/hide and isAbroad
					TST.Admin.Introduction.RegisterCheckBoxActive();
					$(".delete-btn").on("click", function () {
					    var id = $(this).data("index");
					    TST.Helpers.confirmDialog({
					        title: "Xóa giới thiệu",
					        message: "Xác nhận xóa?",
					        callback: function () {
					            TST.Helpers.ajax({
					                url: TST.Helpers.resolveUrl("Introduction", "Delete"),
					                data: { introductionId: id },
					                method: "POST",
					                callback: function () {
					                    TST.Admin.Introduction.ReloadTable();
					                    TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                    TST.Helpers.closePopup();
					                }
					            });
					        }
					    });
					});
					TST.Admin.Introduction.RegisterFilterDatatable(table);
					table.on("draw.dt", function () {
					    TST.Admin.Introduction.RegisterCheckBoxActive();
					    $(".delete-btn").on("click", function () {
					        var id = $(this).data("index");
					        TST.Helpers.confirmDialog({
					            title: "Xóa giới thiệu",
					            message: "Xác nhận xóa?",
					            callback: function () {
					                TST.Helpers.ajax({
					                    url: TST.Helpers.resolveUrl("Introduction", "Delete"),
					                    data: { introductionId: id },
					                    method: "POST",
					                    callback: function () {
					                        TST.Admin.Introduction.ReloadTable();
					                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                        TST.Helpers.closePopup();
					                    }
					                });
					            }
					        });
					    });
					});
				}
			});
		},

		RegisterCheckBoxActive: function () {
		    TST.Helpers.initCheckbox(this.TableId);

			$("input[data-type='isShow']", this.TableId).on("ifChecked", function (event) {
			    TST.Admin.Introduction.UpdateActiveStatus($(this).data("id"), true);
			});

			$("input[data-type='isShow']", this.TableId).on("ifUnchecked", function (event) {
			    TST.Admin.Introduction.UpdateActiveStatus($(this).data("id"), false);
			});
		},
		RegisterFilterDatatable: function (table) {
			$('#introduction-by-Title').on('keyup', function () {
				table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
			});
		},
		UpdateActiveStatus: function (introductionId, isActive) {
			TST.Helpers.ajax({
				url: TST.Helpers.resolveUrl("Introduction", "UpdateActiveStatus"),
				data: {
					introductionId: introductionId,
					isActive: isActive
				},
				method: "POST",
				callback: function (data) {
					TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
				}
			});
		},

		ReloadTable: function () {
		    $(this.TableId).DataTable().ajax.reload();
		    $("delete-introduction-btn").disable();
		}
	}

})(jQuery, window.TST = window.TST || {});