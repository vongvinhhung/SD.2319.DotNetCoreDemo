﻿(function ($, TST) {
	TST.Admin = TST.Admin || {}

	TST.Admin.Nation = {
		TableId: "#nation-table",
		RegisterEvent: function () {
			$("#new-nation-btn").on("click", function () {
				TST.Helpers.ajaxOpenPopup({
					url: TST.Helpers.resolveUrl("Nation", "Detail"),
					width: 500,
					callback: function () {
						TST.Admin.Nation.AddPopup.registerEvent();
					}
				});
			});
		},
		AddPopup: {
			registerEvent: function () {
				$("#save-nation-btn").on("click", function () {
					var form = $("#add-nation-form");
					if (form.isValid()) {
						var formData = new FormData(form[0]);
						TST.Helpers.closePopup();
						TST.Helpers.ajax({
							controller: "Nation",
							action: "Save",
							processData: false,
							contentType: false,
							data: formData,
							method: "POST",
							callback: function (data) {
								TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
								TST.Admin.Nation.ReloadTable();
							}
						});
					}
				});
			}
		},
		InitTable: function () {
			var table = $(this.TableId);
			var deleteButton = $("#delete-nation-btn");
			table.DataTable({
				//dom: "lrtip",
				//select: {
				//	style: "single",
				//	info: false
				//},
				ajax: {
					url: TST.Helpers.resolveUrl("Nation", "GetData")
				},
				columns: [
					{
						title: "Tên",
						data: "Name"
					},
                    {
                        title: "Mã",
                        data: "Sign"
                    },
                    {
                    	title: "Hiển thị",
                    	data: "IsActive",
                    	render: function (data, type, full, meta) {
                    	    var checked = data ? "checked" : "";
                    	    return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                    	}
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
				],
				//serverSide: true,
				//"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
				"initComplete": function () {
					table.DataTable()
                        .on("select", function () {
                        	deleteButton.enable();
                        })
                        .on("deselect", function () {
                        	deleteButton.disable();
                        });

					table.on("dblclick", "tr", function () {
						var selectedItem = table.DataTable().row(this).select().data();
						if (selectedItem) {
							TST.Helpers.ajaxOpenPopup({
								controller: "Nation",
								action: "Detail",
								data: { nationId: selectedItem.Id },
								width: 500,
								callback: function () {
									TST.Admin.Nation.AddPopup.registerEvent();
								}
							});
						}
					});

					//event for show/hide and isAbroad
					TST.Admin.Nation.RegisterCheckBoxActive();
					$(".delete-btn").on("click", function () {
					    var id = $(this).data("index");
					    TST.Helpers.confirmDialog({
					        title: "Xóa quốc gia",
					        message: "Xác nhận xóa?",
					        callback: function () {
					            TST.Helpers.ajax({
					                url: TST.Helpers.resolveUrl("Nation", "Delete"),
					                data: { nationId: id },
					                method: "POST",
					                callback: function () {
					                    TST.Admin.Nation.ReloadTable();
					                    TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                    TST.Helpers.closePopup();
					                }
					            });
					        }
					    });
					});
					TST.Admin.Nation.RegisterFilterDatatable(table);
					table.on("draw.dt", function () {
					    TST.Admin.Nation.RegisterCheckBoxActive();
					    $(".delete-btn").on("click", function () {
					        var id = $(this).data("index");
					        TST.Helpers.confirmDialog({
					            title: "Xóa quốc gia",
					            message: "Xác nhận xóa?",
					            callback: function () {
					                TST.Helpers.ajax({
					                    url: TST.Helpers.resolveUrl("Nation", "Delete"),
					                    data: { nationId: id },
					                    method: "POST",
					                    callback: function () {
					                        TST.Admin.Nation.ReloadTable();
					                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                        TST.Helpers.closePopup();
					                    }
					                });
					            }
					        });
					    });
					});
				}
			});
		},

		RegisterCheckBoxActive: function () {
		    TST.Helpers.initCheckbox(this.TableId);

		    $("input", this.TableId).on("ifChecked", function (event) {
			    TST.Admin.Nation.UpdateActiveStatus($(this).data("id"), true);
			});

		    $("input", this.TableId).on("ifUnchecked", function (event) {
			    TST.Admin.Nation.UpdateActiveStatus($(this).data("id"), false);
			});
		},

		RegisterFilterDatatable: function (table) {
		    $('#nation-by-Name').on('keyup', function () {
		        table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
		    });
		},

		UpdateActiveStatus: function (nationId, isActive) {
			TST.Helpers.ajax({
				url: TST.Helpers.resolveUrl("Nation", "UpdateActiveStatus"),
				data: {
					nationId: nationId,
					isActive: isActive
				},
				method: "POST",
				callback: function (data) {
					TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
				}
			});
		},

		ReloadTable: function () {
		    $(this.TableId).DataTable().ajax.reload();
		    $("#delete-nation-btn").disable();
		}
	}

})(jQuery, window.TST = window.TST || {});