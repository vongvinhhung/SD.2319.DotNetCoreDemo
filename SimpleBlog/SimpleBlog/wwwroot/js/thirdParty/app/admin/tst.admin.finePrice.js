﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.FinePrice = {
        TableId: "#finePrice-table",
        RegisterEvent: function () {
            $("#new-finePrice-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "FinePrice",
                    action: "Detail",
                    data: { finePriceId: 0, tourgroupId: $("#TourGroup_Id").val() },
                    width: 500,
                    callback: function () {
                        TST.Admin.FinePrice.AddPopup.registerEvent();
                    }
                });
            });
        },
        AddPopup: {
            registerEvent: function () {
                $("#save-finePrice-btn").on("click", function () {
                    var form = $("#add-finePrice-form");
                    if (form.isValid()) {
                        var formData = new FormData(form[0]);
                        TST.Helpers.closePopup();
                        TST.Helpers.ajax({
                            controller: "FinePrice",
                            action: "Save",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                                TST.Admin.FinePrice.ReloadTable();
                            }
                        });
                    }
                });
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            var deleteButton = $("#delete-finePrice-btn");
            table.DataTable({
                ajax: {
                    url: TST.Helpers.resolveUrl("FinePrice", "GetData"),
                    data: {
                        id: $("#TourGroup_Id").val()
                    }
                },
                columns: [
                    {
                        title: "Số ngày",
                        data: "Days"
                    },
                    {
                        title: "Phí hủy theo phần trăm giá vé",
                        data: "Percent"
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" category-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
                ],
                serverSide: true,
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "FinePrice",
                                action: "Detail",
                                data: { finePriceId: selectedItem.Id, tourgroupId: $("#TourGroup_Id").val() },
                                width: 500,
                                callback: function () {
                                    TST.Admin.FinePrice.AddPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.FinePrice.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa",
                            message: "Xác nhận xóa?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    url: TST.Helpers.resolveUrl("FinePrice", "Delete"),
                                    data: {
                                        finePriceId: id,
                                        tourgroupId: 6
                                    },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.FinePrice.ReloadTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    table.on("draw.dt", function () {
                        TST.Admin.FinePrice.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa",
                                message: "Xác nhận xóa?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        url: TST.Helpers.resolveUrl("FinePrice", "Delete"),
                                        data: {
                                            finePriceId: id,
                                            tourgroupId: 6
                                        },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.FinePrice.ReloadTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
        },

        RegisterCheckBoxActive: function () {
            var checkBoxs = $("input", this.TableId);

            for (let i = 0; i < checkBoxs.length; i++) {
                var cb = $(checkBoxs[i]);
                if (cb) {
                    cb.iCheck({
                        checkboxClass: "icheckbox_square-red",
                        increaseArea: "20%"
                    });
                    if (cb.val() === "true") cb.iCheck("check");
                }
            }

            $("input", this.TableId).on("ifChecked", function (event) {
                TST.Admin.FinePrice.UpdateActiveStatus(event.currentTarget.getAttribute("category-id"), true);
            });

            $("input", this.TableId).on("ifUnchecked", function (event) {
                TST.Admin.FinePrice.UpdateActiveStatus(event.currentTarget.getAttribute("category-id"), false);
            });
        },

        UpdateActiveStatus: function (finePriceId, isActive) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("FinePrice", "UpdateActiveStatus"),
                data: {
                    finePriceId: finePriceId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});