﻿(function ($, TST) {
	TST.Admin = TST.Admin || {}

	TST.Admin.City = {
		TableId: "#city-table",
		RegisterEvent: function () {
			$("#new-city-btn").on("click", function () {
				TST.Helpers.ajaxOpenPopup({
					url: TST.Helpers.resolveUrl("City", "Detail"),
					width: 500,
					callback: function () {
						TST.Admin.City.AddPopup.registerEvent();
					}
				});
			});
		},
		AddPopup: {
			registerEvent: function () {
				$("#save-city-btn").on("click", function () {
					var form = $("#add-city-form");
					if (form.isValid()) {
						var formData = new FormData(form[0]);
						TST.Helpers.closePopup();
						TST.Helpers.ajax({
							controller: "City",
							action: "Save",
							processData: false,
							contentType: false,
							data: formData,
							method: "POST",
							callback: function (data) {
								TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
								TST.Admin.City.ReloadTable();
							}
						});
					}
				});
			}
		},
		InitTable: function () {
			var table = $(this.TableId);
			var deleteButton = $("#delete-city-btn");
			table.DataTable({
				//dom: "lrtip",
				//select: {
				//	style: "single",
				//	info: false
				//},
				ajax: {
					url: TST.Helpers.resolveUrl("City", "GetData")
				},
				columns: [
					{
						title: "Tên",
						data: "Name"
					},
                    {
                        title: "Mã",
                        data: "Sign"
                    },
                    {
                    	title: "Quốc gia",
                    	data: "Nation"
                    },
                    {
                    	title: "Hiển thị",
                    	data: "IsActive",
                    	render: function (data, type, full, meta) {
                    	    var checked = data ? "checked" : "";
                    	    return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                    	}
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
				],
				//serverSide: true,
				//"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
				"initComplete": function () {
					table.DataTable()
                        .on("select", function () {
                        	deleteButton.enable();
                        })
                        .on("deselect", function () {
                        	deleteButton.disable();
                        });

					table.on("dblclick", "tr", function () {
						var selectedItem = table.DataTable().row(this).select().data();
						if (selectedItem) {
							TST.Helpers.ajaxOpenPopup({
								controller: "City",
								action: "Detail",
								data: { cityId: selectedItem.Id },
								width: 500,
								callback: function () {
									TST.Admin.City.AddPopup.registerEvent();
								}
							});
						}
					});

					//event for show/hide and isAbroad
					TST.Admin.City.RegisterCheckBoxActive();
					$(".delete-btn").on("click", function () {
					    var id = $(this).data("index");
					    TST.Helpers.confirmDialog({
					        title: "Xóa thành phố",
					        message: "Xác nhận xóa?",
					        callback: function () {
					            TST.Helpers.ajax({
					                url: TST.Helpers.resolveUrl("City", "Delete"),
					                data: { cityId: id },
					                method: "POST",
					                callback: function () {
					                    TST.Admin.City.ReloadTable();
					                    TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                    TST.Helpers.closePopup();
					                }
					            });
					        }
					    });
					});
					TST.Admin.City.RegisterFilterDatatable(table);
					table.on("draw.dt", function () {
					    TST.Admin.City.RegisterCheckBoxActive();
					    $(".delete-btn").on("click", function () {
					        var id = $(this).data("index");
					        TST.Helpers.confirmDialog({
					            title: "Xóa thành phố",
					            message: "Xác nhận xóa?",
					            callback: function () {
					                TST.Helpers.ajax({
					                    url: TST.Helpers.resolveUrl("City", "Delete"),
					                    data: { cityId: id },
					                    method: "POST",
					                    callback: function () {
					                        TST.Admin.City.ReloadTable();
					                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
					                        TST.Helpers.closePopup();
					                    }
					                });
					            }
					        });
					    });
					});
				}
			});
		},

		RegisterCheckBoxActive: function () {
		    TST.Helpers.initCheckbox(this.TableId);

		    $("input", this.TableId).on("ifChecked", function (event) {
			    TST.Admin.City.UpdateActiveStatus($(this).data("id"), true);
			});

		    $("input", this.TableId).on("ifUnchecked", function (event) {
			    TST.Admin.City.UpdateActiveStatus($(this).data("id"), false);
			});
		},

		RegisterFilterDatatable: function (table) {
		    $('#city-by-Name').on('keyup', function () {
		        table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
		    });
		    $('#city-by-Nation').on('keyup', function () {
		        table.DataTable().columns(1)
                    .search(this.value)
                    .draw();
		    });
		},

		UpdateActiveStatus: function (cityId, isActive) {
			TST.Helpers.ajax({
				url: TST.Helpers.resolveUrl("City", "UpdateActiveStatus"),
				data: {
					cityId: cityId,
					isActive: isActive
				},
				method: "POST",
				callback: function (data) {
					TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
				}
			});
		},

		ReloadTable: function () {
		    $(this.TableId).DataTable().ajax.reload();
		    $("delete-city-btn").disable();
		}
	}

})(jQuery, window.TST = window.TST || {});