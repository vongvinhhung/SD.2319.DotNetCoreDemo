﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.News = {
        TableId: "#news-table",
        RegisterEvent: function () {
            $("#new-news-btn").on("click", function () {
                TST.Helpers.ajaxOpenLargePopup({
                    url: TST.Helpers.resolveUrl("News", "Detail"),
                    width: 500,
                    callback: function () {
                        CKEDITOR.replace('NewsContent2');
                        TST.Admin.News.AddPopup.registerEvent();
                        TST.Admin.Video.AddVideoPopup.RegisterTagsEvent("#Tags");
                    }
                });
            });
        },
        AddPopup: {
            registerEvent: function () {
                $("#add-news-image").on("change", function () {
                    var previewBox = $("#add-news-image-preview .box.box-primary").empty();
                    var preview = $("#add-news-image-preview");

                    preview.show();
                    function readAndPreview(i, file) {
                        if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                            preview.hide();
                        }
                        var reader = new FileReader();
                        $(reader).on("load", function () {
                            previewBox.append($("<img/>", { src: this.result, height: 100 }));
                        });
                        reader.readAsDataURL(file);
                    }
                    if (this.files && this.files.length > 0) {
                        $.each(this.files, readAndPreview);
                    } else {
                        preview.hide();
                    }
                });
                $("#save-news-btn").on("click", function () {
                    var form = $("#add-news-form");
                    if (form.isValid()) {
                        for (instance in CKEDITOR.instances) {
                            CKEDITOR.instances["NewsContent2"].updateElement();
                        }
                        var formData = new FormData(form[0]);
                        TST.Helpers.ajax({
                            controller: "News",
                            action: "Save",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                                TST.Admin.News.ReloadTable();
                                TST.Helpers.closePopup();
                            }
                        });
                    }
                });
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            var deleteButton = $("#delete-news-btn");
            var fileUrl = $("#file-url").val();
            table.DataTable({
                order: [[5, "desc"]],
                //dom: "lrtip",
                //select: {
                //    style: "single",
                //    info: false
                //},
                ajax: {
                    url: TST.Helpers.resolveUrl("News", "GetData")
                },
                columns: [
                    {
                        title: "Tiêu đề",
                        data: "Title"
                    },
                    {
                        title: "Chuyên mục",
                        data: "Category",
                        width: "10%"
                    },
                    {
                        title: "Mô tả",
                        data: "Description"
                    },
                    {
                        title: "Hiển thị",
                        data: "IsShowed",
                        width: "100px",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    },
                    {
                        title: "Id",
                        data: "Id",
                        visible: false
                    }
                ],
                //serverSide: true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenLargePopup({
                                controller: "News",
                                action: "Detail",
                                data: { newsId: selectedItem.Id },
                                callback: function () {
                                    CKEDITOR.replace('NewsContent2');
                                    TST.Admin.News.AddPopup.registerEvent();
                                    TST.Admin.Video.AddVideoPopup.RegisterTagsEvent("#Tags");
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.News.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa danh mục tin tức",
                                message: "Xác nhận xóa?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        url: TST.Helpers.resolveUrl("News", "Delete"),
                                        data: { newsId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.News.ReloadTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    TST.Admin.News.RegisterFilterDatatable(table);
                    table.on("draw.dt", function () {
                        TST.Admin.News.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa danh mục tin tức",
                                message: "Xác nhận xóa?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        url: TST.Helpers.resolveUrl("News", "Delete"),
                                        data: { newsId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.News.ReloadTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
            
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox(this.TableId);

            $("input", this.TableId).on("ifChecked", function (event) {
                TST.Admin.News.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input", this.TableId).on("ifUnchecked", function (event) {
                TST.Admin.News.UpdateActiveStatus($(this).data("id"), false);
            });
        },
        RegisterFilterDatatable: function (table) {
            $('#titleNewsFilter').on('keyup', function () {
                table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
            });
            $('#descriptionNewsFilter').on('keyup', function () {
                table.DataTable().columns(2)
                    .search(this.value)
                    .draw();
            });
            $('#categoryNewsFilter').on('change', function () {
                table.DataTable().columns(1)
                    .search($("#categoryNewsFilter option:selected").val())
                    .draw();
            });
        },
        UpdateActiveStatus: function (newsId, isActive) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("News", "UpdateActiveStatus"),
                data: {
                    newsId: newsId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});