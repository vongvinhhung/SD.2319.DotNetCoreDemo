﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.ImageGallery = {
        RegisterEvent: function () {

            $("#new-image-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "ImageGallery",
                    action: "AddGalleryPopup",
                    width: 500,
                    callback: function () {
                        TST.Admin.ImageGallery.AddImageGalleryPopup.registerEvent();
                        TST.Admin.ImageGallery.AddImageGalleryPopup.RegisterTagsEvent();
                    }
                });
            });

            $("#detail-gallery-btn").on("click", function () {
                var imageTable = $("#image-table");
                var selectedItem = imageTable.DataTable().rows({ selected: true }).data()[0];

                TST.Helpers.ajaxLoadNewContent({
                    controller: "Image",
                    action: "Index",
                    data: { galleryId: selectedItem.Id },
                    method: "GET",
                    callback: function () {
                        
                    }
                });
            });

            $("#image-categories-ddl").on("change", function () {
                var dataTable = $("#image-table").DataTable();
                var filterValue = $(this).val();
                if (filterValue) {
                    dataTable.search(filterValue).draw();
                }
            });
        },

        AddImageGalleryPopup: {
            registerEvent: function () {
                $("#add-image").on("change", function () {
                    var previewBox = $("#add-image-preview .box.box-primary").empty();
                    var preview = $("#add-image-preview");

                    preview.show();

                    if (this.files && this.files.length > 0) {
                        $.each(this.files, readAndPreview);
                    } else {
                        preview.hide();
                    }

                    function readAndPreview(i, file) {
                        if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                            preview.hide();
                        }
                        var reader = new FileReader();
                        $(reader).on("load", function () {
                            previewBox.append($("<img/>", { src: this.result, height: 100 }));
                        });
                        reader.readAsDataURL(file);
                    }
                });

                var saveBtn = $("#save-image-btn");
                if (saveBtn) {
                    saveBtn.on("click", function () {
                        var form = $("#add-image-form");
                        if (form.isValid()) {
                            var formData = new FormData(form[0]);
                            formData.append("LookupId", $("#LookupId").val());
                            TST.Helpers.ajax({
                                controller: "ImageGallery",
                                action: "AddGallery",
                                processData: false,
                                contentType: false,
                                data: formData,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.ImageGallery.ReloadImageGalleryTable();
                                    TST.Helpers.showAlert("Thêm thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

                var editBtn = $("#edit-image-btn");
                if (editBtn) {
                    editBtn.on("click", function () {
                        var form = $("#edit-image-form");
                        if (form.isValid()) {
                            var formData = new FormData(form[0]);
                            formData.append("LookupId", $("#LookupId").val());
                            TST.Helpers.ajax({
                                controller: "ImageGallery",
                                action: "EditGallery",
                                processData: false,
                                contentType: false,
                                data: formData,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.ImageGallery.ReloadImageGalleryTable();
                                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }
            },

            RegisterTagsEvent: function () {
                var url = TST.Helpers.resolveUrl("Tags", "GetAllTags");
                var tags = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace("Name"),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch: {
                        url: url,
                        filter: function (tags) {
                            return $.map(tags, function (tag) {
                                return {
                                    Name: tag.Name
                                };
                            });
                        },
                        cache: false
                    }
                });
                tags.initialize();

                $("#Gallery_Tags").tagsinput({
                    typeaheadjs: {
                        name: "tags",
                        source: tags.ttAdapter(),
                        displayKey: 'Name',
                        valueKey: 'Name'
                    }
                });
            }
        },


        InitImageGalleryTable: function () {
            var imageTable = $("#image-table");
            var galleryDetailBtn = $("#detail-gallery-btn");
            var deleteButton = $("#delete-image-btn");
            var fileUrl = $("#file-url").val();

            imageTable.DataTable({
                order: [[6, "desc"]],
                ajax: {
                    url: TST.Helpers.resolveUrl("ImageGallery", "GetGalleriesData")
                },
                columns: [
                    {
                        title: "Hình ảnh",
                        data: "PathName",
                        render: function (data, type, full, meta) {
                            return `<img src="${fileUrl}/?image=${data}" height="75" style="display: block; margin: 0 auto;">`;
                        }
                    },
                    {
                        title: "Tên",
                        data: "Name"
                    },
                    {
                        title: "Danh mục",
                        data: "Lookup.Description"

                    },
                    {
                        title: "Tags",
                        data: "Tags",
                        render: function (data, type, full, meta) {
                            if (data) {
                                return `<input class ="form-control tag-column" data-role="tagsinput" value="${data}">`;
                            }
                            return "";
                        }

                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    },
                    {
                        title: "Id",
                        data: "Id",
                        visible: false
                    }
                ],
                stateSave: true,
                //serverSide: true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {

                    //select event
                    imageTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                            galleryDetailBtn.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                            galleryDetailBtn.disable();
                        });


                    imageTable.on("dblclick", "tr", function () {
                        var selectedItem = imageTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "ImageGallery",
                                action: "EditGalleryPopup",
                                data: { galleryId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.ImageGallery.AddImageGalleryPopup.RegisterTagsEvent();
                                    TST.Admin.ImageGallery.AddImageGalleryPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.ImageGallery.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa hình",
                            message: "Bạn có muốn xóa gallery này?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "ImageGallery",
                                    action: "DeleteGallery",
                                    data: { galleryId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.ImageGallery.ReloadImageGalleryTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    imageTable.on("draw.dt", function () {
                        TST.Admin.ImageGallery.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa hình",
                                message: "Bạn có muốn xóa gallery này?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        controller: "ImageGallery",
                                        action: "DeleteGallery",
                                        data: { galleryId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.ImageGallery.ReloadImageGalleryTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                },

                "drawCallback": function (settings) {
                    //show tag in datatable 
                    $(".tag-column", "#image-table").each(function () {
                        $(this).tagsinput();
                        $(this).prev().removeClass('bootstrap-tagsinput');
                        $(this).prev().find("input").remove();
                    });
                }
            });
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox("#image-table");

            $("input", "#image-table").on("ifChecked", function (event) {
                TST.Admin.ImageGallery.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input", "#image-table").on("ifUnchecked", function (event) {
                TST.Admin.ImageGallery.UpdateActiveStatus($(this).data("id"), false);
            });
        },

        UpdateActiveStatus: function (galleryId, isActive) {
            TST.Helpers.ajax({
                controller: "ImageGallery",
                action: "UpdateGalleryActiveStatus",
                data: {
                    galleryId: galleryId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadImageGalleryTable: function () {
            $("#image-table").DataTable().ajax.reload();
            $("#delete-image-btn").disable();
            $("#detail-gallery-btn").disable();
        }
    }

})(jQuery, window.TST = window.TST || {});