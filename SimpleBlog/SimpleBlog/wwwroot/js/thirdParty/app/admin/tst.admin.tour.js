﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.Tour = {
        RegisterTagsEvent: function (control) {
            var url = TST.Helpers.resolveUrl("Tags", "GetAllTags");
            var tags = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Name'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: {
                    url: url,
                    filter: function (tags) {
                        return $.map(tags, function (tag) {
                            return {
                                Name: tag.Name
                            };
                        });
                    },
                    cache: false
                }
            });
            tags.initialize();

            $(control).tagsinput({
                typeaheadjs: {
                    name: "tags",
                    source: tags.ttAdapter(),
                    displayKey: 'Name',
                    valueKey: 'Name'
                }
            });
        },
        LoadFilterArea: function () {
            TST.Helpers.ajax({
                controller: "Tour",
                action: "LoadFilterArea",
                callback: function (data) {
                    $("#FilterArea").html("");
                    $("#FilterArea").append(data);
                }
            });

        },
        RegisterEventFilterArea: function () {

            $("#Tour_AreaId").off("change").on("change", function () {
                var Column = $("#tour-table").DataTable().columns(7);
                var searchValue = $(this).val();
                if (Column.search() !== searchValue) {
                    Column.search(searchValue).draw();
                }
            });

        },
        RegisterEventPriceTextBox: function () {
            $(".pricetypetour-textBox").off("change textInput input").on("change textInput input", function () {
                $(this).data("change", "True");
            });
            $(".pricetypetour-textBox").off("focusout").on("focusout", function () {
                if ($(this).data("change") == "True") {
                    $(this).data("change", "False");
                    TST.Helpers.ajax({
                        controller: "Tour",
                        action: "UpdatePrice",
                        method: "POST",
                        data: {
                            categoryId: $(this).data("category"),
                            pricetypeId: $(this).data("pricetype"),
                            tourId: $(this).data("tourid"),
                            value: $(this).val()
                        },
                        callback: function () {
                            $(this).data("change", "False");
                            TST.Helpers.showAlert("Sửa giá thành công!", "success", "check");
                        }
                    });
                }
            });

            TST.Admin.Tour.registerCheckboxEventForPriceDiv();
        },

        registerCheckboxEventForPriceDiv: function () {

            TST.Helpers.initCheckbox("#price-div");
            $(".is-active-chk", "#price-div").on("ifChecked", function () {
                TST.Admin.Tour.UpdatePriceTypeActiveStatus($(this).data("type-id"), true);
            });
            $(".is-active-chk", "#price-div").on("ifUnchecked", function () {
                TST.Admin.Tour.UpdatePriceTypeActiveStatus($(this).data("type-id"), false);
            });

            $(".main-price-chk", "#price-div").on("ifChecked", function () {
                TST.Admin.Tour.UpdateIsMainPriceTypeStatus($(this).data("type-id"), true);
            });
            $(".main-price-chk", "#price-div").on("ifUnchecked", function () {
                TST.Admin.Tour.UpdateIsMainPriceTypeStatus($(this).data("type-id"), false);
            });
        },

        RegisterEvent: function () {
            $("#new-tour-btn").on("click", function () {
                TST.Helpers.ajaxLoadNewContent({
                    controller: "Tour",
                    action: "TourDetail",
                    callback: function () {
                        $("textarea", "#add-tour-form").each(function () {
                            CKEDITOR.replace(this);
                        });
                        TST.Admin.Tour.TourDetail.registerEvent();

                    }
                });

            });

            $('#back-tour-btn').on("click", function () {
                TST.Helpers.ajaxLoadNewContent({
                    controller: "Tour",
                    action: "index",
                    callback: function () {

                    }
                });

            });

            if ($("#TourGroup_Tags"))
            {
                TST.Admin.Tour.RegisterTagsEvent($("#Tour_Tags"));
            }

            var saveBtn = $("#save-tour-detail-btn");
            if (saveBtn) {
                $("#save-tour-detail-btn").on("click", function () {
                    var form = $("#add-tour-form");
                    if (form.isValid()) {
                        for (instance in CKEDITOR.instances) {
                            CKEDITOR.instances[instance].updateElement();
                        }
                        var formData = new FormData(form[0]);
                        formData.Tags = $("#Tour_Tags").tagsinput('items').toString();

                        TST.Helpers.ajax({
                            controller: "Tour",
                            action: "EditTour",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Admin.Tour.ReloadTourTable();
                                TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                                TST.Helpers.closePopup();
                                TST.Helpers.ajaxLoadNewContent({
                                    controller: "Tour",
                                    action: "index",
                                    callback: function () {

                                    }
                                });
                            }
                        });
                    }
                });
            }

            var addBtn = $("#add-tour-detail-btn");
            if (addBtn) {
                addBtn.on("click", function () {
                    var form = $("#add-tour-form");
                    if (form.isValid()) {
                        for (instance in CKEDITOR.instances) {
                            CKEDITOR.instances[instance].updateElement();
                        }
                        var formData = new FormData(form[0]);
                        formData.Tags = $("#Tour_Tags").tagsinput('items').toString();

                        TST.Helpers.ajax({
                            controller: "Tour",
                            action: "AddTour",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Admin.Tour.ReloadTourTable();
                                TST.Helpers.showAlert("Lưu thành công !", "success", "check");
                                TST.Helpers.ajaxLoadNewContent({
                                    controller: "Tour",
                                    action: "index",
                                    callback: function () {

                                    }
                                });
                            }
                        });
                    }
                });
            }

            var addTourDetailBtn = $("#add-custom-tour-detail-btn");
            if (addTourDetailBtn) {
                addTourDetailBtn.on("click", function () {

                    TST.Helpers.ajaxOpenPopup({
                        controller: "Tour",
                        width: 800,
                        action: "AddTourDetailPopup",
                        callback: function (data) {
                            $("#RelateId").val($("#Tour_Id").val());
                            TST.Admin.Tour.AddTourDetailPopup.registerEvent();
                        }
                    });
                });
            }
            $("#add-custom-location-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "Tour",
                    action: "LocationDetailPopup",
                    data: {
                        id: 0,
                        tourId: $("#Tour_Id").val()
                    },
                    callback: function (data) {
                        //$("#RelateId").val($("#Tour_Id").val());
                        TST.Admin.Tour.AddLocationPopup.registerEvent();
                    }
                });
            });

            $("#add-tour-image").on("change", function () {
                var previewBox = $("#add-tour-preview .box.box-primary").empty();
                var preview = $("#add-tour-preview");
                preview.show();
                if (this.files && this.files.length > 0) {
                    $.each(this.files, readAndPreview);
                } else {
                    preview.hide();
                }
                function readAndPreview(i, file) {
                    if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                        preview.hide();
                    }
                    var reader = new FileReader();
                    $(reader).on("load", function () {
                        previewBox.append($("<img/>", { src: this.result, height: 100 }));
                    });
                    reader.readAsDataURL(file);
                }
            });

            $("#add-tour-pricetype-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "Tour",
                    action: "DetailPriceType",
                    callback: function (data) {
                        var addPriceTypeBtn = $("#save-price-type-btn");
                        if (addPriceTypeBtn) {
                            addPriceTypeBtn.on("click", function (e) {
                                e.preventDefault();
                                TST.Helpers.ajax({
                                    controller: "Tour",
                                    action: "AddPriceType",
                                    data: {
                                        nameType: $("#add-price-type-form #Description").val(),
                                        tourId: $("#Tour_Id").val()
                                    },
                                    method: "POST",
                                    callback: function (data) {
                                        TST.Helpers.ajax({
                                            controller: "Tour",
                                            action: "LoadGridPrice",
                                            data: { tourId: $("#Tour_Id").val() },
                                            callback: function (data) {
                                                $("#price-div").html("");
                                                $("#price-div").append(data);
                                                TST.Admin.Tour.RegisterEventPriceTextBox();
                                            }
                                        });
                                        //TST.Admin.Tour.ReloadTourDetailTable();
                                        TST.Admin.Tour.registerCheckboxEventForPriceDiv();
                                        TST.Helpers.showAlert("Thêm loại giá thành công !", "success", "check");
                                        TST.Helpers.closePopup();

                                    }
                                });
                            });
                        }
                    }
                });
                //var newTypeName = $("#new-price-type-name").val();
                //if (newTypeName == undefined || newTypeName == "") {
                //    TST.Helpers.showAlert("Làm ơn nhập tên loại giá !", "error", "check");
                //}
                //else {
                //    TST.Helpers.ajax({
                //        controller: "Tour",
                //        action: "AddPriceType",
                //        data: {
                //            nameType: newTypeName,
                //            tourId: $("#Tour_Id").val()
                //        },
                //        method: "POST",
                //        callback: function (data) {
                //            TST.Helpers.ajax({
                //                controller: "Tour",
                //                action: "LoadGridPrice",
                //                data: { tourId: $("#Tour_Id").val() },
                //                callback: function (data) {
                //                    $("#price-div").html(data);
                //                }
                //            });
                //            TST.Helpers.initCheckbox("#price-div");
                //            //TST.Admin.Tour.ReloadTourDetailTable();
                //            TST.Helpers.showAlert("Thêm loại giá thành công !", "success", "check");
                //            //TST.Helpers.closePopup();

                //        }
                //    });
                //}
            });

            TST.Admin.Tour.RegisterEventPriceTextBox();

            $(document).on("click", ".delete-pricetype-btn", function () {
                //var table = $(TST.Admin.PriceDefault.TableId);
                //var selectedItem = table.DataTable().rows({ selected: true }).data()[0];

                var priceTypeId = $(this).data("index");

                TST.Helpers.confirmDialog({
                    title: "Xóa loại giá",
                    message: "Xác nhận xóa?",
                    callback: function () {
                        TST.Helpers.ajax({
                            url: TST.Helpers.resolveUrl("Tour", "DeletePriceType"),
                            data: { priceTypeId: priceTypeId },
                            method: "POST",
                            callback: function () {
                                //TST.Admin.PriceDefault.ReloadTable();
                                TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                TST.Helpers.closePopup();
                                TST.Helpers.ajax({
                                    controller: "Tour",
                                    action: "LoadGridPrice",
                                    data: { tourId: $("#Tour_Id").val() },
                                    callback: function (data) {
                                        $("#price-div").html("");
                                        $("#price-div").append(data);
                                        //TST.Helpers.initCheckbox("#price-div");
                                        TST.Admin.Tour.RegisterEventPriceTextBox();
                                        TST.Admin.Tour.registerCheckboxEventForPriceDiv();
                                    }
                                });
                            }
                        });
                    }
                });
            });

            $("#filter-by-tourname").off("change").on("change", function () {
                var Column = $("#tour-table").DataTable().columns(2);
                var searchValue = $(this).val();
                if (Column.search() !== searchValue) {
                    Column.search(searchValue).draw();
                }

                //TST.Admin.Tour.ReloadTourTable();
            });

            $("#filter-by-tourcode").off("change").on("change", function () {
                var Column = $("#tour-table").DataTable().columns(1);
                var searchValue = $(this).val();
                if (Column.search() !== searchValue) {
                    Column.search(searchValue).draw();
                }
                //TST.Admin.Tour.ReloadTourTable();
            });
            $("#Tour_NationId").on("change", function () {
                TST.Helpers.ajax({
                    controller: "Tour",
                    action: "GetCitiesByNationId",
                    data: { nationId: $("#Tour_NationId").val() },
                    cache: false,
                    method: "POST",
                    callback: function (data) {
                        var markup = "";
                        for (var x = 0; x < data.length; x++) {
                            markup += "<option value=" + data[x].Value + ">" + data[x].Text + "</option>";
                        }
                        $("#Tour_CityId").html(markup).show();
                    },
                });
            });

            $('input[name="Tour_Type"]:radio').on("click", function () {
                var data = { isAboard: $('input[name="Tour_Type"]:checked').val() == "outbound" }
                TST.Helpers.loadDDL("#Tour_AreaId", TST.Helpers.resolveUrl("Tour", "LoadAreaDatas"), data);
                if (data.isAboard) {
                    $("#Tour_NationId").enable();
                }
                else {
                    //TO DO: cẩn thận có dấu trong tiếng việt
                    $("#Tour_NationId").val($("#Tour_NationId option:contains('VIET NAM')").val());
                    $("#Tour_NationId").disable();
                }
            });
        },

        TourDetail: {
            registerEvent: function () {
            }
        },

        AddTourDetailPopup: {
            registerEvent: function () {

                $("textarea", "#add-tour-detail-popup").each(function () {
                    CKEDITOR.replace(this);
                });

                var savePopupBtn = $("#save-tour-detail-popup-btn");
                if (savePopupBtn) {
                    savePopupBtn.on("click", function () {
                        var form = $("#add-tour-detail-popup-form");
                        if (form.isValid()) {
                            for (instance in CKEDITOR.instances) {
                                CKEDITOR.instances[instance].updateElement();
                            }
                            var formData = new FormData(form[0]);
                            TST.Helpers.ajax({
                                width: 800,
                                controller: "Tour",
                                action: "AddTourDetailPopup",
                                processData: false,
                                data: form.serialize(),
                                method: "POST",
                                callback: function (data) {
                                    //TST.Admin.Tour.ReloadTourTable();
                                    TST.Admin.Tour.ReloadTourDetailTable();
                                    TST.Helpers.showAlert("Thêm chi tiết thành công!", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

                var savePopupBtn = $("#update-tour-detail-popup-btn");
                if (savePopupBtn) {
                    savePopupBtn.on("click", function () {
                        var form = $("#add-tour-detail-popup-form");
                        if (form.isValid()) {
                            for (instance in CKEDITOR.instances) {
                                CKEDITOR.instances[instance].updateElement();
                            }
                            var formData = new FormData(form[0]);
                            TST.Helpers.ajax({
                                controller: "Tour",
                                action: "UpdateTourDetailPopup",
                                processData: false,
                                data: form.serialize(),
                                method: "POST",
                                callback: function (data) {
                                    //TST.Admin.Tour.ReloadTourTable();
                                    TST.Admin.Tour.ReloadTourDetailTable();
                                    TST.Helpers.showAlert("Cập nhật chi tiết thành công!", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

            }
        },
        AddLocationPopup: {
            registerEvent: function () {
                var savePopupBtn = $("#save-tour-location-btn");
                if (savePopupBtn) {
                    savePopupBtn.on("click", function () {
                        $("#TourLocations_TourId").val($("#Tour_Id").val());
                        var form = $("#add-tour-location-form");
                        if (form.isValid()) {
                            TST.Helpers.ajax({
                                controller: "Tour",
                                action: "SaveTourLocation",
                                processData: false,
                                data: form.serialize(),
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.Tour.ReloadLocationTable();
                                    TST.Helpers.showAlert("Cập nhật chi tiết thành công!", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }
                $("#TourLocations_NationId").on("change", function () {
                    TST.Helpers.ajax({
                        controller: "Tour",
                        action: "GetCitiesByNationId",
                        data: { nationId: $("#TourLocations_NationId").val() },
                        cache: false,
                        method: "POST",
                        callback: function (data) {
                            var markup = "";
                            for (var x = 0; x < data.length; x++) {
                                markup += "<option value=" + data[x].Value + ">" + data[x].Text + "</option>";
                            }
                            $("#TourLocations_CityId").html(markup).show();
                        },
                    });
                });
            }
        },

        PriceTourPopup: {
            registerEvent: function () {



                var saveBtn = $("#save-price-btn");
                if (saveBtn) {
                    saveBtn.on("click", function () {
                        var form = $("#add-price-default-form");
                        if (form.isValid()) {
                            var formData = new FormData(form[0]);
                            TST.Helpers.ajax({
                                controller: "Tour",
                                action: "SubmitPricePopup",
                                processData: false,
                                data: form.serialize(),
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.Tour.ReloadTourDetailTable();
                                    TST.Helpers.showAlert("Sửa giá thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }


            }
        },

        InitTourTable: function () {
            var tourTable = $("#tour-table");
            var deleteButton = $("#delete-tour-btn");
            var priceButton = $("#price-tour-btn");
            var detailButton = $("#detail-tour-btn");
            var fileUrl = $("#file-url").val();

            tourTable.DataTable({
                order: [[0, "desc"]],
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("Tour", "GetToursData"),
                    "type": 'POST',
                    "data": {
                        "tourname":
                        $("#filter-by-tourname").val(),
                        "tourcode":
                        $("#filter-by-tourcode").val()
                    }
                },
                //"order": [],
                columns: [
                    {
                        title: "Id",
                        data: "Id",
                        visible: false
                    },
                    {
                        title: "Mã tour",
                        data: "TourCode"
                    },
                    {
                        title: "Tên tour",
                        data: "TourName"
                    },
                    {
                        title: "Hình ảnh",
                        data: "Image",
                        render: function (data, type, full, meta) {
                            return `<img src="${fileUrl}/?image=${data}" height="75" style="display: block; margin: 0 auto;">`;
                        }
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" class="chkIsActive" data-id=${full.Id} value=${data} ${checked} >`;
                        }
                    },
                    {
                        title: "Hot tour",
                        data: "IsHotTour",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" class="chkHotTour" data-id=${full.Id} value=${data} ${checked} >`;
                        }
                    },
                    {
                        title: "Xoá",
                        render: function (data, type, full, meta) {
                            return TST.Templates.DeleteButton(full.Id)

                            //return `<input type="button" class="btn btn-danger delete-tour-grid-btn" data-id=${full.Id} value="Xoá" aria-invalid="false">`;
                        }
                    },
                    {
                        title: "Thị trường",
                        "visible": false,
                        "searchable": true,
                        data: "AreaId"
                    }
                ],
                serverSide: true,
                "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {

                    tourTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                            priceButton.enable();
                            detailButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                            priceButton.disable();
                            detailButton.enable();
                        });

                    tourTable.on("dblclick", "tr", function () {
                        var selectedItem = tourTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxLoadNewContent({
                                controller: "Tour",
                                action: "TourDetail",
                                data: { tourId: selectedItem.Id },
                                callback: function () {
                                    $("textarea", "#add-tour-form").each(function () {
                                        CKEDITOR.replace(this);
                                    });
                                    TST.Admin.Tour.TourDetail.registerEvent();

                                }
                            });
                        }


                    });

                    $(document).on("click", "#tour-table .delete-btn", function () {

                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa chi tiết Tour",
                            message: "Bạn có muốn xóa chi tiết tour",
                            dialogType: "danger",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "Tour",
                                    action: "DeleteTour",
                                    data: { tourId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.Tour.ReloadTourTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });

                    TST.Admin.Tour.PriceTourPopup.registerEvent();


                    TST.Helpers.initCheckbox(tourTable);
                    TST.Admin.Tour.RegisterCheckBoxActive(tourTable);
                    TST.Admin.Tour.RegisterCheckBoxHotTour(tourTable);

                    tourTable.on("draw.dt", function () {
                        TST.Helpers.initCheckbox(tourTable);
                        TST.Admin.Tour.RegisterCheckBoxActive(tourTable);
                        TST.Admin.Tour.RegisterCheckBoxHotTour(tourTable);

                        $(document).off("change").on("change", "#filter-by-tourname", function () {
                            TST.Admin.Tour.ReloadTourTable();
                        });

                        $(document).off("change").on("change", "#filter-by-tourcode", function () {
                            TST.Admin.Tour.ReloadTourTable();
                        });
                    });


                }
            });
        },

        InitTourDetailTable: function () {
            var tourTableDetail = $("#tour-detail-table");
            var deleteButton = $("#remove-custom-tour-detail-btn");
            var fileUrl = $("#file-url").val();

            tourTableDetail.DataTable({
                dom: "t",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("Tour", "GetTourDetailsData"),
                    "type": 'POST',
                    "data": {
                        "tourId":
                        $("#Tour_Id").val()
                    }
                },

                columns: [

                    {
                        title: "Tên chi tiết tour",
                        data: "Title"
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked} >`;
                            //return `<input type="checkbox" tour-id=${full.Id} value=${data}>`;
                        }
                    },
                    {
                        title: "Xoá",
                        render: function (data, type, full, meta) {
                            return TST.Templates.DeleteButton(full.Id)
                            //return `<input type="button" class="btn btn-danger delete-tour-detail-grid-btn" data-id=${full.Id} value="Xoá" aria-invalid="false">`;
                        }
                    }
                ],
                serverSide: true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {

                    tourTableDetail.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                            //priceButton.enable();
                            //detailButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                            //priceButton.disable();
                            //detailButton.enable();
                        });

                    tourTableDetail.on("dblclick", "tr", function () {
                        var selectedItem = tourTableDetail.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "Tour",
                                width: 800,
                                action: "UpdateTourDetailPopup",
                                data: { tourDetailId: selectedItem.Id },
                                callback: function () {
                                    //$("textarea", "#add-tour-detail-popup").each(function () {
                                    //    CKEDITOR.replace(this);
                                    //});
                                    TST.Admin.Tour.AddTourDetailPopup.registerEvent();

                                }
                            });
                        }
                    });

                    $(document).on("click", "#tour-detail-table .delete-btn", function (e) {
                        e.preventDefault();
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa chi tiết Tour",
                            message: "Bạn có muốn xóa chi tiết tour",
                            dialogType: "danger",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "Tour",
                                    action: "DeleteTourDetail",
                                    data: { tourDetailId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.Tour.ReloadTourDetailTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });


                    TST.Admin.Tour.PriceTourPopup.registerEvent();

                    TST.Helpers.initCheckbox(tourTableDetail);

                    $("input", tourTableDetail).off("ifChecked").on("ifChecked", function (event) {
                        TST.Admin.Tour.UpdateActiveStatusTourDetail($(this).data("id"), true);
                    });

                    $("input", tourTableDetail).off("ifUnchecked").on("ifUnchecked", function (event) {
                        TST.Admin.Tour.UpdateActiveStatusTourDetail($(this).data("id"), false);
                    });

                    tourTableDetail.on("draw.dt", function () {
                        TST.Helpers.initCheckbox(tourTableDetail);

                        $("input", tourTableDetail).off("ifChecked").on("ifChecked", function (event) {
                            TST.Admin.Tour.UpdateActiveStatusTourDetail($(this).data("id"), true);
                        });

                        $("input", tourTableDetail).off("ifUnchecked").on("ifUnchecked", function (event) {
                            TST.Admin.Tour.UpdateActiveStatusTourDetail($(this).data("id"), false);
                        });
                    });
                }
            });
        },
        InitLocationTable: function () {
            var locationTable = $("#location-table");
            var deleteButton = $("#remove-custom-location-btn");

            locationTable.DataTable({
                dom: "t",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("Tour", "GetLocations"),
                    "type": 'POST',
                    "data": {
                        tourId: $("#Tour_Id").val()
                    }
                },

                columns: [
                    {
                        title: "Số thứ tự",
                        data: "STT"
                    },
                    {
                        title: "Quốc gia",
                        data: "Nation"
                    },
                    {
                        title: "Thành phố",
                        data: "City"
                    },
                    {
                        title: "Xoá",
                        render: function (data, type, full, meta) {
                            return TST.Templates.DeleteButton(full.Id);
                        }
                    }
                ],
                serverSide: true,
                "initComplete": function () {

                    locationTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    locationTable.on("dblclick", "tr", function () {
                        var selectedItem = locationTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "Tour",
                                //action: "UpdateTourDetailPopup",
                                action: "LocationDetailPopup",
                                data: { id: selectedItem.Id, tourId: $("#Tour_Id").val() },
                                callback: function () {
                                    TST.Admin.Tour.AddLocationPopup.registerEvent();
                                }
                            });
                        }
                    });

                    $(document).on("click", "#location-table .delete-btn", function (e) {
                        e.preventDefault();
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa chi tiết Tour",
                            message: "Bạn có muốn xóa địa điểm này?",
                            dialogType: "danger",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "Tour",
                                    action: "DeleteTourLocation",
                                    data: { tourLocationId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.Tour.ReloadLocationTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                }
            });
        },

        RegisterCheckBoxActive: function (tourTable) {
            $("input.chkIsActive", tourTable).on("ifChecked", function (event) {
                TST.Admin.Tour.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input.chkIsActive", tourTable).on("ifUnchecked", function (event) {
                TST.Admin.Tour.UpdateActiveStatus($(this).data("id"), false);
            });
        },

        RegisterCheckBoxHotTour: function (tourTable) {
            $("input.chkHotTour", tourTable).on("ifChecked", function (event) {
                TST.Admin.Tour.UpdateHottourStatus($(this).data("id"), true);
            });

            $("input.chkHotTour", tourTable).on("ifUnchecked", function (event) {
                TST.Admin.Tour.UpdateHottourStatus($(this).data("id"), false);
            });
        },

        UpdateActiveStatus: function (tourId, isActive) {
            TST.Helpers.ajax({
                controller: "Tour",
                action: "UpdateTourActiveStatus",
                data: {
                    tourId: tourId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        UpdateHottourStatus: function (tourId, isActive) {
            TST.Helpers.ajax({
                controller: "Tour",
                action: "UpdateTourHottourStatus",
                data: {
                    id: tourId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        UpdateActiveStatusTourDetail: function (id, isActive) {
            TST.Helpers.ajax({
                controller: "Tour",
                action: "UpdateTourActiveStatusTourDetail",
                data: {
                    id: id,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },
        UpdatePriceTypeActiveStatus: function (id, isActive) {
            TST.Helpers.ajax({
                controller: "Tour",
                action: "UpdatePriceActiveStatus",
                data: {
                    typeId: id,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },
        UpdateIsMainPriceTypeStatus: function (id, isMain) {
            TST.Helpers.ajax({
                controller: "Tour",
                action: "UpdateIsMainPriceStatus",
                data: {
                    typeId: id,
                    isMain: isMain
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },
        UpdateActiveStatusLocation: function (id, isActive) {
            TST.Helpers.ajax({
                controller: "Tour",
                action: "UpdateTourActiveStatusTourDetail",
                data: {
                    id: id,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },
        ReloadTourTable: function () {
            $("#tour-table").DataTable().ajax.reload();
            $("#delete-tour-btn").addClass("disabled");
            $("#price-tour-btn").addClass("disabled");
            $("#detail-tour-btn").addClass("disabled");
        },
        ReloadTourDetailTable: function () {
            $("#tour-detail-table").DataTable().ajax.reload();
            $("#remove-custom-tour-detail-btn").addClass("disabled");
            TST.Helpers.initCheckbox("#tour-detail-table");
        },
        ReloadLocationTable: function () {
            $("#location-table").DataTable().ajax.reload();
            $("#remove-custom-location-btn").addClass("disabled");
            TST.Helpers.initCheckbox("#location-table");
        }
    }

})(jQuery, window.TST = window.TST || {});