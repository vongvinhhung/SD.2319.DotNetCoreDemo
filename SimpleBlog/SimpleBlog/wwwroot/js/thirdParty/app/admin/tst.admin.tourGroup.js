﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.TourGroup = {
        RegisterTagsEvent: function (control) {
            var url = TST.Helpers.resolveUrl("Tags", "GetAllTags");
            var tags = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Name'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: {
                    url: url,
                    filter: function (tags) {
                        return $.map(tags, function (tag) {
                            return {
                                Name: tag.Name
                            };
                        });
                    },
                    cache: false
                }
            });
            tags.initialize();

            $(control).tagsinput({
                typeaheadjs: {
                    name: "tags",
                    source: tags.ttAdapter(),
                    displayKey: 'Name',
                    valueKey: 'Name'
                }
            });
        },
        RegisterEventPriceTextBox: function () {
            $(".pricetype-textBox").off("change textInput input").on("change textInput input", function () {
                $(this).data("change", "True");
            });
            $(".pricetype-textBox").off("focusout").on("focusout", function () {
                if ($(this).data("change") == "True") {
                    $(this).data("change", "False");
                    TST.Helpers.ajax({
                        controller: "TourGroup",
                        action: "UpdatePrice",
                        method: "POST",
                        data: {
                            categoryId: $(this).data("category"),
                            pricetypeId: $(this).data("pricetype"),
                            tourgroupId: $(this).data("tourgroupid"),
                            value: $(this).val()
                        },
                        callback: function () {
                            $(this).data("change", "False");
                            TST.Helpers.showAlert("Sửa giá thành công!", "success", "check");
                        }
                    });
                }
            });

            //wtf ???
            //$("#priceTable .main-price-chk").on("click", function () {
            //    //var table = $(TST.Admin.PriceDefault.TableId);
            //    //var selectedItem = table.DataTable().rows({ selected: true }).data()[0];
            //    TST.Admin.TourGroup.UpdateMainStatus($(this).data("id"), $(this).is("checked"));

            //});
            //$("input", "#priceTable .main-price-chk").on("ifChecked", function (event) {
            //    debugger;
            //    var checked = $(this).parent('[class*="icheckbox"]').hasClass("checked");
            //    TST.Admin.TourGroup.UpdateMainStatus($(this).data("id"), checked);
            //});
            //$(document).on("ifChanged", "#priceTable .main-price-chk", function (event) {
            //    TST.Admin.TourGroup.UpdateMainStatus($(this).data("id"), event.target.checked);
            //});

            TST.Admin.TourGroup.registerCheckboxEventForPriceDiv();
        },

        registerCheckboxEventForPriceDiv: function () {
            
            TST.Helpers.initCheckbox("#price-div");
            $(".main-price-chk", "#price-div").on("ifChecked", function () {
                TST.Admin.TourGroup.UpdateMainStatus($(this).data("id"), true);
            });
            $(".main-price-chk", "#price-div").on("ifUnchecked", function () {
                TST.Admin.TourGroup.UpdateMainStatus($(this).data("id"), false);
            });

            $(".is-active-chk", "#price-div").on("ifChecked", function () {
                TST.Admin.TourGroup.UpdateActiveStatusPriceTourGroup($(this).data("id"), true);
            });
            $(".is-active-chk", "#price-div").on("ifUnchecked", function () {
                TST.Admin.TourGroup.UpdateActiveStatusPriceTourGroup($(this).data("id"), false);
            });
        },

        RegisterEvent: function () {
            $('.datepicker').removeAttr("data-val-date");

            var priceShowonTour = $("#PriceShowonTour");
            if (priceShowonTour)
            {
                $("#PriceShowonTour").on("change", function () {
                    if ($(this).val() != undefined && $(this).val() != null && $(this).val().indexOf("_") !== -1)
                    {
                        var ids = $(this).val().split("_");

                        TST.Helpers.ajax({
                            controller: "TourGroup",
                            action: "ChosenShowPrice",
                            method: "POST",
                            data: {
                                categoryId: ids[0],
                                pricetypeId: ids[1],
                                tourgroupId: $("#TourGroup_Id").val()
                            },
                            callback: function () {
                                TST.Helpers.showAlert("Chọn thành công!", "success", "check");
                            }
                        });
                    }
                }).change();
            }
            
            //$("#PriceShowonTour").change();
            if ($("#TourGroup_Tags"))
            {
                TST.Admin.TourGroup.RegisterTagsEvent($("#TourGroup_Tags"));
            }

            $("#new-tourgroup-btn").on("click", function () {
                TST.Helpers.ajaxLoadNewContent({
                    controller: "TourGroup",
                    action: "TourGroupDetail",
                    callback: function () {
                        $("textarea", "#add-tourgroup-form").each(function () {
                            CKEDITOR.replace(this);
                        });
                        //TST.Admin.TourGroup.TourGroupDetail.registerEvent();

                    }
                });

            });

            $('#back-tour-btn').on("click", function () {
                TST.Helpers.ajaxLoadNewContent({
                    controller: "TourGroup",
                    action: "index",
                    callback: function () {

                    }
                });

            });

            $("#delete-tour-btn").on("click", function () {
                var TourGroupTable = $("#TourGroup-table");
                var selectedItem = TourGroupTable.DataTable().rows({ selected: true }).data()[0];

                TST.Helpers.confirmDialog({
                    title: "Xóa hình",
                    message: "Bạn có muốn xóa Tour",
                    callback: function () {
                        TST.Helpers.ajax({
                            controller: "TourGroup",
                            action: "DeleteTourGroup",
                            data: { tourGroupId: selectedItem.Id },
                            method: "POST",
                            callback: function () {
                                TST.Admin.TourGroup.ReloadTourGroupTable();
                                TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                TST.Helpers.closePopup();
                            }
                        });
                    }
                });
            });

            var addBtn = $("#add-tour-detail-btn");
            if (addBtn) {
                addBtn.on("click", function () {
                    var form = $("#add-tourgroup-form");
                    if (form.isValid()) {
                        for (instance in CKEDITOR.instances) {
                            CKEDITOR.instances[instance].updateElement();
                        }
                        var formData = new FormData(form[0]);
                        formData.Tags = $("#TourGroup_Tags").tagsinput('items').toString();

                        var parts = formData.get('TourGroup.LatestBookingDate').split('/');
                        var latestDate = new Date(parts[2], parts[1] - 1, parts[0]).toUTCString();
                        formData.set('TourGroup.LatestBookingDate', latestDate);

                        parts = formData.get('TourGroup.StartDate').split('/');
                        var startDate = new Date(parts[2], parts[1] - 1, parts[0]).toUTCString();
                        formData.set('TourGroup.StartDate', startDate);

                        TST.Helpers.ajax({
                            controller: "TourGroup",
                            action: "AddTourGroup",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Admin.TourGroup.ReloadTourGroupTable();
                                TST.Helpers.showAlert("Lưu thành công!", "success", "check");
                                TST.Helpers.ajaxLoadNewContent({
                                    controller: "TourGroup",
                                    action: "index",
                                    callback: function () {

                                    }
                                });
                            }
                        });
                    }
                });
            }

            var updateBtn = $("#save-tour-detail-btn");
            if (updateBtn) {
                updateBtn.on("click", function () {
                    var form = $("#add-tourgroup-form");
                    if (form.isValid()) {
                        for (instance in CKEDITOR.instances) {
                            CKEDITOR.instances[instance].updateElement();
                        }
                        var formData = new FormData(form[0]);
                        formData.Tags = $("#TourGroup_Tags").tagsinput('items').toString();
                        var parts = formData.get('TourGroup.LatestBookingDate').split('/');
                        var latestDate = new Date(parts[2], parts[1] - 1, parts[0]).toUTCString();
                        formData.set('TourGroup.LatestBookingDate', latestDate);

                        parts = formData.get('TourGroup.StartDate').split('/');
                        var startDate = new Date(parts[2], parts[1] - 1, parts[0]).toUTCString();
                        formData.set('TourGroup.StartDate', startDate);

                        TST.Helpers.ajax({
                            controller: "TourGroup",
                            action: "EditTourGroup",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Admin.TourGroup.ReloadTourGroupTable();
                                TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                                TST.Helpers.closePopup();
                                TST.Helpers.ajaxLoadNewContent({
                                    controller: "TourGroup",
                                    action: "index",
                                    callback: function () {

                                    }
                                });
                            }
                        });
                    }
                });
            }

            var addTourGroupDetailBtn = $("#add-custom-tour-detail-btn");
            if (addTourGroupDetailBtn) {
                addTourGroupDetailBtn.on("click", function () {

                    TST.Helpers.ajaxOpenPopup({
                        controller: "TourGroup",
                        width: 800,
                        action: "AddTourGroupDetailPopup",
                        callback: function (data) {
                            $("#RelateId").val($("#TourGroup_Id").val());
                            TST.Admin.TourGroup.AddTourGroupDetailPopup.registerEvent();
                        }
                    });
                });
            }

            $("#add-custom-group-location-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "TourGroup",
                    action: "GroupLocationDetailPopup",
                    data: {
                        id: 0,
                        tourGroupId: $("#TourGroup_Id").val()
                    },
                    callback: function (data) {
                        //$("#RelateId").val($("#Tour_Id").val());
                        TST.Admin.TourGroup.AddLocationPopup.registerEvent();
                    }
                });
            });

            $("#add-tour-image").on("change", function () {
                var previewBox = $("#add-tour-preview .box.box-primary").empty();
                var preview = $("#add-tour-preview");
                preview.show();
                if (this.files && this.files.length > 0) {
                    $.each(this.files, readAndPreview);
                } else {
                    preview.hide();
                }
                function readAndPreview(i, file) {
                    if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                        preview.hide();
                    }
                    var reader = new FileReader();
                    $(reader).on("load", function () {
                        previewBox.append($("<img/>", { src: this.result, height: 100 }));
                    });
                    reader.readAsDataURL(file);
                }
            });

            TST.Admin.TourGroup.RegisterEventPriceTextBox();


            $("#add-tour-pricetype-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "TourGroup",
                    action: "DetailPriceType",
                    callback: function (data) {
                        var addPriceTypeBtn = $("#save-price-type-btn");
                        if (addPriceTypeBtn) {
                            addPriceTypeBtn.on("click", function (e) {
                                e.preventDefault();
                                TST.Helpers.ajax({
                                    controller: "TourGroup",
                                    action: "AddPriceType",
                                    data: {
                                        nameType: $("#add-price-type-form #Description").val(),
                                        TourGroupId: $("#TourGroup_Id").val()
                                    },
                                    method: "POST",
                                    callback: function (data) {
                                        TST.Helpers.ajax({
                                            controller: "TourGroup",
                                            action: "LoadGridPrice",
                                            data: { TourGroupId: $("#TourGroup_Id").val() },
                                            callback: function (data) {
                                                //$("#price-div").html(data);
                                                $("#price-div").html("");
                                                $("#price-div").append(data);
                                                TST.Admin.TourGroup.RegisterEventPriceTextBox();
                                            }
                                        });
                                        //TST.Helpers.initCheckbox("#price-div"); //goi init lai no mat may cai event cu~
                                        //TST.Admin.Tour.ReloadTourDetailTable();

                                        TST.Admin.TourGroup.registerCheckboxEventForPriceDiv();
                                        TST.Helpers.showAlert("Thêm loại giá thành công !", "success", "check");
                                        TST.Helpers.closePopup();

                                    }
                                });
                            });
                        }
                    }
                });
            });

            $(document).on("click", ".delete-pricetype-btn", function () {
                //var table = $(TST.Admin.PriceDefault.TableId);
                //var selectedItem = table.DataTable().rows({ selected: true }).data()[0];

                var priceTypeId = $(this).data("index");

                TST.Helpers.confirmDialog({
                    title: "Xóa loại giá",
                    message: "Xác nhận xóa?",
                    callback: function () {
                        TST.Helpers.ajax({
                            url: TST.Helpers.resolveUrl("TourGroup", "DeletePriceType"),
                            data: { priceTypeId: priceTypeId },
                            method: "POST",
                            callback: function () {
                                //TST.Admin.PriceDefault.ReloadTable();
                                TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                TST.Helpers.closePopup();
                                TST.Helpers.ajax({
                                    controller: "TourGroup",
                                    action: "LoadGridPrice",
                                    data: { tourgroupId: $("#TourGroup_Id").val() },
                                    callback: function (data) {
                                        $("#price-div").html("");
                                        $("#price-div").append(data);
                                        TST.Admin.TourGroup.RegisterEventPriceTextBox();
                                        TST.Admin.TourGroup.registerCheckboxEventForPriceDiv();
                                    }
                                });
                            }
                        });
                    }
                });
            });

            $('#TourGroup_TourId').off("change").on("change", function () {
                TST.Helpers.ajax({
                    controller: "TourGroup",
                    action: "PopulateTourData",
                    data: {
                        id: $(this).val()
                    },
                    callback: function (data) {
                        var a = data;
                        $("#TourGroup_AreaId").val(data.TourGroup.AreaId);
                        CKEDITOR.instances.TourGroup_Content.setData(data.TourGroup.Content, function () {
                            this.checkDirty();  // true
                        });
                        CKEDITOR.instances.TourGroup_Description.setData(data.TourGroup.Description, function () {
                            this.checkDirty();  // true
                        });
                        //$("#TourGroup_Content").val(data.TourGroup.Content);
                        //$("#TourGroup_Description").val(data.TourGroup.Description);
                        $("#TourGroup_Image").val(data.TourGroup.Image);
                        //var cbHtoTour = $("#TourGroup_IsHotTour").attr("checked", data.TourGroup.IsHotTour);
                        var cbHtoTour = $("#TourGroup_IsHotTour");
                        if (data.TourGroup.IsHotTour) cbHtoTour.iCheck("check");
                        $("#TourGroup_IsActive").val(data.TourGroup.IsActive);
                        $("#TourGroup_IsDeleted").val(data.TourGroup.IsDeleted);
                        $("#TourGroup_IsHotTour").val(data.TourGroup.IsHotTour);
                        $("#TourGroup_StartLocation").val(data.TourGroup.StartLocation);
                        $("#TourGroup_Title").val(data.TourGroup.Title);
                        $("#TourGroup_TotalDay").val(data.TourGroup.TotalDay);
                        $("#TourGroup_TotalNight").val(data.TourGroup.TotalNight);
                        $("#TourGroup_TourName").val(data.TourGroup.TourName);
                        $("#TourGroup_Title").val(data.TourGroup.Title);
                        $("#tourGroupImage").attr("src", data.ImageUrl);
                        //debugger;
                        //$("#RelateId").val($("#Tour_Id").val());
                        //TST.Admin.Tour.AddTourDetailPopup.registerEvent();
                    }
                });
            });
        },

        AddTourGroupDetailPopup: {
            registerEvent: function () {

                $("textarea", "#add-tour-detail-popup").each(function () {
                    CKEDITOR.replace(this);
                });

                var savePopupBtn = $("#save-tour-detail-popup-btn");
                if (savePopupBtn) {
                    savePopupBtn.on("click", function () {
                        var form = $("#add-tour-detail-popup-form");
                        if (form.isValid()) {
                            for (instance in CKEDITOR.instances) {
                                CKEDITOR.instances[instance].updateElement();
                            }
                            var formData = new FormData(form[0]);
                            TST.Helpers.ajax({
                                controller: "TourGroup",
                                action: "AddTourGroupDetailPopup",
                                processData: false,
                                data: form.serialize(),
                                method: "POST",
                                callback: function (data) {
                                    //TST.Admin.Tour.ReloadTourTable();
                                    TST.Admin.TourGroup.ReloadTourGroupDetailTable();
                                    TST.Helpers.showAlert("Thêm chi tiết thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

                var savePopupBtn = $("#update-tour-detail-popup-btn");
                if (savePopupBtn) {
                    savePopupBtn.on("click", function () {
                        var form = $("#add-tour-detail-popup-form");
                        if (form.isValid()) {
                            for (instance in CKEDITOR.instances) {
                                CKEDITOR.instances[instance].updateElement();
                            }
                            var formData = new FormData(form[0]);
                            TST.Helpers.ajax({
                                controller: "TourGroup",
                                action: "UpdateTourGroupDetailPopup",
                                processData: false,
                                data: form.serialize(),
                                method: "POST",
                                callback: function (data) {
                                    //TST.Admin.Tour.ReloadTourTable();
                                    TST.Admin.TourGroup.ReloadTourGroupDetailTable();
                                    TST.Helpers.showAlert("Cập nhật chi tiết thành công!", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

            }
        },

        AddLocationPopup: {
            registerEvent: function () {
                var savePopupBtn = $("#save-tour-group-location-btn");
                if (savePopupBtn) {
                    savePopupBtn.on("click", function () {
                        $("#TourGroupLocations_TourGroupId").val($("#TourGroup_Id").val());
                        var form = $("#add-tour-group-location-form");
                        if (form.isValid()) {
                            TST.Helpers.ajax({
                                controller: "TourGroup",
                                action: "SaveTourGroupLocation",
                                processData: false,
                                data: form.serialize(),
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.TourGroup.ReloadLocationTable();
                                    TST.Helpers.showAlert("Cập nhật chi tiết thành công!", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

                $("#TourGroupLocations_NationId").on("change", function () {
                    TST.Helpers.ajax({
                        controller: "Tour",
                        action: "GetCitiesByNationId",
                        data: { nationId: $("#TourGroupLocations_NationId").val() },
                        cache: false,
                        method: "POST",
                        callback: function (data) {
                            var markup = "";
                            for (var x = 0; x < data.length; x++) {
                                markup += "<option value=" + data[x].Value + ">" + data[x].Text + "</option>";
                            }
                            $("#TourGroupLocations_CityId").html(markup).show();
                        },
                    });
                });
            }
        },

        InitLocationTable: function () {
            var locationTable = $("#location-table");
            var deleteButton = $("#remove-custom-location-btn");

            locationTable.DataTable({
                dom: "t",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("TourGroup", "GetLocations"),
                    "type": 'POST',
                    "data": {
                        tourGroupId: $("#TourGroup_Id").val()
                    }
                },

                columns: [
                    {
                        title: "Số thứ tự",
                        data: "STT"
                    },
                    {
                        title: "Quốc gia",
                        data: "Nation"
                    },
                    {
                        title: "Thành phố",
                        data: "City"
                    },
                    {
                        title: "Xoá",
                        render: function (data, type, full, meta) {
                            return TST.Templates.DeleteButton(full.Id);
                        }
                    }
                ],
                serverSide: true,
                "initComplete": function () {

                    locationTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    locationTable.on("dblclick", "tr", function () {
                        var selectedItem = locationTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "TourGroup",
                                //action: "UpdateTourDetailPopup",
                                action: "GroupLocationDetailPopup",
                                data: { id: selectedItem.Id, tourGroupId: $("#TourGroup_Id").val() },
                                callback: function () {
                                    TST.Admin.TourGroup.AddLocationPopup.registerEvent();
                                }
                            });
                        }
                    });

                    $(document).on("click", "#location-table .delete-btn", function (e) {
                        e.preventDefault();
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa chi tiết Đoàn",
                            message: "Bạn có muốn xóa địa điểm này?",
                            dialogType: "danger",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "TourGroup",
                                    action: "DeleteTourGroupLocation",
                                    data: { tourGroupLocationId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.TourGroup.ReloadLocationTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                }
            });
        },

        PriceTourGroupPopup: {
            registerEvent: function () {

                var saveBtn = $("#save-price-btn");
                if (saveBtn) {
                    saveBtn.on("click", function () {
                        var form = $("#add-price-default-form");
                        if (form.isValid()) {
                            var formData = new FormData(form[0]);
                            TST.Helpers.ajax({
                                controller: "TourGroup",
                                action: "SubmitPricePopup",
                                processData: false,
                                data: form.serialize(),
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.TourGroup.ReloadTourGroupDetailTable();
                                    TST.Helpers.showAlert("Sửa giá thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }


            }
        },

        InitTourGroupTable: function () {
            var TourGroupTable = $("#tourgroup-table");
            var deleteButton = $("#delete-tour-btn");
            var priceButton = $("#price-tour-btn");
            var detailButton = $("#detail-tour-btn");
            var fileUrl = $("#file-url").val();

            TourGroupTable.DataTable({
                order: [[0, "desc"]],
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("TourGroup", "GetTourGroupsData")
                },
                //"order": [],
                columns: [
                    {
                        title: "Id",
                        data: "Id",
                        visible: false
                    },
                    {
                        title: "Mã tour",
                        data: "TourGroupCode"
                    },
                    {
                        title: "Tên tour",
                        data: "TourName",
                        render: function (data, type, full, meta) {
                            return `<a href="` +  `/tour-du-lich/${full.Id}" target="blank">${full.TourName}</a>`;
                        }
                    },
                    {
                        title: "Hình ảnh",
                        data: "Image",
                        render: function (data, type, full, meta) {
                            return `<img src="${fileUrl}/?image=${data}" height="75" style="display: block; margin: 0 auto;">`;
                        }
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" class="chkIsActive" data-id=${full.Id} value=${data} ${checked} >`;
                        }
                    },
                    {
                        title: "Hot tour",
                        data: "IsHotTour",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" class="chkHotTour" data-id=${full.Id} value=${data} ${checked} >`;
                        }
                    },
                    {
                        title: "Xoá",
                        render: function (data, type, full, meta) {
                            return TST.Templates.DeleteButton(full.Id);

                            //return `<input type="button" class="btn btn-danger delete-tour-grid-btn" data-id=${full.Id} value="Xoá" aria-invalid="false">`;
                        }
                    }
                ],
                serverSide: true,
                "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {

                    TourGroupTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                            priceButton.enable();
                            detailButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                            priceButton.disable();
                            detailButton.enable();
                        });

                    TourGroupTable.on("dblclick", "tr", function () {
                        var selectedItem = TourGroupTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxLoadNewContent({
                                controller: "TourGroup",
                                action: "TourGroupDetail",
                                data: { tourgroupId: selectedItem.Id },
                                callback: function () {
                                    $("textarea", "#add-tourgroup-form").each(function () {
                                        CKEDITOR.replace(this);
                                    });
                                    //TST.Admin.TourGroup.TourGroupDetail.registerEvent();

                                }
                            });
                        }
                    });

                    $(document).on("click", "#tourgroup-table .delete-btn", function (e) {
                        e.preventDefault();
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa đoàn",
                            message: "Bạn có muốn xóa đoàn?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "TourGroup",
                                    action: "DeleteTourGroup",
                                    data: { tourGroupId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.TourGroup.ReloadTourGroupTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });

                    TST.Admin.TourGroup.PriceTourGroupPopup.registerEvent();

                    //event for show/hide
                    var thisDataTable = $(this);
                    TST.Helpers.initCheckbox(thisDataTable);
                    TST.Admin.TourGroup.RegisterCheckBoxActive(thisDataTable);
                    TST.Admin.TourGroup.RegisterCheckBoxIsHotTour(thisDataTable);

                    TourGroupTable.on("draw.dt", function () {
                        TST.Helpers.initCheckbox(thisDataTable);
                        TST.Admin.TourGroup.RegisterCheckBoxActive(thisDataTable);
                        TST.Admin.TourGroup.RegisterCheckBoxIsHotTour(thisDataTable);
                    });
                }
            });
        },

        InitTourGroupDetailTable: function () {
            var TourGroupTable = $("#tourgroup-detail-table");
            var deleteButton = $("#remove-custom-tour-detail-btn");
            //var priceButton = $("#price-tour-btn");
            //var detailButton = $("#detail-tour-btn");
            var fileUrl = $("#file-url").val();

            TourGroupTable.DataTable({
                dom: "t",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("TourGroup", "GetTourGroupDetailsData"),
                    "type": 'POST',
                    "data": {
                        "tourgroupId":
                        $("#TourGroup_Id").val()
                    }
                },

                columns: [

                    {
                        title: "Tên chi tiết tour",
                        data: "Title"
                    },
                    {
                        title: "Mô tả",
                        data: "Description"
                    },
                    {
                        title: "Nội dung",
                        data: "Content"
                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "Xoá",
                        render: function (data, type, full, meta) {
                            return TST.Templates.DeleteButton(full.Id)
                            //return `<input type="button" class="btn btn-danger delete-tour-detail-grid-btn" data-id=${full.Id} value="Xoá" aria-invalid="false">`;
                        }
                    }
                ],
                serverSide: true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {

                    TST.Helpers.initCheckbox("#tour-detail-table");
                    TourGroupTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                            //priceButton.enable();
                            //detailButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                            //priceButton.disable();
                            //detailButton.enable();
                        });

                    TourGroupTable.on("dblclick", "tr", function () {
                        var selectedItem = TourGroupTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                width: 800,
                                controller: "TourGroup",
                                action: "UpdateTourGroupDetailPopup",
                                data: { tourgroupDetailId: selectedItem.Id },
                                callback: function () {
                                    //$("textarea", "#add-tour-detail-popup").each(function () {
                                    //    CKEDITOR.replace(this);
                                    //});
                                    TST.Admin.TourGroup.AddTourGroupDetailPopup.registerEvent();

                                }
                            });
                        }
                    });

                    $(document).on("click", '#tourgroup-detail-table .delete-btn', function (e) {
                        e.preventDefault();
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa chi tiết đoàn",
                            message: "Bạn có muốn xóa chi tiết đoàn",
                            dialogType: "danger",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "TourGroup",
                                    action: "DeleteTourGroupDetail",
                                    data: { TourGroupDetailId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.TourGroup.ReloadTourGroupDetailTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });

                    TST.Admin.TourGroup.PriceTourGroupPopup.registerEvent();

                    //event for show/hide
                    TST.Helpers.initCheckbox(TourGroupTable);

                    $("input", TourGroupTable).on("ifChecked", function (event) {
                        TST.Admin.TourGroup.UpdateActiveStatusTourDetail($(this).data("id"), true);
                    });

                    $("input", TourGroupTable).on("ifUnchecked", function (event) {
                        TST.Admin.TourGroup.UpdateActiveStatusTourDetail($(this).data("id"), false);
                    });

                    TourGroupTable.on("draw.dt", function () {
                        TST.Helpers.initCheckbox(TourGroupTable);

                        $("input", TourGroupTable).on("ifChecked", function (event) {
                            TST.Admin.TourGroup.UpdateActiveStatusTourDetail($(this).data("id"), true);
                        });

                        $("input", TourGroupTable).on("ifUnchecked", function (event) {
                            TST.Admin.TourGroup.UpdateActiveStatusTourDetail($(this).data("id"), false);
                        });
                    });
                }
            });
        },

        RegisterCheckBoxActive: function (tourTable) {
            $("input.chkIsActive", tourTable).on("ifChecked", function (event) {
                TST.Admin.TourGroup.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input.chkIsActive", tourTable).on("ifUnchecked", function (event) {
                TST.Admin.TourGroup.UpdateActiveStatus($(this).data("id"), false);
            });
        },

        UpdateActiveStatus: function (tourId, isActive) {
            TST.Helpers.ajax({
                controller: "TourGroup",
                action: "UpdateTourGroupActiveStatus",
                data: {
                    TourGroupId: tourId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        RegisterCheckBoxIsHotTour: function (tourTable) {
            $("input.chkHotTour", tourTable).on("ifChecked", function (event) {
                TST.Admin.TourGroup.UpdateHotTourStatus($(this).data("id"), true);
            });

            $("input.chkHotTour", tourTable).on("ifUnchecked", function (event) {
                TST.Admin.TourGroup.UpdateHotTourStatus($(this).data("id"), false);
            });
        },

        UpdateHotTourStatus: function (tourGroupId, isHotTour) {
            TST.Helpers.ajax({
                controller: "TourGroup",
                action: "UpdateTourGroupHottourStatus",
                data: {
                    id: tourGroupId,
                    isHotTour: isHotTour
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        UpdateMainStatus: function (priceDefaultId, isMain) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("TourGroup", "UpdateMainStatus"),
                data: {
                    priceDefaultId: priceDefaultId,
                    isMain: isMain
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        UpdateActiveStatusPriceTourGroup: function (priceDefaultId, isActive) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("TourGroup", "UpdatePriceIsActiveStatus"),
                data: {
                    priceDefaultId: priceDefaultId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        UpdateActiveStatusTourDetail: function (id, isActive) {
            TST.Helpers.ajax({
                controller: "TourGroup",
                action: "UpdateTourGroupdetailActiveStatus",
                data: {
                    tourgroupId: id,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },
        ReloadTourGroupTable: function () {
            $("#tourgroup-table").DataTable().ajax.reload();
            $("#delete-tour-btn").addClass("disabled");
            $("#price-tour-btn").addClass("disabled");
            $("#detail-tour-btn").addClass("disabled");
        },
        ReloadTourGroupDetailTable: function () {
            $("#tourgroup-detail-table").DataTable().ajax.reload();
            $("#remove-custom-tour-detail-btn").addClass("disabled");
        },
        ReloadLocationTable: function () {
            $("#location-table").DataTable().ajax.reload();
            $("#remove-custom-location-btn").addClass("disabled");
            TST.Helpers.initCheckbox("#location-table");
        }
    }

})(jQuery, window.TST = window.TST || {});