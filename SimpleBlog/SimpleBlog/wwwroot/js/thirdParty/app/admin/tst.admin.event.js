﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.Event = {
        TableId: "#event-table",
        RegisterEvent: function () {
            $("#new-event-btn").on("click", function () {
                TST.Helpers.ajaxOpenLargePopup({
                    url: TST.Helpers.resolveUrl("Event", "Detail"),
                    width: 500,
                    callback: function () {
                        CKEDITOR.replace('eventDescription');
                        TST.Admin.Event.AddPopup.registerEvent();
                    }
                });
            });
        },
        AddPopup: {
            registerEvent: function () {
                $("#save-event-btn").on("click", function () {
                    var form = $("#add-event-form");
                    if (form.isValid()) {
                        for (instance in CKEDITOR.instances) {
                            CKEDITOR.instances[instance].updateElement();
                        }
                        var formData = new FormData(form[0]);
                        TST.Helpers.ajax({
                            controller: "Event",
                            action: "Save",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                                TST.Admin.Event.ReloadTable();
                                TST.Helpers.closePopup();
                            }
                        });
                    }
                });
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            var fileUrl = $("#file-url").val();
            var deleteButton = $("#delete-event-btn");
            table.DataTable({
                order: [[3, "desc"]],
                ajax: {
                    url: TST.Helpers.resolveUrl("Event", "GetData")
                },
                columns: [
					{
					    title: "Tiêu đề",
					    data: "Title"
					},
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked} data-type="isShow">`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return TST.Templates.DeleteButton(full.Id)
                        }
                    },
                    {
                        title: "Id",
                        data: "Id",
                        visible: false
                    }
                ],
                //serverSide: true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenLargePopup({
                                controller: "Event",
                                action: "Detail",
                                data: { eventId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    CKEDITOR.replace('eventDescription');
                                    TST.Admin.Event.AddPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide and isAbroad
                    TST.Admin.Event.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa khuyến mãi",
                            message: "Xác nhận xóa?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    url: TST.Helpers.resolveUrl("Event", "Delete"),
                                    data: { eventId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.Event.ReloadTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    TST.Admin.Event.RegisterFilterDatatable(table);
                    table.on("draw.dt", function () {
                        TST.Admin.Event.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa giới thiệu",
                                message: "Xác nhận xóa?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        url: TST.Helpers.resolveUrl("Event", "Delete"),
                                        data: { eventId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.Event.ReloadTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox(this.TableId);

            $("input[data-type='isShow']", this.TableId).on("ifChecked", function (event) {
                TST.Admin.Event.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input[data-type='isShow']", this.TableId).on("ifUnchecked", function (event) {
                TST.Admin.Event.UpdateActiveStatus($(this).data("id"), false);
            });
        },
        RegisterFilterDatatable: function (table) {
            $('#event-by-Title').on('keyup', function () {
                table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
            });
        },
        UpdateActiveStatus: function (eventId, isActive) {
            TST.Helpers.ajax({
                url: TST.Helpers.resolveUrl("Event", "UpdateActiveStatus"),
                data: {
                    eventId: eventId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                }
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
            $("delete-event-btn").disable();
        }
    }

})(jQuery, window.TST = window.TST || {});