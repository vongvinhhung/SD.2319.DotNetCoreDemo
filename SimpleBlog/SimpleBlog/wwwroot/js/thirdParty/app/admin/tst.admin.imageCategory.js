﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.ImageCategory = {
        RegisterEvent: function () {

            $("#new-image-category-btn").on("click", function () {
                TST.Helpers.ajaxOpenPopup({
                    controller: "ImageCategory",
                    action: "AddImageCategoryPopup",
                    width: 500,
                    callback: function () {
                        TST.Admin.ImageCategory.AddImageCategoryPopup.registerEvent();
                    }
                });
            });
        },

        AddImageCategoryPopup: {
            registerEvent: function () {
                var saveBtn = $("#save-image-category-btn");
                if (saveBtn) {
                    saveBtn.on("click", function () {
                        var form = $("#add-image-category-form");
                        if (form.isValid()) {
                            //var formData = TST.Helpers.formToObject(form);
                            var formData = new FormData(form[0]);

                            TST.Helpers.ajax({
                                controller: "ImageCategory",
                                action: "AddImageCategory",
                                data: formData,
                                processData: false,
                                contentType: false,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.ImageCategory.ReloadImageCategoryTable();
                                    TST.Helpers.showAlert("Thêm thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }

                var editBtn = $("#edit-image-category-btn");
                if (editBtn) {
                    editBtn.on("click", function () {
                        var form = $("#edit-image-category-form");
                        if (form.isValid()) {
                            //var formData = TST.Helpers.formToObject(form);
                            var formData = new FormData(form[0]);

                            TST.Helpers.ajax({
                                controller: "ImageCategory",
                                action: "EditImageCategory",
                                data: formData,
                                processData: false,
                                contentType: false,
                                method: "POST",
                                callback: function (data) {
                                    TST.Admin.ImageCategory.ReloadImageCategoryTable();
                                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                                    TST.Helpers.closePopup();
                                }
                            });
                        }
                    });
                }
            }
        },


        InitImageCategoryTable: function () {
            var imageCategoryTable = $("#image-category-table");
            var deleteButton = $("#delete-image-category-btn");

            imageCategoryTable.DataTable({
                //dom: "lrtip",
                //select: {
                //    style: "single",
                //    info: false
                //},
                ajax: {
                    url: TST.Helpers.resolveUrl("ImageCategory", "GetImageCategorysData")
                },
                columns: [
                    {
                        title: "Mô tả",
                        data: "Description"

                    },
                    {
                        title: "Hiển thị",
                        data: "IsActive",
                        render: function (data, type, full, meta) {
                            var checked = data ? "checked" : "";
                            return `<input type="checkbox" data-id=${full.Id} value=${data} ${checked}>`;
                        }
                    },
                    {
                        title: "Xóa",
                        data: "Id",
                        render: function (data, type, full, meta) {
                            return `<input type="button" class="btn btn-danger delete-btn" data-index=${data} value="Xoá" />`;
                        }
                    }
                ],
                //serverSide: true,
                //"lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    imageCategoryTable.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    imageCategoryTable.on("dblclick", "tr", function () {
                        var selectedItem = imageCategoryTable.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "ImageCategory",
                                action: "EditImageCategoryPopup",
                                data: { lookupId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.ImageCategory.AddImageCategoryPopup.registerEvent();
                                }
                            });
                        }
                    });

                    TST.Admin.ImageCategory.RegisterFilterEvent();

                    //event for show/hide
                    TST.Admin.ImageCategory.RegisterCheckBoxActive();
                    $(".delete-btn").on("click", function () {
                        var id = $(this).data("index");
                        TST.Helpers.confirmDialog({
                            title: "Xóa",
                            message: "Bạn có muốn xóa danh mục này?",
                            callback: function () {
                                TST.Helpers.ajax({
                                    controller: "ImageCategory",
                                    action: "DeleteImageCategory",
                                    data: { lookupId: id },
                                    method: "POST",
                                    callback: function () {
                                        TST.Admin.ImageCategory.ReloadImageCategoryTable();
                                        TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                        TST.Helpers.closePopup();
                                    }
                                });
                            }
                        });
                    });
                    imageCategoryTable.on("draw.dt", function () {
                        TST.Admin.ImageCategory.RegisterCheckBoxActive();
                        $(".delete-btn").on("click", function () {
                            var id = $(this).data("index");
                            TST.Helpers.confirmDialog({
                                title: "Xóa",
                                message: "Bạn có muốn xóa danh mục này?",
                                callback: function () {
                                    TST.Helpers.ajax({
                                        controller: "ImageCategory",
                                        action: "DeleteImageCategory",
                                        data: { lookupId: id },
                                        method: "POST",
                                        callback: function () {
                                            TST.Admin.ImageCategory.ReloadImageCategoryTable();
                                            TST.Helpers.showAlert("Xóa thành công !", "success", "check");
                                            TST.Helpers.closePopup();
                                        }
                                    });
                                }
                            });
                        });
                    });
                },

                "drawCallback": function (settings) {
                    
                }
            });
        },

        RegisterCheckBoxActive: function () {
            TST.Helpers.initCheckbox("#image-category-table");

            $("input", "#image-category-table").on("ifChecked", function (event) {
                TST.Admin.ImageCategory.UpdateActiveStatus($(this).data("id"), true);
            });

            $("input", "#image-category-table").on("ifUnchecked", function (event) {
                TST.Admin.ImageCategory.UpdateActiveStatus($(this).data("id"), false);
            });
        },

        RegisterFilterEvent: function () {
            var imageCategoryTable = $("#image-category-table");
            var timer; // for delay keyup
            function delayKeyUp(callBack) {
                clearTimeout(timer);
                timer = setTimeout(function () { callBack() }, 500);
            }

            $("#filter-by-description", "#image-category-filter-group").on("keyup", function () {
                delayKeyUp(function () {
                    var descriptionColumn = imageCategoryTable.DataTable().columns(0);
                    var searchValue = $("#filter-by-description", "#image-category-filter-group").val();
                    if (descriptionColumn.search() !== searchValue) {
                        descriptionColumn.search(searchValue).draw();
                    }
                });
            });
        },

        UpdateActiveStatus: function (lookupId, isActive) {
            TST.Helpers.ajax({
                controller: "ImageCategory",
                action: "UpdateImageCategoryActiveStatus",
                data: {
                    lookupId: lookupId,
                    isActive: isActive
                },
                method: "POST",
                callback: function (data) {
                    TST.Helpers.showAlert("Sửa thành công !", "success", "check");
                }
            });
        },

        ReloadImageCategoryTable: function () {
            $("#image-category-table").DataTable().ajax.reload();
            $("#delete-image-category-btn").addClass("disabled");
        }
    }

})(jQuery, window.TST = window.TST || {});