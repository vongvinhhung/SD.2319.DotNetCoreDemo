﻿(function ($, TST) {
    TST.Admin = TST.Admin || {}

    TST.Admin.Parameter = {
        TableId: "#parameter-table",
        RegisterEvent: function () {

        },
        AddPopup: {
            registerEvent: function () {
                $("#save-parameter-btn").on("click", function () {
                    var form = $("#add-parameter-form");
                    if (form.isValid()) {
                        var formData = new FormData(form[0]);
                        TST.Helpers.closePopup();
                        TST.Helpers.ajax({
                            controller: "Parameter",
                            action: "Save",
                            processData: false,
                            contentType: false,
                            data: formData,
                            method: "POST",
                            callback: function (data) {
                                TST.Helpers.showAlert("Cập nhật thành công !", "success", "check");
                                TST.Admin.Parameter.ReloadTable();
                            }
                        });
                    }
                });
            }
        },
        InitTable: function () {
            var table = $(this.TableId);
            var deleteButton = $("#delete-parameter-btn");
            table.DataTable({
                dom: "lrtip",
                select: {
                    style: "single",
                    info: false
                },
                ajax: {
                    url: TST.Helpers.resolveUrl("Parameter", "GetData")
                },
                columns: [
                    {
                        title: "Tiêu đề",
                        data: "Key"
                    },
                    {
                        title: "Giá trị",
                        data: "Value"
                    }
                ],
                serverSide: true,
                "lengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
                "initComplete": function () {
                    table.DataTable()
                        .on("select", function () {
                            deleteButton.enable();
                        })
                        .on("deselect", function () {
                            deleteButton.disable();
                        });

                    table.on("dblclick", "tr", function () {
                        var selectedItem = table.DataTable().row(this).select().data();
                        if (selectedItem) {
                            TST.Helpers.ajaxOpenPopup({
                                controller: "Parameter",
                                action: "Detail",
                                data: { parameterId: selectedItem.Id },
                                width: 500,
                                callback: function () {
                                    TST.Admin.Parameter.AddPopup.registerEvent();
                                }
                            });
                        }
                    });

                    //event for show/hide
                    TST.Admin.Parameter.RegisterFilterDatatable(table);
                }
            });
        },
        RegisterFilterDatatable: function (table) {
            $('#parameter-by-key').on('keyup', function () {
                table.DataTable().columns(0)
                    .search(this.value)
                    .draw();
            });
        },

        ReloadTable: function () {
            $(this.TableId).DataTable().ajax.reload();
        }
    }

})(jQuery, window.TST = window.TST || {});