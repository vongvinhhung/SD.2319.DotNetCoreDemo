﻿$.extend(true,
    $.fn.dataTable.defaults,
    {
        dom: "rtp",
        pageLength: 10,
        select: {
            style: "single",
            info: false
        },
        serverSide: true
    });