﻿(function ($, TST) {

    TST.Templates = {
        DeleteButton: function (id) {
            return `<div class="btn btn-danger delete-btn" data-index=${id}><i class="fa fa-trash-o"></i></div>`;
        }
    }
    
})(jQuery, window.TST = window.TST || {});