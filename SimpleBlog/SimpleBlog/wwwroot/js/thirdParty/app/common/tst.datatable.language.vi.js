﻿$.extend(true,
    $.fn.dataTable.defaults,
    {
        language: {
            infoEmpty: "",
            zeroRecords: "Không có dữ liệu",
            emptyTable: "Không có dữ liệu",
            search: "Tìm kiếm",
            lengthMenu: "Hiển thị _MENU_ dòng",
            paginate: {
                first: "Đầu",
                last: "Cuối",
                previous: "Trước",
                next: "Sau"
            }

        }
    });
(function ($) {
    $.validator.addMethod('GenericRequired', function (value, element, params) {
        //var isChecked = $(param).is(':checked');
        //if (isChecked) {
        //    return false;
        //}
        //return true;
    }, '');

    $.validator.unobtrusive.adapters.add("GenericRequired", ["GenericRequired"], function (options) {
        options.rules["GenericRequired"] = "#" + options.params.GenericRequired;
        options.messages["GenericRequired"] = options.message;
    });
})(jQuery);