﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SB.Core.Services;
using SimpleBlog.Models;
using System.Diagnostics;
using SB.Core.DTO;

namespace SimpleBlog.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICategory _category;
        private readonly IPost _post;

        public HomeController(ICategory category, IPost post)
        {
            _category = category;
            _post = post;
        }

        public IActionResult Index(int? categoryId)
        {
            List<PostDto> posts;
            if (categoryId.HasValue)
                posts = _post.GetAllByCategoryId(categoryId.Value);
            else
                posts = _post.GetAll();

            return View(posts);
        }

        public IActionResult PostDetail(int id)
        {
            return View(_post.Get(id));
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
