﻿using Microsoft.AspNetCore.Mvc;
using SB.Service.Identity;
namespace SimpleBlog.ViewComponents
{
    public class LogoutViewComponent : ViewComponent
    {
        private readonly SBSignInManager _signInManager;

        public LogoutViewComponent(SBSignInManager signInManager)
        {
            _signInManager = signInManager;
        }

        public IViewComponentResult Invoke()
        {
            var userName = _signInManager.Context.User.Identity.Name;
            return View("Default",userName);
        }
    }
}
