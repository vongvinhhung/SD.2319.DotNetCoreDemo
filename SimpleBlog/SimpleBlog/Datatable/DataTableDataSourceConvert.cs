﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleBlog.Datatable
{
    public static class DataTableDataSourceConvert
    {
        public static dynamic ConvertToDataSource<T>(this IEnumerable<T> data, DataTableParameterModel param)
        {
            var sortedColumns = param.Order;
            var orderByString = string.Empty;

            //Global filter
            if (param.Search?.Value != null)
            {
                var searchValue = param.Search.Value.Trim().ToLower();
                data = data.Where(x =>
                {
                    return param.Columns
                                .Select(col => x.GetType().GetProperty(col.Data)?.GetValue(x)?.ToString().ToLower().Contains(searchValue))
                                .Any(isMatch => isMatch.HasValue && isMatch.Value);
                }).AsEnumerable();
            }

            var columnsSearch = param.Columns.Where(x => x.Search.Value != "").ToList(); // get all column need to filter
            foreach (var col in columnsSearch)
            {
                var searchValue = col.Search.Value.Trim().ToLower();
                data = data.Where(x =>
                {
                    var isMatch = x.GetType().GetProperty(col.Data).GetValue(x)?.ToString().ToLower().Contains(searchValue);
                    return isMatch.HasValue && isMatch.Value;
                }).AsEnumerable();
            }

            var total = data.Count();

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != string.Empty ? "," : "";
                orderByString += (param.Columns.ElementAt(column.Column).Data) + (column.Dir == "asc" ? " asc" : " desc");
            }

            data = data.Skip(param.Start).Take(param.Length).ToList();

            return new
            {
                draw = param.Draw,
                recordsTotal = total,
                recordsFiltered = total,
                data = data
            };
        }
    }
}