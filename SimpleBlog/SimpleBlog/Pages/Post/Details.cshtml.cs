﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SimpleBlog.Data;
using SimpleBlog.Data.Entities;

namespace SimpleBlog.Pages.Post
{
    public class DetailsModel : PageModel
    {
        private readonly SimpleBlog.Data.ApplicationDbContext _context;

        public DetailsModel(SimpleBlog.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public SimpleBlog.Data.Entities.Post Post { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Post = await _context.Posts.SingleOrDefaultAsync(m => m.Id == id);

            if (Post == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
