﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SimpleBlog.Data;
using SimpleBlog.Data.Entities;

namespace SimpleBlog.Pages.Post
{
    public class IndexModel : PageModel
    {
        private readonly SimpleBlog.Data.ApplicationDbContext _context;

        public IndexModel(SimpleBlog.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<SimpleBlog.Data.Entities.Post> Post { get;set; }

        public async Task OnGetAsync()
        {
            Post = await _context.Posts.ToListAsync();
        }
    }
}
