﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using SB.Core.DbInitializer;
using SB.Service.Ioc;
using SimpleBlog.Services;
using System.Collections.Generic;
using System.IO;

namespace SimpleBlog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Register services module
            services.AddServicesModule(Configuration);

            //services.AddDbContext<ApplicationDbContext>(options =>
            //    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //services.AddIdentity<ApplicationUser, IdentityRole>()
            //    .AddEntityFrameworkStores<ApplicationDbContext>()
            //    .AddDefaultTokenProviders();

            // In production, the Angular files will be served from this directory
            //services.AddSpaStaticFiles(configuration =>
            //{
            //    configuration.RootPath = "adminapp/dist";
            //});
            services.AddAutoMapper();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

                dbInitializer.Initialize();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //      name: "Admin",
            //      template: "{admin}/{controller=Category}/{action=Index}/{id?}"
            //    );
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "area",
                    template: "{area:exists}/{controller=Category}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });


            //app.Map("/admin", adminApp =>
            //{
            //    HandleMap(adminApp, env, AppSetting.AdminApp);
            //});
        }

        private void HandleMap(IApplicationBuilder app, IHostingEnvironment env, string appName)
        {
            // To learn more about options for serving an Angular SPA from ASP.NET Core,
            // see https://go.microsoft.com/fwlink/?linkid=864501 
            if (env.IsProduction())
            {
                app.UseSpaStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), $"adminapp/dist")),
                });
            }

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "adminapp";
                spa.Options.DefaultPage = $"/index.html";

                if (env.IsDevelopment())
                {
                    var setting = this.Configuration["angularCli:use"];

                    //Run "ng serve" independently
                    //more info: https://docs.microsoft.com/en-us/aspnet/core/spa/angular?view=aspnetcore-2.1&tabs=visual-studio#run-ng-serve-independently
                    if (setting == AppSetting.AngularCliProxy)
                    {
                        switch (appName)
                        {
                            case AppSetting.AdminApp:
                                {
                                    spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                                    break;
                                }
                        }
                    }
                    else
                    {
                        spa.UseAngularCliServer(npmScript: $"start-{appName}");
                    }
                }
            });
        }

        //private AutofacServiceProvider ConfigureAutofac(IServiceCollection collection)
        //{
        //    var containerBuilder = new ContainerBuilder();
        //    containerBuilder.Register(i => new LogEngineInterceptor());
        //    containerBuilder.RegisterType<LogEngineToFile>().As<ILogEngineWriter>();

        //    containerBuilder.RegisterAssemblyTypes(typeof(IService).Assembly)
        //                        .Where(s => s.Name.EndsWith("Service"))
        //                        .AsImplementedInterfaces()
        //                        .EnableInterfaceInterceptors()
        //                        .InterceptedBy(typeof(LogEngineInterceptor));
        //    containerBuilder.Populate(collection);
        //    var container = containerBuilder.Build();

        //    return new AutofacServiceProvider(container);
        //}

        private static class AppSetting
        {
            public const string AdminApp = "app";
            public const string AngularCliServer = "server";
            public const string AngularCliProxy = "proxy";
        }
    }
}
