﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.Data.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
