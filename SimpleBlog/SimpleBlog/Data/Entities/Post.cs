﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.Data.Entities
{
    public class Post: BaseEntity
    {
        public string Title { get; set; }
        public string Detail { get; set; }
        public string Description { get; set; }
    }
}
