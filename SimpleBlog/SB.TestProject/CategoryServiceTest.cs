using System;
using System.Collections.Generic;
using AutoMapper;
using Moq;
using SB.Repository.Entities;
using SB.Repository.Repositories.Interfaces;
using SB.Service.ServicesImpl;
using Xunit;

namespace SB.TestProject
{
    public class CategoryServiceTest
    {
        public readonly CategoryService _categoryService;
        public readonly Mock<IRepository<Category>> category;
        public readonly Mock<IMapper> mapper;

        public CategoryServiceTest()
        {
            category = new Mock<IRepository<Category>>();
            mapper = new Mock<IMapper>();
            _categoryService = new CategoryService(category.Object, mapper.Object);
        }

        [Fact]
        public void GetAllTest()
        {
            List<Category> cs = new List<Category>(){new Category()};
            category.Setup(r => r.GetAll()).Returns(cs);
            var a = _categoryService.GetAll();

            Assert.Equal(a.Count, cs.Count);
        }
    }
}
