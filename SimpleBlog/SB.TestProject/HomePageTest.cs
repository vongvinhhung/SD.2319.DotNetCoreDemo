﻿using System.Net;
using System.Threading.Tasks;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using AspNetSample.E2ETests.Helpers;
using AngleSharp.Dom.Html;
using SimpleBlog;
using System.Net.Http;

namespace SB.TestProject
{
    public class HomePageTest : IClassFixture<WebApplicationFactory<Program>>
    {
        public HomePageTest(WebApplicationFactory<Program> factory)
        {
            Factory = factory;
        }

        public WebApplicationFactory<Program> Factory { get; }

        [Fact]
        public async Task CanLogin()
        {
            HttpClient client = null;
            client = Factory.CreateClient();

            // Act
            var loginPage = await client.GetAsync("/Account/Login");
            Assert.Equal(HttpStatusCode.OK, loginPage.StatusCode);
            var loginPageHtml = await HtmlHelpers.GetDocumentAsync(loginPage);

            var profileWithUserName = await client.SendAsync(
                (IHtmlFormElement)loginPageHtml.QuerySelector("#account"),
                new Dictionary<string, string> { ["Email"] = "ADMIN@SIMPLEBLOG.COM", ["Password"] = "!Abc123" });

            Assert.Equal(HttpStatusCode.OK, profileWithUserName.StatusCode);
        }
    }
}
