﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SB.Core.DbInitializer;
using SB.Core.Identity;
using SB.Core.Services;
using SB.Repository.DbContextImpl;
using SB.Repository.Extensions;
using SB.Repository.Identity;
using SB.Service.DbInitialize;
using SB.Service.Identity;
using System.Reflection;

namespace SB.Service.Ioc
{
    public static class ServiceIoc
    {
        public static IServiceCollection AddServicesModule(this IServiceCollection services, IConfiguration configuration)
        {
            //Data module
            services.AddDataModule(configuration);

            //Configure Identity
            services.AddIdentity<User, Role>()
                    .AddEntityFrameworkStores<SBDbContext>()
                    .AddUserStore<SBUserStore>()
                    .AddRoleStore<SBRoleStore>()
                    .AddUserManager<SBUserManager>()
                    .AddSignInManager<SBSignInManager>()
                    .AddRoleManager<SBRoleManager>()
                    .AddDefaultTokenProviders();

            //Configure DI for services
            services.Scan(scan =>
            {
                scan.FromAssemblies(Assembly.GetExecutingAssembly())
                    .AddClasses(classes => classes.AssignableTo<IService>())
                    .AsImplementedInterfaces()
                    .WithTransientLifetime(); //Transient = instance when required
            });

            //Other
            services.AddTransient(typeof(IDbInitializer), typeof(DbInitializer));
            services.AddTransient(typeof(IHttpContextAccessor), typeof(HttpContextAccessor));
            return services;
        }
    }
}
