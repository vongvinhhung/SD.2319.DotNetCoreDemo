﻿using AutoMapper;
using SB.Core.DTO;
using SB.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SB.Service.AutoMapper
{
    public class MappingConfiguration : Profile
    {
        public MappingConfiguration()
        {
            CreateMap<Category, CategoryDto>().ReverseMap();
            CreateMap<Post, PostDto>()
                .ForMember(des => des.CategoryName, opt => opt.MapFrom(src => src.Category.Name));

            //Revert();
        }

        public void Revert()
        {
            CreateMap<PostDto, Post>();
            CreateMap<CategoryDto, Category>()
                .ForMember(des => des.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(des => des.Name, opt => opt.MapFrom(src => src.Name));
            
        }
    }
}
