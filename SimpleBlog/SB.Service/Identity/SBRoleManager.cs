﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using SB.Core.Identity;
using System.Collections.Generic;


namespace SB.Service.Identity
{
    public class SBRoleManager : RoleManager<Role>
    {
        public SBRoleManager(
              IRoleStore<Role> store,
              IEnumerable<IRoleValidator<Role>> roleValidators,
              ILookupNormalizer keyNormalizer,
              IdentityErrorDescriber errors,
              ILogger<SBRoleManager> logger) 
            : base(store, roleValidators, keyNormalizer, errors, logger)
        {
        }
    }
}
