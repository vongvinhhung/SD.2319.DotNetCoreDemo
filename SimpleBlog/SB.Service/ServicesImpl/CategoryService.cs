﻿using SB.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using SB.Core.DTO;
using SB.Repository.Repositories.Interfaces;
using SB.Repository.Entities;
using System.Linq;
using AutoMapper;

namespace SB.Service.ServicesImpl
{
    public class CategoryService : ICategory
    {
        protected readonly IRepository<Category> CategoryDbSet;
        protected readonly IMapper mapper;

        public CategoryService(IRepository<Category> _categoryDbSet, IMapper _mapper)
        {
            CategoryDbSet = _categoryDbSet;
            mapper = _mapper;
        }

        public bool Add(CategoryDto dto)
        {

            var entity = mapper.Map<Category>(dto);
                
            var result = CategoryDbSet.Insert(entity) ;

            CategoryDbSet.SaveChanges();
            return true;
        }

        public bool Edit(CategoryDto dto)
        {

            var entity = CategoryDbSet.GetById(dto.Id);

            entity =  mapper.Map(dto, entity);

            CategoryDbSet.SaveChanges();
            return true;
        }

        public List<CategoryDto> GetAll()
        {
            return CategoryDbSet.GetAll().Select(x => new CategoryDto{ Id = x.Id, Name = x.Name, Description = x.Description }).ToList();
        }
    }
}
