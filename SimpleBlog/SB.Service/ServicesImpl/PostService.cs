﻿using SB.Core.DTO;
using SB.Core.Services;
using SB.Repository.Entities;
using SB.Repository.Repositories.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using AutoMapper.QueryableExtensions;

namespace SB.Service.ServicesImpl
{
    public class PostService : IPost
    {
        protected readonly IRepository<Post> PostDbSet;

        public PostService(IRepository<Post> _postDbSet)
        {
            PostDbSet = _postDbSet;
        }

        public List<PostDto> GetAllByCategoryId(int categoryId)
        {
            return PostDbSet.Query().Where(r => r.CategoryId == categoryId).ProjectTo<PostDto>().ToList();
        }

        public bool Add(PostDto dto)
        {
            var entity = new Post
            {
                Description = dto.Description,
                Title = dto.Title,
                Detail = dto.Detail,
                CategoryId = dto.CategoryId
            };

            var result = PostDbSet.Insert(entity);

            PostDbSet.SaveChanges();
            return true;
        }

        public PostDto Get(int id)
        {
            return PostDbSet.Query().Where(r => r.Id == id).ProjectTo<PostDto>().FirstOrDefault();
        }

        public List<PostDto> GetAll()
        {
            return PostDbSet.Query().ProjectTo<PostDto>().ToList();
        }
    }
}
