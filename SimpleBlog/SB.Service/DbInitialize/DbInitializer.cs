﻿using Microsoft.EntityFrameworkCore;
using SB.Core.DbInitializer;
using SB.Core.Identity;
using SB.Repository.DbContextImpl;
using SB.Repository.Entities;
using SB.Service.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SB.Service.DbInitialize
{
    public class DbInitializer : IDbInitializer
    {
        private readonly SBUserManager userManager;
        private readonly SBRoleManager roleManager;
        private readonly SBDbContext context;
        public DbInitializer(SBUserManager userManager, SBRoleManager roleManager, SBDbContext _context)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.context = _context;
        }

        public void Initialize()
        {
            context.Database.Migrate();

            if (!this.roleManager.Roles.Any(x => x.Name == "Admin" || x.Name == "User"))
            {
                this.roleManager.CreateAsync(new Role { Name = "Admin" }).Wait();
                this.roleManager.CreateAsync(new Role { Name = "User" }).Wait();

                var category = new Category()
                {
                    Name = "IT",
                    Description = "IT field",
                    CreatedBy = "admin",
                    CreatedDate = DateTime.Now
                };

                var category1 = new Category()
                {
                    Name = "Sport",
                    Description = "Sport field",
                    CreatedBy = "admin",
                    CreatedDate = DateTime.Now
                };
                context.Add(category);
                context.Add(category1);

                var posts = new List<Post>()
                {
                    new Post ()
                    {
                        CategoryId = category.Id,
                        Title = "Get started with .NET in 10 minutes",
                        Description = "<p>ASP.NET Core is a cross-platform, high-performance, <a>open-source</a> framework for building modern, cloud-based, Internet-connected applications. With ASP.NET Core, you can:</p><ul><li> Build web apps and services, <a> IoT </a> apps,                and mobile backends.</li><li> Use your favorite development tools on Windows,                macOS,                and Linux.</li><li> Deploy to the cloud or on - premises.</li><li> Run on <a>.NET Core or.NET Framework </a>.</li></ul> ",
                        Detail = "<h2 id=\"why-use-aspnet-core\" class=\"heading - with - anchor\">Why use ASP.NET Core?<a class=\"docon docon - link heading - anchor\" tabindex=\" - 1\" aria-hidden=\"true\" ></a></h2><p class=\"x-hidden-focus\">Millions of developers have used (and continue to use) <a href=\"https://docs.microsoft.com/aspnet/overview\" data-linktype=\"external\">ASP.NET 4.x</a> to create web apps. ASP.NET Core is a redesign of ASP.NET 4.x, with architectural changes that result in a leaner, more modular framework.</p>"
                    },
                    new Post()
                    {
                        CategoryId = category.Id,
                        Title = "IoC.Resolve vs Constructor Injection",
                        Description = "Resolve instead of Constructor Injection is that you don't need to create classes that have 5 parameters in the constructor, and whenever you are going to create a instance of that class you're not gonna need to provide it with anything",
                        Detail = "<div class=\"post-text\" itemprop=\"text\"><p><code>IoC.Resolve&lt;&gt;</code> is an example of the <a href=\"http://martinfowler.com/articles/injection.html#UsingAServiceLocator\" rel=\"noreferrer\">Service Locator</a> pattern. That pattern imposes a few restrictions that constructor injection does not:</p><ul><li>Objects can have no more fine-grained context than the application domain, due to the static calls</li><li>Objects decide <em>which versions</em> of dependencies to resolve. All instances of a certain class will get the same dependency configuration.</li><li>The temptation to couple code to the container is high, for example instead of creating an intention-revealing factory.</li><li>Unit testing requires container configuration, where the classes could just be created and used otherwise. (This is especially troublesome when you want to test multiple configurations of the same class, due to the second issue above.)</li><li>An application's structure cannot be inferred from its public API. (Constructor parameters are a <strong>good</strong> thing. They are not a problem you should feel the need to solve.)</li></ul>"
                    },new Post ()
                    {
                        CategoryId = category1.Id,
                        Title = "Serbia 0-2 Brazil: Paulinho and Thiago Silva take Brazil through",
                        Description = "Serbia finish third in World Cup Group E behind Switzerland ",
                        Detail = @"<p>Brazil eliminated Serbia and secured their place in the second round of the World Cup after a comfortable 2-0 victory in Moscow.</p>
        <p>Brazil were at risk of becoming the second World Cup heavyweight to crash out on Wednesday following Germany's shock exit, but Paulinho's lob settled the nerves on 36 minutes and Thiago Silva's second-half header (68) confirmed their passage as Group E winners.</p><p>The victory means Tite's side will play Mexico in Samara on Monday afternoon and could then potentially face the winner of England's group in a quarter-final.</p>"
                    },
                    new Post()
                    {
                        CategoryId = category1.Id,
                        Title = "England's possible route to the World Cup final: Who could they face?",
                        Description = "Defending champions Germany crashed out on a day of World Cup drama in Groups E and F, but what does it all mean for England's knockout prospects?",
                        Detail = @"Gareth Southgate's side are already through to the last-16 having beaten Tunisia and Panama in Group G, but the outcome of Thursday evening's meeting with Belgium will dictate whether they go through as group winners or in second place.

Which scenario would provide a more favourable route to the final? Here, we take a look at what might be in store - and why a second-placed finish might be preferable."
                    }
                };

                context.AddRange(posts);
                context.SaveChanges();

            }

            var defaultAdminAccount = this.userManager.FindByNameAsync("admin").Result;
            if (defaultAdminAccount == null)
            {
                var user = new User { UserName = "admin", Email = "admin@simpleblog.com" };
                this.userManager.CreateAsync(user, "!Abc123").Wait();
                this.userManager.AddToRoleAsync(user, "Admin").Wait();
            }

        }
    }
}
